//Back and forward buttons to navigate between menus

const navRender = `
   	<div id=navbar>
   		<button id=backNav class="fa fa-arrow-circle-left navButton"></button>
		<div class="flex-spacer"></div>
   		<button id=forwardNav class="fa fa-arrow-circle-right navButton"></button>
   	</div>
`

export default navRender;
