//Block containing the Interbranch question template

const trunkTemplate = `
    <div id="trunk-container">
      <div id="trunk-question-title">What is your question?</div>
      <div id="trunk-paragraph-label-a" class="trunk-paragraph-label">Paragraph A:</div>
      <div id=trunk-paragraph-a class=trunk-paragraph>
        This text is the content of the box. We have added a 50px padding, 20px margin and a 15px green border. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </div>
      <a id=trunk-paragraph-link-a class=trunk-paragraph-link href="https://gnu.org">Source</a>
      <div id="trunk-paragraph-label-b" class="trunk-paragraph-label">Paragraph B:</div>
      <div id=trunk-paragraph-b class=trunk-paragraph>
        This text is the content of the box. We have added a 50px padding, 20px margin and a 15px green border. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </div>
          <a id=trunk-paragraph-link-b class=trunk-paragraph-link href="https://gnu.org">Source</a>
      <div id=trunk-range-slider>
        <input id=trunk-range type="range" class=trunk-slider min="-2" max="2" step="1" list="trunk-values">
        <datalist id="trunk-values">
          <option value="-2" label="A"></option>
          <option value="-1">|</option>
          <option value="0">|</option>
          <option value="1">|</option>
          <option value="2" label="B"></option>
        </datalist>
      </div>
    </div>
`;

export default trunkTemplate;

