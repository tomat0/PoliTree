//Block containing the Branch result card contents

const branchTemplate = `
<div id=branch-container>
    <div id="branch-header">
        <div id=branch-title>
            <h3>At long last, you have arrived at the <span id="branchNameTitle" class="branchName keyName" class="keyName">Developer</span> branch of the <span id="headerLeafName" class="keyName leafName">Bugged</span> leaf <span id="clusterInfo"> between the Cluster1 and Cluster2 clusters.</span></h3>
        </div>
        <div id="branch-definition">Developers may expect to see this screen, but if you are not a developer the site is likely bugged.</div>
    </div>
    <div id=branch-notes-block>
        <div id=branch-notes-header>Things to note about the <span class="branchName keyName">this page</span> branch:</div>
        <ul id=branch-notes>
          <li>If you did not intend to see this page, report the issue on the Codeberg, what device/browser you're running on, and what happened before it occurred.</li>
        </ul>
    </div>
    <div id="subbranch-block">
        <div id="subbranch-header">Ideologies you may want to look into:</div>
        <ul id="subbranch-list">
            <li>JavaScript may have not loaded properly</li>
            <li>Possibly an error in the trunkRender.js file</li>
        </ul>
    </div>
    <div id="leaf-book-list-block">
        <div id="leaf-book-list-header" class="main-book-list-header collapsible"><span class="collapse-marker">+</span>Good places to start if you are interested in <span id="leafname-books" class="leafName keyName">Software Development</span> thought:</div>
        <ul id=leaf-book-list class="main-book-list collapsible-content"></ul>
    </div>
    <div id="branch-book-list-block">
        <div id="branch-book-list-header" class="main-book-list-header collapsible"><span class="collapse-marker">+</span>Good places to start if you are interested in <span id="branchname-books" class="branchName keyName">Technology</span> thought:</div> 
        <ul id="branch-book-list" class="main-book-list collapsible-content"></ul>
    </div>
</div>
`;

export default branchTemplate;

