//Block containing the structure for each book item's collapsible
//"book-item" is a misnomer, that button is just the +/- collapsible
//likely going to replace the table with a grid for better responsiveness


const bookItem = `
    <li id="book-item-tmp" class="book-item">
    <button class="collapsible">
        <span class="collapse-marker">+</span>
        <span class=book-title href="https://gnu.org">The Art of Unix Programming</span> by <span class="book-author">Eric S. Raymond</span> 
    </button> 
    <a href="https://gnu.org" class="pdf-button badge">PDF</a> 
    <a href="https://gnu.org" class="epub-button badge">ePUB</a>
    <div class="collapsible-content">
        <div class="book-data">              
            <div class="length-field book-field"><span class="book-field-name">Length:</span> <span class="book-length">&#9733;</span></div>
            <div class="difficulty-field book-field"><span class="book-field-name">Difficulty:</span> <span class="book-difficulty">&#9733;</span></div>
            <div class="genre-field book-field"><span class="book-field-name">Genre:</span> <span class="book-genre">Technology</span></div>
            <div class="type-field book-field"><span class="book-field-name">Type:</span> <span class="book-type">Theory</span></div>
            <div class="summary-field book-field"><span class="book-field-name">Summary:</span> <span class="book-summary">Raymond discusses the history of Unix culture and the unique principles behind its design.</span></div>
        </div>
    </div>
    </li>
`
export default bookItem;
