
// Generate the HTML components for the splash page
export function genSplashComponent() {
	let splashHTML =`
	  <div class="splash" style="display:flex;flex-direction:column;align-items:center;">
	    <img id="splash-img" src="favicon.png">
	    <h1 class="splash-head">PoliTree</h1>
	    <div class="splash-subhead">A different kind of political test.</div>
		<p><a button id="start-button" class="pure-button">Start</a></p>
	  </div>`;
	return splashHTML;
}
