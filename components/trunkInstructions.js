//Block containing instructions for paragraph questions

var trunkInstructions = `
   	<div id="trunk-instructions">
       		<ul style="text-align:start">
                <li>Now that we have a more general idea of where you fall, the next step is to narrow things down.</u>
                <li>You will be presented with a series of questions, each accompanied by two passages providing different answers to said question.</li>
                <li><b>For each of these, drag the slider to which passage you agree with more/lean towards.</b></li>
                <li>You may have two or three of these questions, after which you will be presented with your final result.</li>

            </ul>
            <p><center><b>Click the right arrow to continue.</b></center></p>		
   	</div>
`;
export default trunkInstructions;
