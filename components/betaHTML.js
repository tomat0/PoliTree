//Block containing individual canopy question/answer sets

var betaHTML = `
    <div style="display:flex;flex-direction:column;">
    <p>That's it for the test for now. The rest still has to be coded, so please be patient.</p>
    <p>Screenshot your selections and send it over, and I'll manually analyze it.</p> 
    </div>
`;
export default betaHTML;
