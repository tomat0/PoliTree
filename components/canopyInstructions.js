//Block containing individual canopy question/answer sets

var canopyInstructions = `
   	<div id="canopy-instructions">
       		<ul style="text-align:start">
                <li>The goal of this level is to give us a general idea of where you'd fall.</u>
                <li>You will be presented with six "question blocks". In these blocks you can choose both a question and an answer.</li>
                <li><b>From each dropdown, select the question that's most important to you.</b></li>
                <li>After you've selected a question, select the answer to that question you agree with the most.</li>
                <li>After you've selected six questions/answers, proceed to the follow-up. In the follow-up, you'll get one last question block, where you once again do the same thing.</li>

            </ul>
            <p><b>Click the right arrow to continue.</b></p>		
   	</div>
`;
export default canopyInstructions;
