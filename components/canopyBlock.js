export const BLOCK_ID = "c-block-X"; //HTML ID for canopy block
export const QB_ID = "c-q-block-X" //HTML ID for question block
export const AB_ID = "c-a-block-X"; //HTML ID for answer block
export const D_ID = "d-cX";
export const DEFAULT_ANS = "Use the dropdown to pick a question..."
export const DEFAULT_VAL = "blank"

export var template = `
	<div id="c-block-X" class="canopy-question-grid-container">
		<select id="c-q-block-X" class="canopy-q-block-question canopy-grid-item" id="question"></select>
		<div id="c-a-block-X" class="canopy-q-block-answer canopy-grid-item"></div>
	</div>
`;

//Generate the list of div IDs to use in the template construction, takes question and answer counts as parameter
export function divIDs(qCount, aCount) {
    let idPH = [BLOCK_ID, QB_ID, AB_ID];

    idPH.push(D_ID);
    for (let q = 0; q < qCount; q++) { //How many are generated depends on parameters  
        idPH.push("qY-cX".replace("Y", q));
    }
    for (let a = 0; a < aCount; a++) {
	    idPH.push("aY-cX".replace("Y", a));
    }
    return idPH;
}

//Generate a question to add to the select menu
export function qSelect(qCount) {
    addDefaultQ();
    for (let q = 0; q < qCount; q++) {
        document.getElementById(QB_ID).innerHTML += `<option id="qY-cX" value="">Placeholder...?</option>`.replace("Y", q);
    }
}


//Generate an answer to add to the block
export function ansBlock(aCount) {
    for (let a = 0; a < aCount; a++) { 
        //HTML for answer block
        document.getElementById(AB_ID).innerHTML += `
            <div>
                <input id="aY-cX" type="radio" value="NULL">
                <label for="aY-cX">If you see this, the site is bugged</label>
            </div>
        `.replace(/Y/g, a);
    }
}
//Generate the default selections to use
export function addDefaultQ() {
    let qBlock = document.getElementById(QB_ID);
    let option = document.createElement("option");
    option.text = "Select the question most important to you...";
    option.value = "blank";
    option.id = D_ID;
    qBlock.appendChild(option);
}

