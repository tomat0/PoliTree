//Block containing the Leaf result card contents

const leafTemplate = `
    <div id=leafContents>
        <img id=leafIcon src=resources/bug-icon.svg></img>
        <div id="leafHead">
            You are traveling along the <span id="leafName" class="keyName">Leaf</span> leaf 
                <span id="clusterInfo">between the Cluster1 and Cluster2 clusters.</span>
        </div>
        <div id="leafQuote">“Well done. Here come the test results: You are a horrible person...I'm serious, that's what it says: "A horrible person." - GlaDoS</div>
        <div id="noteHeader">What you've answered in the previous activity:</div>
        <ul id="leafNotes">
            <li>If you are seeing this screen, the test is likely <span class=keyword>bugged</span>.</li>
            <li>This screen is for <span class=keyword>developers</span> only.</li>
            <li>If you see this, report it to <span class=keyword>Tomat0.</span></li>
        </ul>
        <div id="continueMessage">However, the test is not over yet. We can still get a more precise result.</div>
    </div>
`;

export default leafTemplate;

