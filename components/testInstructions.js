//Block containing individual canopy question/answer sets

var testInstructions = `
   	<div id="start-instructions">
       		<p><b>This test works like a <i>tree</i>. You will be presented with <u>two</u> tasks. Completing each task will move you a "level" down the tree, narrowing down your political stance.</b></p>
       		<p>Unlike other tests, the questions here deal more in the realm of abstract concepts, so having some prior political knowledge will give you more accurate results. But if you're unsure on how to answer, feel free to keep guessing and retrying until you find something you want to dig into.</p>
       		<p>At the end of the test, you'll be presented with a list of reading material and ideas for further reference. Even if you're unsure of your result, <i>don't</i> be afraid to read and explore. That's the best way to learn.</p>	
       		<p><b>Click the right arrow to continue to the first question.</u></p>		
   	</div>
`;
export default testInstructions;
