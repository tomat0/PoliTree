# JSON-Based Referencing

The entire site makes use of a JSON system in order to fetch information. This modular structure makes it simpler to edit infomation in one file and have the changes automatically and consistently apply across the whole site.

All JSONs have an ID property. This ID is used to determine what type of JSON is being dealt with in addition to allowing for JSONs to reference other JSONs. The ID once made should never be changed, because of the chance that it is referenced elsewhere. Other properties such as name can be readily changed, but the ID must remain constant. The filename must also always match the ID (with the exception of config.json).


## JSON Directory Structure

The `json` directory contains four subfolders: `json`, `js`, `tools` and `unused`. 

- Under `json/json` exists the JSON files we edit on a regular basis. These files are directly operable on by the Python `tools` scripts. Within this structure there's further division depending on the "type" of JSON: 
	- `json/clusters`, which contains the information for Focuses and Approaches (identifiable by a C- prefix)
	- `json/leaves`, which contains the information for leaves (identifiable by a 'L-' prefix)
	- `json/branches`, which contains the information for branches (identifiable by a 'B-' prefix)
	- `json/questions/canopy`, which contains the information for Level 1 JSONs (identifiable by a 'Q-' prefix)
	- `json/questions/filter`, which contains the information for Level 2 JSONs (identifiable by a 'F-' prefix) 
	- `json/references`, which contain information for books (no prefix, just `last-name_book-title` format)
- The site however, does not directly pull its information from these files. Due to [issues with directly loading JSONs](https://codeberg.org/tomat0/PoliTree/issues/8), it's easier to load them as JS files. The entire `json/json` file structure is replicated in `json/js`. These JS files are not to be directly edited. 
- Under `tools` there exists various Python scripts in the code to allow for easier automation of JSON editing. These scripts will act on JSON files en masse, and have some capability for error detection.
	- `json-js-converter.py` automatically converts all `json/json` files to `json/js`. If you ever change the JSONs, you'll need to run this. This runs as a CLI.
	- `json-validator.py` checks all JSONs for any obvious problems in their construction, whether that be syntax errors, incorrect filenames, etc. This runs as a CLI.
	- `list-field-match.py` allows you to filter for JSONs whose properties match a criteria you specify on a per-directory basis. You edit the global variables before running to use this one. 
	- `json-edit-field.py` will take a specified set of JSONs and set the specified field to the given value. This script is good to modify if you want to write other stuff to the JSONs en masse. You edit the global variables before running to use this one.
- Under `unused` are pretty much all scrapped JSONs and JSONs which aren't finished. There's a continuous turnover in information and sometimes information needs to be brought back. This directory acts as a safe backup for JSONs not ready for use.
- `config.json` contains the global config settings for the site (identifiable by a 'J-' prefix)


## JSON File Structure
Each JSON type has its own set of fields, which contain different properties necessary for loading the various components and information of the site. 

### Config 

- `id`: Do not modify this. 
- `gateway`: URL prefix to the IPFS gateway. Everything except the hash itself should be put into here
- `icon-path`: Relative path to where all the leaf icons are contained
- `default-additional-tendencies-message`

### Clusters

- `name`: Contains the display name of the cluster
- `type`: Either 'focus' or 'approach': indicates whether the file is to be read as a focus or approach
- `color`: Associated color, used as the display color whenever the focus/approach is referenced on the site
- `leaves`: Array of IDs for the leaves contained within the cluster
- `other`: Array of IDs for the other-clustered leaves which overlap with the cluster

### Leaves 

- `name`: Contains the display name of the leaf
- `other`: Boolean stating whether or not the leaf should be considered Other-clustered
- `focus`: Array containing the focuses this would fall on
- `approach`: Approach containing the focuses this would fall on
- `icon`: Icon associated with the leaf, PNG which is displayed on the leaf page, only the filename is needed
- `quote`: Quote to be displayed on the leaf page
- `description`: List of characteristics and how they relate to the approach/focus, array of bullet points with HTML
- `branches`: Array of IDs for branches under this leaf
- `filter_questions`: Array of IDs for filter questions under this leaf
- `references`: Array of IDs for books under this leaf
- `reference-categories`: If there are custom named subsections for references, they're to be listed in this array
- `color`: Associated color, used as the display color whenever the focus/approach is referenced on the site (only for Other-clustered leaves)
- `tiebreaker`: Specifies which question should be considered the tiebreaker (only used for three-branch leaves)
- `tiebreaker-branch`: Specifies which branch the tiebreaker question is with reference to (only used for three-branch leaves)

### Branches

- `name`: Contains the display name of the leaf
- `leaf`: ID of leaf which this falls under
- `subbranches`: Array of strings, contains various ideologies that would "Also be included under..." on the branch page
- `definition`: Definition of the branch on branch page
- `notes`: List of strings which act as bullet points for extra notes on the branch page
- `references`: Array of IDs for books under this branch
- `reference-categories`: If there are custom named subsections for references, they're to be listed in this array
- `additional-references`: Array of arrays, each array maps to the corresponding element in `reference-categories`. Within each array is the list of reference IDs to put under each subsection. If `reference-categories` is empty, this field is not needed.

### Questions (Canopy)

- `weight`: How much weight the question has on a respective focus, default is 1 (currently unused)
- `focus`: Array of focuses. For most questions this is 1-length. For Other-clustered questions, the first index *must* be "C-OTH" and the second index the leaf it pertains to
- `question`: String containing the question. Should end in '...?'
- `answers`: Array of dictionaries, each answer gets a dict. Contained in each answer dict is:
	- `answer`: The answer text
	- `weight`: The approach weight, defaults to 1
	- `approach`: One-element array of approaches
	- `focus`: Optional property which if specified will override the question focus if said answer is selected. One-element array.

### Questions (Filter)

- `leaf`: The leaf the filter question belongs to (string)
- `set`: Either A/B/C for 2-3 branch leaves, otherwise 1A/2A/2B for four-branch leaves. Number signifies level, letter distinguishes questions at the same "level"
- `question`: Text of the question
- `passage-a`/`passage-b`: Properties which contain the text of the passages. Quotation marks are added later on in the JS.
- `source-a`/`source-b`: Link to where the passage was paraphrased from
- `branch-a`/`branch-b`: Either the branch or filter question selecting either A or B will lead you towards. Depending on which is specified, the logic is altered.

### References

- `title`: The book title
- `author`: The book author
- `section`: The leaf or branch ID the reference belongs under
- `subsection`: Boolean, specifies whether or not the book belongs under "Additional references..."
- `pdf`/`epub`: IPFS hashes for the books in their given formats
- `length`: A rating for the length of the book. Use [the table](manual-4.md) on the wiki to determine this value. Must be a number between 0 and 5, only in increments of 0.5.
- `difficulty`: A rating for the book difficulty. This is currently subjective, and done by scanning the text. Must be a number between 0 and 5, only in increments of 0.5.
- `genre`: The book genre, this is also subjectively defined
- `type`: Which of the main 'types' the book would fall under. Use the [list and definitions](manual-4.md) on the wiki to determine.

## List of JSON IDs

This list will not contain the reference JSONs, as those are self-explanatory and theres like 200 of them. 

### Cluster JSONs
{{ read_csv('csv/cluster_id_table.csv') }}

### Leaf JSONs
{{ read_csv('csv/leaf_id_table.csv') }}

### Branch JSONs
{{ read_csv('csv/branch_id_table.csv') }}
