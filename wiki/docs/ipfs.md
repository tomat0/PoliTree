# Accessing Books With IPFS

If you try to access any of the books, you may see that you're unable to open it, because it's using an *IPFS link*. IPFS is a decentralized storage system, which means (without getting into the technical details), the files are not hosted on any single server, but distributed across multiple computers. 

Books are accessed this way because it allows for a much easier, more sustainable, and lightweight way to keep track of content. Weighing the options, it seemed like the best approach for the project long-term. The only issue is that if you would like to access the books, you will have to install some stuff. The IPFS team makes this process relatively easy though.

If you are on desktop, you have two options to get started:

- Install and open the site in [Brave Browser](https://brave.com/), which has everything you need built in
- If you are using another browser, install both the [IPFS app](https://docs.ipfs.tech/install/ipfs-desktop/) and the [IPFS web extension](https://docs.ipfs.tech/install/ipfs-companion/) for your browser

If you are on either an iPhone or Android, install the following:

- Install [Durin](https://docs.ipfs.tech/install/ipfs-desktop/), the IPFS mobile app

Once you're set up, you should be able to visit any of the links and automatically see the contents. If you need to quickly navigate back to any of the results pages which contain the books, the wiki contains [a handy directory](results.md). 
