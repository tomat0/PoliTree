# Copyright

Any material or made by Tomat0 (the triangle itself, the manual, the favicon, blogposts, site-original information, etc.) is licensed under [CC BY-NC-SA 4.0.](https://creativecommons.org/licenses/by-nc-sa/4.0/) The code for the site is licensed under the GNU AGPLv3. 

Special thanks goes out to Roomsaver and XYZInferno for assisting with coding the site.

As should be obvious, linked references and books are not written by me, but rather instead their respective authors, who are named within the pages citing the work. 

Credits for miscellaneous icons are as follows, all have been slightly modified in that they were recolored:

* Internationalist: "File:Blue globe icon.svg" by by Torty3 is licensed under CC BY-SA 3.0.
* Moderate: "File:Peace-dove.svg" by wikimedia user EOZyo is licensed under CC BY-SA 3.0 
* Proprietarian: "key" is licensed under CC0 1.0 
* Integralist: "File:Statement icon (The Noun Project).svg" by Veysel Kara is licensed under CC BY 3.0 
* Nationalist: "flag" is licensed under CC0 1.0 
* Aristocrat: "Medal" created by FreePik.
* Communist: "File:Falce e martello.svg" by rotemliss is licensed under CC BY-SA 3.0 
* Anarchist: "anarchy symbol" falls under the Public Domain.  
* New Left: "conversation" is licensed under CC0 1.0 
* Progressive:  "rose free icon" from "Astrology Pack" by monkik
* Accelerationist: "gulung semula" is licensed under CC0 1.0 
* Populist: "File:Fist (137364) - The Noun Project.svg" by HANII is licensed under CC0 1.0

The wiki runs on [MkDocs](https://www.mkdocs.org/) and uses the [MkDocs GitBook theme](https://gitlab.com/lramage/mkdocs-gitbook-theme) by Lucas Ramage. 

# Privacy

This site currently only retrieves information pertaining to your selections, all of which can be viewed in your browser's SessionStorage. The site currently is not compliant with LibreJS' extension, this is planned to be fixed in the near future. I am also looking into adding anonymized analytics as a possibility, but that would be a future project too and this page will update accordingly should that occur. 
