## What does all of this stuff on the results page mean?


Once you've completed the assessment, you will be redirected to the Results page. This page contains the branch that matches your answers, a brief definition of the branch, important things to note, and accompanying reading references for both your respective branch and leaf. 

Click the title of each reference to be redirected to a PDF version of the text.

References are chosen based on the following criteria:

* **Length:** Is this book long enough to make a thorough argument while still being at a readable length?
* **Reputation:** Does the author have the background of a professional or one of a celebrity? How has the piece aged? How do those familiar with the topics regard the book?
* **Topic:** Is the topic clear and specific enough? Does it overlap with any other references included? Is the topic something an intrigued/new learner could grasp without preliminary reading?
* **Standpoint:** Does the book encourage or discourage exploration into the subject? Is it written by a member of said ideological current? Was the author witness to any important historical moments?

By clicking on the "+" sign next to each reference, you can expand it to view more details. The following information fields are provided for each text:


* **Difficulty:** The difficulty of the text, represented on a scale of up to five stars. The higher the rating, the more difficult the text. Factors that go into difficulty are presentation, prerequisite knowledge, and writing style.
* **Genre:** The subject matter the text deals with.
* **Type:** The intended purpose of the text. References are classified into: *Critique, Theory, Defense, Interpretation, Tactics* or *Seminal.*
* **Length:** The length of the text. The ratings are assigned as according to the below table, page count being adjusted for font size and appendices:
* **Summary:** A one-to-two sentence long blurb explaining the contents of the reference and its relevance. 


{{ read_csv('csv/manual4_table_2x.csv') }}

