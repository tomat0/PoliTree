# What are interbranch questions? 

Interbranch questions serve the purpose of narrowing down a leaf (the result from the Canopy) to a branch (the final result).
 
* Users are provided with two short passages, each of them arguing a side of a debate highlighted by a singular question. They then select which passage they agree with more (and to what extent) via a slider. 
* Each question contains a specific overarching theme to which there are two opposing principles. What you answer to each of these questions will either bring you further or closer to one side.  
* The nature of these passages/questions varies depending upon the leaf in question.
* Different methods of filtering are employed for leaves containing *two branches*, *three branches*, and *four branches* respectively. 

### Two-Branch Process
1. Develop the central theme, and construct it in the form of an implicit question.  
2. Determine two possible responses, each one aligning with each branch's stance on the issue at hand.
3. Present two passages, and ask the user which passage they agree with more via slider. 
4. Translate the winning principle to its corresponding branch.
5. Present the test-taker with the result of step 4. 

### Three-Branch Process
1. Identify every possible combination of paired branches, and match them up. Since there are only three branches to select from, the number of combinations should come up to three.
2. Conduct steps 1 through 4 of the Two-Branch process with respect to each pair of branches.
3. If any branch has won two **matchups**, present that as the result. 
4. If not resolved, translate, combine the results between the three matchups, evaluate, and present the closest match as the result. 
5. If still not resolved, identify the **eliminating principle**. The eliminating principle is a potential outcome of a matchup which is fundamentally incompatible with one of the branches in question; if the eliminating principle wins a matchup, the corresponding branch should be removed from the running. For the two branches left, reference the matchup with their combination and present the winner of that as the result. 

### Four-Branch Process
1. Divide the four branches into 2x2 groups. We'll call these Group A and Group B. 
2. Conduct steps 1 through 5 of the Two-Branch process with respect to each 2x2 group.
3. If Group A won the above step, conduct steps 1 through 5 of the Two-Branch process with respect to the branches within Group A. 
4. If Group B won the above step, conduct steps 1 through 5 of the Two-Branch process with respect to the branches within Group B.
