# What makes this test different from the countless others?

The methodology for this test differs greatly from that of other political assessments. It uses a mix of various methods, which for the sake of simplicity, has been dubbed the tree method. It is referred to as the tree method, because the composition resembles a tree in structure. In simpler terms, it is a combination of tables and process of elimination.

The components of this tree include:

 *  **The Canopy**: A triangular 3x3 table detailing the intersection between two categories (each with three items). This intersection represents the result of evaluating The Canopy with respect to the test-taker's answers, and is called a *Leaf.*
 * **The Branches**: As each Leaf is rather broad in scope, within them you will find a set of two to four Branches. These Branches are more specific and represent the final result. We evaluate these branches through a set of binary questions, evaluated using either logic gates or a provisional scoring system. These questions are called *Filter Questions.*


The advantages of the Tree Method over traditional scoring or gradient systems is that it is qualitatively assessed rather than quantitatively, much more accurately reflecting the true nature of political distinction.
