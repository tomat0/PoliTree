# Getting Started

If you would like to [contribute](https://codeberg.org/tomat0/PoliTree/issues) to the PoliTree project, the following docs should help to get you up to speed with the codebase. When designing this project, I tried to keep things simple, so it can be easily rehosted/forked. 


- This site is primarily built using [PureCSS](https://purecss.io) and JavaScript. 
- All site information (questions, answers, definitions, books) is stored in JSONs, which are dynamically processed and loaded onto the site. 
- As the user takes the test, values are updated in SessionStorage to track and evaluate their progress. 
- PDFs to books are stored on IPFS, a distributed P2P file-sharing service. In order to access them, you may need to install the IPFS app. More information here.[https://test.tomat0.me/wiki/ipfs/]

The other pages within this category break down our design by tool: how we made use of each tool and what standards/workflow we set for our usage of said tools. 

## Setting up the Developer Environment
In order to develop properly, you will need to have a couple tools running. The below outlines how to set up your environment to begin development. 

### Lighttpd
The code structure relies on modularizing JS/JSON via imports, which runs into conflicts with CORS' Single-Origin-Policy when run without a server, meaning you need a local webserver in order to do development/testing of the site (deployment/production is unaffected). Otherwise all JS-loaded content will be blocked, rendering the site non-functional. 

Lighttpd is a very lightweight and simple webserver to run, and it does the job for testing while being incredibly simple to set up and run. In order to install and set up in under 5 minutes, [follow the 5 Minutes section on the official installation guide.](https://redmine.lighttpd.net/projects/1/wiki/TutorialConfiguration#5-minutes) 

#### Configuration File
The following settings are needed in your `lighttpd.conf` file to run. 

```
server.document-root = <put path to directory containing index.html here>
server.port = <put empty port number here>
dir-listing.activate = "enable"
```

#### Console Commands

##### Validate Lighttpd Configuration File
`lighttpd -tt -f lighttpd.conf`

##### Run Server
`lighttpd -D -f lighttpd.conf`

### SaSS
Sass is part of the toolchain as it extends CSS' functionality, however in order to use, changes have to be transpiled back to CSS.

#### Auto-Transpile on Edit

The way the codebase is set up, `css/main.scss` is where the changes are made, and then the transpiler converts that into main.scss. Make sure you're in the redesign directory and then run the following command to have Sass automatically transpile any changes you make to the SCSS file on save. 

`sass --watch css/main.scss main.css`

To manually transpile, run:

`sass css/main.scss main.css`
