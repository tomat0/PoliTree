<img src="https://tomatdividedby0.gitlab.io/resources/triangle.png" alt="triangle" width="500"/>

# What do the colors/placements mean?
The triangle itself is not a grid or map like the traditional political compass, but rather instead more akin to a table. The primary colors represent one factor of the table, and the secondary represents the other factor. In this sense, each individual triangle acts as an intersection of Approach and Focus: 

## Approach (Primary Colors): 

### Advance 
  * Accepts the fundamental assumptions of modern thought and practice, seeking to preserve the underlying methodology while emphasizing the scrutinization of resulting doctrines.
     * Colored **yellow** on the diagram. 
     * Sees the value of an individual (within the context of their Focus) as *mutable.* 
     * Typically associated with *liberal democracy*.
### Restore
  * Holds firmly to a consistent set of ideals around which they wish to rebuild society. 
     * Colored **blue** on the diagram.
     * Sees the value of an individual (within the context of their Focus) as *innate.* 
     * Typically associated with the *reactionary right.*
### Abolish
  * Critical on a fundamental level, stressing the liberation of humanity as incompatible with the existing structures and orthodoxies.  
     * Colored **red** on the diagram.
     * Sees the value of an individual (within the context of their Focus) as *incommensurable*.
     * Typically associated with the *radical left*.

## Focus (Secondary Colors): 

### Economic
  * Believes a society's process of production and distribution primarily shape its other aspects.
    * Colored **green** on the diagram.
    * Words associated with this include: economy, currency, production, labor, distribution, resources, trade, exchange, technology 
### Ideological
  * Believes a society's dominant values/narrative primarily shapes its other aspects. 
    * Colored **orange** on the diagram.
    * Words associated with this include: culture, morals, ethics, values, ideas, discussion, progress, tradition, common sense, opinions, beliefs, art, media.
### Political
  * Believes a society's concept of order and stability primarily shapes its other aspects. 
    * Colored **purple** on the diagram.
    * Words associated with this include: power, influence, coercion, hierarchy, strength, force, conflict, domination, rule, elite, law, justice, violence, peace 