# Home

The PoliTree is a political test which tries to address the problems that have long been known to plague other political tests, such as the political compass and 8-Values. People often say it's impossible to make an actually good political test, but I figured I'd try anyways. 

**Problems with traditional political tests:**

- Results just arbitrarily places you on a meaningless scale
- Overly focused on asking questions about policy, instead of getting to the root of people's worldviews
- Built with a "more questions is better" mentality, meaning these tests take forever to finish
- Don't really give you anywhere to go after you finish it

**Here's what the PoliTree aims to to differently:**

- Gives meaningful results that are actual ideologies and not just a chart or made-up label
- Takes only about 5 minutes to complete, with two meaningful activities 
- Provides actual reading material to encourage people to learn
- Based on years of research and manual design as opposed to recycling someone else's model

**With this project, what I hope to do is:**

- Help people new to politics get started, even if they don't know where to look
- Encourage people to approach politics with nuance, and discover high-quality reading on their information as opposed to junk
- Show that political beliefs are qualitatively rather than quantitatively distinct


**Important disclaimer: The PoliTree just launched into beta. Any issues should be reported either to me directly or on the Git, and should not be seen as representative of the final product. There's still a lot of bugfixing and tweaking to the questions/scoring to do.**

## Links 
- [PoliTree test](https://test.tomat0.me) if you would like to see the (still-unfinished) test
- [Project Timeline](https://blog.tomat0.me/2019/10/16/pda-journal/) to view the full development history of the project and keep up with updates
- [Git repository](https://codeberg.org/tomat0/PoliTree/src/branch/master) to contribute and see development live
- [My Mastodon](https://mastodon.social/@tomat0) if you would like to contact me
