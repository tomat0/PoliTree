# Why is this project necessary?
This might seem like a niche project, but what these subjects reflect is not just our views on politics but society and the world as a whole. What kind of information we're being fed, the level of curiosity and nuance we have when learning about or discussing these subjects, ends up playing a large role in shaping the sort of civic engagement we end up taking. 

The sort of politics you see all over the news and on social media is *pop politics*: more focused on signalling and pushing partisan agendas as opposed to honest discussion. It treats its audience not as students, but as customers. It discourages learning, because learning means independence and with critical thinking comes questioning. So people who are immersed in this sort of environment only really understand *who they support*, rather than *what they actually believe* at the core.  The *why* is just as important as the *who* or *what* when it comes to action, because it allows us to know we're going along the path that we truly want. 

It's in this sort of environment we see a large amount of people who remain unsure of what they believe, and these people will often seek out online tests in the hopes that they can better understand themselves. But -- to put it bluntly -- these tests are garbage. Notoriously so. The sort of results you get from the political compass or 8Values, while fun, don't really do much to actually tell you about your politics or where to go from there. 

To be more specific about the problems:
-   The traditional left-right spectrum ends up falling short because "left-wing" and "right-wing" are *relative terms*. There is no universal "left" or "right", depending on what kind of society and time period you're in these words can take wildly different meanings.
-   The political compass tries to fix this by using more concrete terms, but runs into its own set of issues. It comes at things too strongly from a policy perspective. All of its questions are about policy, it defines its spectrum based on how much a democratic government (similar to the USA) should intervene. This completely falls apart when you have ideologies which step outside of policy matters. 
-   8Values attempts to fix this by adding more axes, but all it does is kick the can down the road. The scores tell me nothing about what I believe apart from a set of numbers with no real-world application and the questions still remain contextual. The "ideology" tags it gives you are often meaningless and correlate arbitrarily to the score. 

All of these view politics in a *quantitative* fashion as opposed to a qualitative one; ideas are treated as if they're numbers which can be placed along a spectrum. This model ends up putting a limit on our understanding of politics, which leads to a vaguer understanding of the terms we use. I believe that the result you get should be an actual result, the label you get should have a definition that is both clear and has meaning in the real-world. 

Through the years I've worked on this project, I've gotten a lot of flak from people telling me that it's impossible to make a *good* political test, that a test can't summarize something so complex. Maybe this won't be the perfect political oracle, but I'm more focused on giving it value as an educational tool. It's why I've put so much effort towards carefully curating the reading material and information presented on the site. On the internet, there's an unimaginably large amount of information out there, and that information can often conflict or be unreliable. If you don't know what you're doing, processing it all can get overwhelming. What I want is for people — instead of facing these questions feeling paralyzed or misled — to have a jumping off point to learn about politics in a truly constructive fashion. 

# How does the model work?
There is a wiki currently up, which gives a high-level overview of how the process works. I haven't updated it in years, so it may be rather outdated and is likely in need of a rewrite which is coming eventually. 

# How far along is the development?

The test is undergoing a massive overhaul and redesign: the current goal is to finish by June 2024.  Progress is at full-throttle again. If you would like to follow along with progress, I would recommend checking out the [project timeline](https://blog.tomat0.me/2019/10/16/pda-journal/), which is regularly updated. You can also check out the [Codeberg repository](https://codeberg.org/tomat0/PoliTree) if you'd like to help or see realtime progress.

If you would like to contact me, best place to reach me is via my [Mastodon account](https://mastodon.social/@tomat0).
I also have a [Discord server](https://discord.gg/DVRRgBXnSc) up incase you would like to follow that way. 
