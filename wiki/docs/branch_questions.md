**As of June 11, 2022, 30 out of 32 inter-branch questions have been completed.**

## Advance:
* ### Libertarian
       
     * [Minarchism vs Anarchism](https://test.tomat0.me/leaves/advance/libertarian/filter_MINRvANAR.html)
       
       * [Deontological vs Consequentialist](https://test.tomat0.me/leaves/advance/libertarian/filter_DEONvCNSQ.html)
      
       * [Homesteading vs Usufruct](https://test.tomat0.me/leaves/advance/libertarian/filter_HOMEvsUSUF.html) 

* ### Internationalist

     * [Prescriptive vs Descriptive](https://test.tomat0.me/leaves/advance/internationalist/filter_PRSCvDSCR.html)

     * [Unilateralism vs Multilateralism](https://test.tomat0.me/leaves/advance/internationalist/filter_MULTvUNIL.html) 

     * [Competition vs Cooperation](https://test.tomat0.me/leaves/advance/internationalist/filter_COMPvCOOP.html)

* ### Moderate 
       
     * [Agreement vs Disagreement](https://test.tomat0.me/leaves/advance/moderate/filter_AGREvDISG.html)

## Restore:

* ### Integralist 
       
     * [Social Responsibility vs Personal Responsibility](https://test.tomat0.me/leaves/restore/integralist/filter_PRSNvsSOCL.html)
      
     * [Rehabilitation vs Retribution](https://test.tomat0.me/leaves/restore/integralist/filter_RTRBvsRHBL.html)

     * [Free vs Established Religion*](https://test.tomat0.me/leaves/restore/integralist/filter_FREEvsESTB.html)

* ### Aristocrat
       
     * Contribution vs Manipulation
      
     * [Allocation vs Compensation](https://test.tomat0.me/leaves/restore/aristocrat/filter_ALLOvCMPN.html)

     * [Stratification vs Hierarchy](https://test.tomat0.me/leaves/restore/aristocrat/filter_STRFvHIER.html)
   

* ### Nationalist
     
     * [Domination vs Separation](https://test.tomat0.me/leaves/restore/nationalist/filter_DOMNvSEPR.html)
  
     * [Leadership vs Populace](https://test.tomat0.me/leaves/restore/nationalist/filter_LEADvPOPL.html)
      
     * [Political* vs Ethnic](https://test.tomat0.me/leaves/restore/nationalist/filter_POLIvETHN.html)

## Abolish:

* ### Communist
       
     * [Democratic vs Organic](https://test.tomat0.me/leaves/abolish/communist/filter_DEMOvORGN.html)
       
       * [Decentralized vs Centralized](https://test.tomat0.me/leaves/abolish/communist/filter_DCNTvCENT.html)
       
       * [Class vs Capital](https://test.tomat0.me/leaves/abolish/communist/filter_CLSSvCPTL.html)      

* ### Anarchist 
       
     * [Majority vs Minority](https://test.tomat0.me/leaves/abolish/anarchist/filter_MAJOvsMINO.html)
       
     * [Resistance vs Reconciliation](https://test.tomat0.me/leaves/abolish/anarchist/filter_RTRBvsCLMC.html)
      
     * [Moral Power vs Political Power](https://test.tomat0.me/leaves/abolish/anarchist/filter_MORLvPOLT.html)

* ### New Left
       
     * [Critical Theory vs Self-Theory](https://test.tomat0.me/leaves/abolish/newleft/filter_SELFvsCRIT.html)
      
       * [Culture vs Discourse](https://test.tomat0.me/leaves/abolish/newleft/filter_CULTvsDISC.html)
               
       * Ethical vs Ideological 

## Other:

   * ### Progressive 
       
     * [Incrementalism vs Gradualism](https://test.tomat0.me/leaves/other/progressive/filter_INCRvGRAD.html)      

       * [Regulation vs Nationalization](https://test.tomat0.me/leaves/other/progressive/filter_REGLvNATL.html)    

       * [Class Collaboration vs Class Conflict](https://test.tomat0.me/leaves/other/progressive/filter_CNFLvCLLB.html)
        

   * ### Accelerationist
       
     * [Technological vs Historiographic](https://test.tomat0.me/leaves/other/accelerationist/filter_TECHvHIST.html)

        * [Hypermodernist vs Anti-Modernist](https://test.tomat0.me/leaves/other/accelerationist/filter_ANTIvHYPR.html)

        * [Pessimistic vs Optimistic](https://test.tomat0.me/leaves/other/accelerationist/filter_PSSMvOPTM.html)
       

   * ### Populist
       
     * [Assimilation vs Liberation](https://test.tomat0.me/leaves/other/populist/filter_ASSMvsLIBR.html)