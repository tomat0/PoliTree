## Advance:
* ### [Libertarian](https://test.tomat0.me/?leaf=L-LIBT) (ownership)
       
     * [Anarchocapitalist](https://test.tomat0.me/?result=B-ANCP)
       
     * [Mutualist](https://test.tomat0.me/?result=B-MUTL)
      
     * [Classical Liberal](https://test.tomat0.me/?result=B-MINR)
        
     * [Neoliberal](https://test.tomat0.me/?result=B-NEOL)

* ### [Internationalist](https://test.tomat0.me/?leaf=L-INTR) (conflict)
       
     * [Idealist](https://test.tomat0.me/?result=B-IDEA)
       
     * [Realist](https://test.tomat0.me/?result=B-REAL)

     * [Cosmopolitanist](https://test.tomat0.me/?result=B-COSM)

* ### [Moderate](https://test.tomat0.me/?leaf=L-MODR) (discourse)

     * [Liberal Conservative](https://test.tomat0.me/?result=B-LCON)

     * [Radical Centrist](https://test.tomat0.me/?result=B-RCNT)
     
     * [Pluralist](https://test.tomat0.me/?result=B-PLUR)
       
     * [Syncretist](https://test.tomat0.me/?result=B-SYNC)

## Restore:

* ### [Integralist](https://test.tomat0.me/?leaf=L-INTG) (good)
       
     * [Paleoconservative](https://test.tomat0.me/?result=B-PALO)
       
     * [Paternalist](https://test.tomat0.me/?result=B-PATR)
      
     * [Theocrat](https://test.tomat0.me/?result=B-THEO)

* ### [Aristocrat](https://test.tomat0.me/?leaf=L-ARIS) (reward)
       
     * [Elitist](https://test.tomat0.me/?result=B-ELIT)
       
     * [Technocrat](https://test.tomat0.me/?result=B-TCHN)
      
     * [Meritocrat](https://test.tomat0.me/?result=B-MERT)
   

* ### [Nationalist](https://test.tomat0.me/?leaf=L-NATN) (identity)
       
     * [Identitarian](https://test.tomat0.me/?result=B-IDNT)
       
     * [Fascist](https://test.tomat0.me/?result=B-FASC)
      
     * [Monarchist](https://test.tomat0.me/?result=B-MONR)

## Abolish:

* ### [Communist](https://test.tomat0.me/?leaf=L-COMM) (production)
       
     * [Syndicalist](https://test.tomat0.me/?result=B-SYND)
       
     * [Vanguardist](https://test.tomat0.me/?result=B-VANG)
      
     * [Left Communist](https://test.tomat0.me/?result=B-LCOM)
        
     * [Ultra-Left](https://test.tomat0.me/?result=B-ULFT)


* ### [Anarchist](https://test.tomat0.me/?leaf=L-ANAR) (hierarchy)
       
     * [Social Anarchist](https://test.tomat0.me/?result=B-SANR)
       
     * [Pacifist](https://test.tomat0.me/?result=B-PACF)
      
     * [Liberation Movements](https://test.tomat0.me/?result=B-LBMV)

* ### [New Left](https://test.tomat0.me/?leaf=L-NEWL) (conditioning)
       
     * [Neo-Marxist](https://test.tomat0.me/?result=B-NMRX)
       
     * [Postmodernist](https://test.tomat0.me/?result=B-POMO)
      
     * [Egoist](https://test.tomat0.me/?result=B-EGOI)
        
     * [Post-Leftist](https://test.tomat0.me/?result=B-PLFT)

## Other:

   * ### [Progressive](https://test.tomat0.me/?leaf=L-PROG) (equality)
       
     * [Social Liberal](https://test.tomat0.me/?result=B-SLIB)
       
     * [Social Democrat](https://test.tomat0.me/?result=B-SDEM)

     * [Democratic Socialist](https://test.tomat0.me/?result=DSOC)
      
     * [Utopian](https://test.tomat0.me/?result=B-UTOP)
        
   * ### [Populist](https://test.tomat0.me/?leaf=L-POPL) (people)
       
     * [Civic Nationalist](https://test.tomat0.me/?result=B-CNAT)
       
     * [Left Nationalist](https://test.tomat0.me/?result=B-LNAT)
      

   * ### [Accelerationist](https://test.tomat0.me/?leaf=L-ACCL) (progress)
       
     * [Futurist](https://test.tomat0.me/?result=B-FUTR)
       
     * [Neo-Luddite](https://test.tomat0.me/?result=B-LUDD)
     
     * [Neo-Reactionary](https://test.tomat0.me/?result=B-NEOR)
  
     * [Transhumanist](https://test.tomat0.me/?result=B-THUM)
