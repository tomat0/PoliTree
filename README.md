**Special thanks goes out to Roomsaver and XYZInferno for assisting with coding the site!**

An attempted political-compass killer which prioritizes manual design and encouraging learning over arbitrary scores. Has been in development hell for way too long. Will hopefully be finished by the end of 2024.

**For information on the model or developer documentation, please visit the [wiki.](https://test.tomat0.me/wiki)**


Any material made by the site developers (the triangle itself, the manual, blogposts, site-original information, etc.) is licensed under [CC BY-NC-SA 4.0.](https://creativecommons.org/licenses/by-nc-sa/4.0/) The code for the site is licensed under the GNU AGPLv3. 

As should be obvious, linked references and books are not written by me, but rather instead their respective authors, who are named alongside the title and link to the file. 

Credits for icons are as follows, all have been slightly modified in that they were recolored:
* Internationalist: "File:Blue globe icon.svg" by by Torty3 is licensed under CC BY-SA 3.0.
* Moderate: "File:Peace-dove.svg" by wikimedia user EOZyo is licensed under CC BY-SA 3.0 
* Proprietarian: "key" is licensed under CC0 1.0 
* Integralist: "File:Statement icon (The Noun Project).svg" by Veysel Kara is licensed under CC BY 3.0 
* Nationalist: "flag" is licensed under CC0 1.0 
* Aristocrat: "Medal" created by FreePik.
* Communist: "File:Falce e martello.svg" by rotemliss is licensed under CC BY-SA 3.0 
* Anarchist: "anarchy symbol" falls under the Public Domain.  
* New Left: "conversation" is licensed under CC0 1.0 
* Progressive:  "rose free icon" from "Astrology Pack" by monkik
* Accelerationist: "gulung semula" is licensed under CC0 1.0 
* Populist: "File:Fist (137364) - The Noun Project.svg" by HANII is licensed under CC0 1.0
* Bug: "Bug" from GitHub Octicons is licensed under the MIT License
