//Logic for trunk questions (also known as filter questions)
export {default as template} from  '../../components/trunkPage.js';
import * as importJSON from '../backend/importJSON.js'; 
import * as rand from '../backend/randHelper.js';
import * as pageRender from '../frontend/pageRender.js'

//Initialize the first question
export function init(leafID) {    
    const curQ = initScores(leafID);
    fill(curQ);
    //Ensure an answer is given before continuing
    document.getElementById('trunk-range').addEventListener("input", lockNav); 
}



// Initialize scores in sessionStorage
function initScores(leafID) { 
    const leaf = importJSON.find(leafID);
    let queue;
    //If 2 or 3, add a queue property
    if (leaf.branches.length <= 3) {
        queue = rand.shuffle(leaf.filter_questions);
        sessionStorage.setItem("trunkQueue", JSON.stringify(queue)); 
    }
    else { 
        queue = leaf.filter_questions;
        sessionStorage.setItem("trunkQueue", JSON.stringify(queue));
    }
    
    //Create dict to track scores of branches
    for (const b of leaf.branches) {
        sessionStorage.setItem(b, 0);
    }
    //Track the outcomes of each filter question
    for (const q of leaf['filter_questions']) {
        sessionStorage.setItem(q, "");
    }
    //Get the first question, track it, then return it
    sessionStorage.setItem("currentQ", queue[0]);
    return getNextQ(queue[0],null);
    
}


//Given a question, load its contents onto the UI
export function fill(qID) {
    const q = importJSON.find(qID);
    document.getElementById('trunk-question-title').innerHTML = q.question; //Fill in question 
    //Fill in the paragraphs
    document.getElementById('trunk-paragraph-a').innerHTML = quoteFormat(q['passage-a']); 
    document.getElementById('trunk-paragraph-b').innerHTML = quoteFormat(q['passage-b']);  
    //Fill in the links
    document.getElementById('trunk-paragraph-link-a').href = q['source-a']; 
    document.getElementById('trunk-paragraph-link-b').href = q['source-b'];
    //Fill in the number
    const fillTitle = "Level 2: The Trunk (Question ".concat(q['set']).concat(")");
    document.getElementById('card-ribbon').innerHTML = fillTitle;
    //Fill in website title
    document.title = "Trunk Questions (Set ".concat(q['set']).concat(") - PoliTree");
}


//Determine which trunk question is loaded next, update scores, and load it
export function loadNextQ() {
    let score = parseInt(document.getElementById("trunk-range").value) 
    let flag = null;
    let qID = sessionStorage.getItem("currentQ");

    if (score < 0) { //If negative, take that as paragraph A
        flag = "branch-a";
    }
    else if (score == 0) { //If 0, do not allow proceeding
        return null;
    }
    else if (score > 0) { //If positive, take that as paragraph B
        flag = "branch-b"; 
    }
    
    //Update the score and reset the slider back to center
    updateScore(qID, flag, Math.abs(score));
    document.getElementById("trunk-range").value = 0;
    document.getElementById('forwardNav').disabled = true; //Lock on initialization since slider initializes to 0
   
   // If there's still questions left, go to them, otherwise go to end 
   let curQueue = JSON.parse(sessionStorage.getItem("trunkQueue"));
   if (curQueue.length > 0) {
        const nextQ = getNextQ(qID, flag);
        fill(nextQ);
   }
   else { 
        const result = evaluate();
        pageRender.loadBranch(result);
   }
}


function loadPrevQ() {
    //reset on back
}

function getNextQ(qID, answer) {
    //Check to see whether next question is branching and whether or not there is a next question
    const q = importJSON.find(qID);
    let selection;
    if (answer == null) {
        selection = "B-"; //If this is the initial question, default to the B logic
    }
    else {
        selection = q[answer]; //The next question id
    }
    
    //If the answer leads to a follow-up question, go there straight, otherwise do the standard queue logic
    if (selection.startsWith("F-")) {
        sessionStorage.setItem("trunkQueue", JSON.stringify([]));
        sessionStorage.setItem("currentQ", selection);
        return selection;
    }
    else if (selection.startsWith("B-")) {
        //"Pop" the first element from the queue and return it
        let curQueue = JSON.parse(sessionStorage.getItem("trunkQueue"));
        let pop = curQueue[0]; 
        sessionStorage.setItem("trunkQueue", JSON.stringify(curQueue.splice(1)));
        sessionStorage.setItem("currentQ", pop);
        return pop;
    }
}

//Update the score based on slider selection
function updateScore(qID, flag, score) {
    const curQ = importJSON.find(qID);
    const selQID = curQ[flag];
    if (selQID.startsWith("B-")) { //Only branches get scored
       let selQScore = parseInt(sessionStorage.getItem(selQID)) + score;
       sessionStorage.setItem(selQID, selQScore);
       sessionStorage.setItem(qID, selQID); //Track which branch won which filter question for later
    }
}

//Evaluate the scores and return a result based on what scored highest
function evaluate() {
    const leaf = importJSON.find(sessionStorage.getItem("currentLeaf"));
    
    let maxVal = 0;
    let maxList = [];

    //Iterate through the branches, track highest scoring branches
    for (const b of leaf.branches) {
        const bScore = parseInt(sessionStorage.getItem(b));
        if (bScore > maxVal) {
            maxVal = bScore;
            maxList = [b];
        }
        else if (bScore == maxVal) { //Need to track ties too
            maxList.push(b);
        }
    }
    
    //If 1 max value, return directly
    if (maxList.length == 1) {
        return maxList[0];
    }


    //If 3 max values, defer to the tiebreaker
    //The 3 check MUST come before the 2 check for the code to work
    if (maxList.length == 3) {
        //If the tiebreaker question is in favor of the tiebreaker branch return that 
        const tbBranch = sessionStorage.getItem(leaf.tiebreaker);
        if (tbBranch == leaf['tiebreaker-branch']) {
            return tbBranch;
        }
        //Else strike off the branch and use the next conditional to figure out the versus on the other two
        else {
            maxList = maxList.filter((result) => result != leaf['tiebreaker-branch']);
        }
    }

    //If 2 max values, look up which question compared the two branches and pick the winner of that
    if (maxList.length == 2) {
        let vsQuestion;
        for (const q of leaf['filter_questions']) {
            let filterQ = importJSON.find(q);
            let bSet = [filterQ['branch-a'], filterQ['branch-b']];
            if (areEqual(bSet,maxList)) {
                vsQuestion = q;
            }
        }
        const vsBranch = sessionStorage.getItem(vsQuestion);
        return vsBranch;
    }     
}

//Locks proceeding unless the question has been answered
function lockNav() {
    const navButton = document.getElementById("forwardNav");
    if (this.value == 0) {
        navButton.disabled = true;
        //Switch icon to locked
        navButton.classList.remove("fa-arrow-circle-right");
        navButton.classList.add("fa-lock");
    }
    else {
        navButton.disabled = false;
        //Switch icon to forward
        navButton.classList.remove("fa-lock");
        navButton.classList.add("fa-arrow-circle-right");
    }
}

//Small helper function to stuff text into quotes 
function quoteFormat(text) {
    return "“".concat(text).concat("”")
}


//Helper function to compare the contents of two arrays, irrespective of order
//Originally written by Borislav Hadzhiev: https://bobbyhadz.com/blog/javascript-check-if-two-arrays-have-same-elements

function areEqual(array1, array2) {
  if (array1.length === array2.length) {
    return array1.every(element => {
      if (array2.includes(element)) {
        return true;
      }

      return false;
    });
  }

  return false;
}
