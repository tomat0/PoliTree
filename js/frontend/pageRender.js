//Import the various HTML components (encased in JS) involved in page rendering
import * as splash from '../../components/splashRender.js';
import navBar from '../../components/navButtons.js';
import loadCard from './loadCard.js';


//Import the page contents
import testInst from '../../components/testInstructions.js';
import canopyInst from '../../components/canopyInstructions.js';
import * as renderLeaf from './renderLeaf.js';
import trunkInst from '../../components/trunkInstructions.js';
import betaContent from '../../components/betaHTML.js';
import trunkQ from '../../components/trunkPage.js';
import * as renderTrunk from './renderTrunk.js';
import * as renderBranch from './renderBranch.js';
//Import backend logic
import * as generateCanopy from '../backend/generateCanopy.js';
import * as canopyScore from '../backend/canopyScore.js';
import * as importJSON from '../backend/importJSON.js';

//Keep track of what values navButtons are currently on;
var currentForwardNav;
var currentBackNav;


// Set up the splash to load by default, and title to by default redirect to splash
window.onload = (event) => {
    document.getElementById("title-container").addEventListener("click", loadSplash);
    //Write code to dynamically generate year, currently broken
    //document.getElementById("copyright-year").innerHTML = +new Date().getFullYear()+; 
    if (!urlLoad()) { //Check for (valid) URL parameters, 
        loadSplash(); //If no valid parameters, skip straight to loading splash
    }
};



// The various nav functions for navigating through the site

function loadSplash() { // Home page, at beginning, connects to the Start page
    //Reset title and session storage to defaults
    sessionStorage.clear();
    document.title = "PoliTree - Political Test";

    //Render HTML from JS component then inject into main-frame 
    let splashHTML = splash.genSplashComponent();
    document.getElementById('main-frame').innerHTML = splashHTML;
    document.getElementById('test-nav').innerHTML = ''; //test-nav needs to be reset every time the home screen is loaded

    const startButton = document.getElementById("start-button");
    startButton.addEventListener("click", loadStartInst);
}



function loadStartInst() { // Start page, describes test, connects to Home and Canopy Question
   const startHTML = loadCard("PoliTree: General Instructions",testInst);
   document.getElementById('main-frame').innerHTML = startHTML;
   updateNav(loadSplash, loadCanopyInst); //Update navigation buttons on each page change 
}

function loadCanopyInst() {
   //toggleNavLock(document.getElementById('forwardNav'));
   sessionStorage.clear();
   const cInstHTML = loadCard("Level 1: The Canopy (Instructions)",canopyInst);
   document.getElementById('main-frame').innerHTML = cInstHTML;
   updateNav(loadStartInst, loadCanopy); //Update navigation buttons on each page change 
   document.title = "PoliTree - Level 1: Canopy Questions";
}

function loadCanopy() {
    //Set up grid container, then dynamically generate six canopy-q-grid components to fill the grid
    document.getElementById('main-frame').innerHTML = `<div id=canopy-q-grid class=canopy-grid-container></div>`;
    generateCanopy.pull(1, generateCanopy.CANOPY1_QUESTION_COUNT, generateCanopy.CANOPY1_BLOCK_ANSWER_COUNT, generateCanopy.CANOPY1_QUESTION_OPTIONS); 
    updateNav(loadCanopyInst, loadCanopy2);
    generateCanopy.lockNav(); //Default the continue button to locked
};

function loadCanopy2() {
    //Score the Canopy Stage 1 Results
    canopyScore.init();
    canopyScore.focusTally(generateCanopy.CANOPY1_QUESTION_COUNT);
    canopyScore.approachTally(generateCanopy.CANOPY1_QUESTION_COUNT, generateCanopy.CANOPY1_BLOCK_ANSWER_COUNT);
    const followupQ = canopyScore.funnel();
    
    //Generate the Canopy Stage 2 Questions
    document.getElementById('main-frame').innerHTML = `<div id=canopy-q-grid class=canopy-grid-container></div>`;
    generateCanopy.pull(2, generateCanopy.CANOPY2_QUESTION_COUNT, generateCanopy.CANOPY2_BLOCK_ANSWER_COUNT, followupQ.length, followupQ);
    updateNav(loadCanopy, loadLeaf); 
    generateCanopy.lockNav(); //Default the continue button to locked
}

function loadLeaf(testFlag = true) {
    let leafID;
    if (testFlag) { //Only score if this is in the context of a test
        leafID = canopyScore.finalize();
        sessionStorage.setItem("currentLeaf", leafID); //Pass result into sessionStorage for further use
    }
    else {
        leafID = sessionStorage.getItem("currentLeaf");
    }
    //Load the leaf given the result
    const leaf = importJSON.find(leafID);
    const leafHTML = loadCard("The Canopy: Results", renderLeaf.template);
    document.getElementById('main-frame').innerHTML = leafHTML;
    const leafCard = renderLeaf.fill(leaf);
    updateNav(loadCanopyInst, loadTrunkInst);
}

function loadTrunkInst() {
    const trInstHTML = loadCard("Level 2: The Trunk (Instructions)",trunkInst);
    document.getElementById('main-frame').innerHTML = trInstHTML;
    updateNav(loadLeaf, loadTrunkQ); //Update navigation buttons on each page change
    document.title = "PoliTree - Level 2: Trunk Questions"
    
    //Placeholder logic to reset session storage until I implement proper going back, ensure stuff resets 
    const currentLeaf = sessionStorage.getItem("currentLeaf");
    sessionStorage.clear() //THIS MAY BREAK CONFIG JSON WILL HAVE TO SEE LATER
    sessionStorage.setItem("currentLeaf", currentLeaf); //Preserve the currentLeaf for later
}

function loadTrunkQ() {
    const leafID = sessionStorage.getItem("currentLeaf");
    const trunkHTML = loadCard("Level 2: The Trunk",trunkQ);
    document.getElementById('main-frame').innerHTML = trunkHTML;
    renderTrunk.init(leafID);
    updateNav(loadTrunkInst, renderTrunk.loadNextQ); //Update navigation buttons on each page change 
    toggleNavLock(document.getElementById('forwardNav'));
}

export function loadBranch(branch) {
    //Get the branch ID, load the template, and then pass the values into the render function
    const branchHTML = loadCard("PoliTree: Final Results", renderBranch.template);
    document.getElementById('main-frame').innerHTML = branchHTML;
    const leafCard = renderBranch.fill(branch);
    //May not need sessionStorage as this isn't an onClick passing an argument
    //Find result, pull template, fill template
    //Delete navbar 
}

function loadBeta() {
    const betaHTML = loadCard("The rest of the test is unfinished", betaContent);
    document.getElementById('main-frame').innerHTML = betaHTML;
    updateNav(loadTrunkQ, loadBeta); //Update navigation buttons on each page change
    
}


function updateNav(newBack, newForward) {
    //Initialize the navbar if not initialized
    if (document.getElementById('test-nav').innerHTML === "") { 
        document.getElementById('test-nav').innerHTML = navBar;
    }
    if (currentForwardNav != undefined && currentBackNav != undefined) {
        document.getElementById("backNav").removeEventListener("click", currentBackNav);
        document.getElementById("forwardNav").removeEventListener("click", currentForwardNav);
    }

    document.getElementById("backNav").addEventListener("click", newBack);
    document.getElementById("forwardNav").addEventListener("click", newForward);
    
    //Ensure locks don't carry over
    document.getElementById("forwardNav").disabled = false; //Ensure that disables don't carry over
    if (document.getElementById("forwardNav").classList.contains("fa-lock")) {  
        document.getElementById("forwardNav").classList.remove("fa-lock");
        document.getElementById("forwardNav").classList.add("fa-arrow-circle-right");
    }

    currentBackNav = newBack;
    currentForwardNav = newForward;
}

//Locks the forward nav on question initialization, same logic as in lockNav() of renderTrunk and generateCanopy
function toggleNavLock(navButton) {
    if (!navButton.disabled) {
        navButton.disabled = true;
        navButton.classList.remove("fa-arrow-circle-right");
        navButton.classList.add("fa-lock");
    }
    else { 
        navButton.disabled = false;
        navButton.classList.remove("fa-lock");
        navButton.classList.add("fa-arrow-circle-right");
    }
}

//Allows for the loading of certain leaf/branch pages directly via URL parameters
function urlLoad() {
    sessionStorage.clear()
    const url = new URL(document.location.href);
    const leafQuery = url.searchParams.get('leaf'); //Directly jump to a leaf page
    const devQuery = url.searchParams.get('dev'); //If true, you will still have test navigation 
    const branchQuery = url.searchParams.get('result');    

    if (leafQuery) { //Ensure there's only one param passed, to reduce unexpected input
        const leafID = leafQuery; //User should specify leaf ID
        try {
            importJSON.find(leafID); //Verify that leaf exists 
            if (!leafID.startsWith("L-")) { //Verify that ID is a leaf ID
                throw new importJSON.InvalidURLError(`Not a leaf JSON ID: ${leafID}`); //Throw error if JSON ID of incorrect type
            }
            sessionStorage.setItem("currentLeaf", leafID);
            loadLeaf(false);
            //Unless dev is enabled, remove the nav so users aren't confused
            if (devQuery != "true") { 
                document.getElementById('test-nav').innerHTML = "";
                document.getElementById('continueMessage').innerHTML = "This is a working definition for a broad classification. One who is taking the test would normally be directed to continue from here to get a more precise result. <p>To take the test, click the site header to go to the main page.</p>";
            }
            return 1;
        }
        catch (err) 
        {   
            //If the error is due to wrong user input, return the custom errors and load main page
            if (err instanceof importJSON.InvalidURLError) { 
                console.error(err);
            }
            else { //Otherwise let the site break
                throw(err);
                return 1;
            }
        }
    }

    if (branchQuery) { 
        try {
            importJSON.find(branchQuery); //Verify that branch exists 
            if (!branchQuery.startsWith("B-")) { //Verify that ID is a branch ID
                throw new importJSON.InvalidURLError(`Not a branch JSON ID: ${branchQuery}`); //Throw error if JSON ID of incorrect type
            }
            loadBranch(branchQuery); 
            return 1;
        }
        catch (err) {   
            //If the error is due to wrong user input, return the custom errors and load main page
            if (err instanceof importJSON.InvalidURLError) { 
                console.error(err);
            }
            else { //Otherwise let the site break
                throw(err);
                return 1;
            }
        }
    }


    return 0; //If any of the checks fail, return 0
}


