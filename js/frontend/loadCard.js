//Render a card, given its body and title as parameters
export default function loadCard(title, content) {
    //Place body into the card container
    const divHead = `<div id=card-container>`;
    const divRibbon = `<div id=card-ribbon>`.concat(title, "</div>");
    const divContent = `<div id=card-content>`.concat(content,"</div>");
    const divEnd = `</div>`;
    return divHead.concat(divRibbon,divContent,divEnd)
}
