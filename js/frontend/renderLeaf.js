export {default as template} from  '../../components/leafPage.js';
import * as importJSON from '../backend/importJSON.js'; 

export function fill(leaf) {
    fillHead(leaf);
    fillQuote(leaf.quote);
    fillNotes(leaf.description);
    fillIcon(leaf.icon);
    document.title = leaf.name.concat(" - PoliTree");
}


//Fill the top sentence with the leaf/cluster names
function fillHead(leaf) {
    const otherFlag = (leaf.focus.length + leaf.approach.length)/2; //If Other-clustered, will be 2 otherwise 1. If neither, code is bugged somewhere. 
    
    //Fill in leaf name
    const leafDiv = document.getElementById('leafName');
    leafDiv.innerHTML = leaf.name;

    const cluster1 = importJSON.find(leaf.approach[0]); //Get cluster info from ID

    if (otherFlag == 1) {
        //Single-focus leaves get color from Focus
        const focus = importJSON.find(leaf.focus[0]); 
        leafDiv.setAttribute('style', `color:${focus.color}`); 
        //Pull in cluster information alongside grammar for single-focus leaves
        const clusterInfo = `within the <span id=cluster1>${cluster1.name}</span> cluster.`;
        document.getElementById('clusterInfo').outerHTML = clusterInfo;
        //Style the clusters
        document.getElementById('cluster1').setAttribute('style', `color:${cluster1.color}`);
        document.getElementById("cluster1").className += "keyName"; //Set it as keyName so it gets underlined
    }

    else if (otherFlag == 2) { 
        const cluster2 = importJSON.find(leaf.approach[1]);
        leafDiv.setAttribute('style', `color:${leaf.color}`); //Other-clustered leaves have a direct color property
        //Other-clustered leaves have two clusters, and corresponding grammar
        const clusterInfo = `between the <span id=cluster1>${cluster1.name}</span> and <span id=cluster2>${cluster2.name}</span> clusters.`;
        document.getElementById('clusterInfo').outerHTML = clusterInfo;
        //Style the clusters
        document.getElementById('cluster1').setAttribute('style', `color:${cluster1.color}`); 
        document.getElementById("cluster1").className += "keyName"; 
        document.getElementById('cluster2').setAttribute('style', `color:${cluster2.color}`);
        document.getElementById("cluster2").className += "keyName"; 
    }
}

//Fill in the quote associated with the leaf
function fillQuote(quote) {
    document.getElementById('leafQuote').innerHTML = quote;
}


function fillNotes(notes) {
    document.getElementById('leafNotes').innerHTML = ""; //Reset the contents before appending
    //For each note, append the HTML as a <li> tag
    for (const n of notes) {
        document.getElementById('leafNotes').innerHTML += `<li>${n}</li>`  
        //Keywords are currently hardcoded, this may need to be fixed
    }
}


function fillIcon(image) {
    const configFile = importJSON.find("J-CONFIG");
    const imgPath = configFile['icon-path'] + image;
    document.getElementById("leafIcon").src = imgPath;
}
