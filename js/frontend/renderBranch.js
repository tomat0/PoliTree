export {default as template} from  '../../components/branchPage.js';
import bookItem from '../../components/bookItem.js';
import * as importJSON from '../backend/importJSON.js';

export function fill(bID) {
    const branch = importJSON.find(bID);
    fillPageInfo(branch);
    document.title = "PoliTree - ".concat(branch.name);
    document.getElementById("test-nav").innerHTML = ''; //Hide navbar to show test is done
}


//Fill the top-level info for the page, not the books
function fillPageInfo(branch) {
    //Fill in the branch names all over the page
    const bNameInstances = document.getElementsByClassName("branchName");
    for (let b of bNameInstances) {
        b.innerHTML = branch.name;
    }
    //Fill in the leaf names all over the page
    const lNameInstances = document.getElementsByClassName("leafName");
    const leaf = importJSON.find(branch.leaf);
    for (let l of lNameInstances) {
        l.innerHTML = leaf.name;
    }
    fillClusters(leaf); //Fill in the cluster names in the heading
    document.getElementById("branch-definition").innerHTML = branch.definition; //Fill in definition
    fillBullet(branch.notes, "branch-notes"); //Fill in notes
    if (Object.keys(branch.subbranches).length == 0) {
        document.getElementById("subbranch-block").style.display = "none";
    }
    else {
        fillBullet(Object.keys(branch.subbranches), "subbranch-list"); //Fill in sub-branches
    }
    //Add in the book items for the lists 
    addBooks(leaf, "leaf-book-list")
    addBooks(branch, "branch-book-list"); 

    //Add eventListeners
    let collList = document.getElementsByClassName('collapsible');
    for (let i = 0; i < collList.length; i++) {
	    collList[i].addEventListener('click', setCollapsible, false);
    }
}


//Logic for filling in clusters, copied from renderLeaf.js' fillHead() function
function fillClusters(leaf) {
    const otherFlag = (leaf.focus.length + leaf.approach.length)/2; //If Other-clustered, will be 2 otherwise 1. If neither, code is bugged somewhere. 
    const cluster1 = importJSON.find(leaf.approach[0]); //Get cluster info from ID

    if (otherFlag == 1) {
        //Single-focus leaves get color from Focus
        const focus = importJSON.find(leaf.focus[0]); 
        document.getElementById("headerLeafName").setAttribute('style', `color:${focus.color}`); 
        //Pull in cluster information alongside grammar for single-focus leaves
        const clusterInfo = `within the <span id=cluster1>${cluster1.name}</span> cluster.`;
        document.getElementById('clusterInfo').outerHTML = clusterInfo;
        //Style the clusters
        document.getElementById('cluster1').setAttribute('style', `color:${cluster1.color}`);
        document.getElementById("cluster1").className += "keyName"; //Set it as keyName so it gets underlined
    }
    else if (otherFlag == 2) { 
        const cluster2 = importJSON.find(leaf.approach[1]);
        document.getElementById("headerLeafName").setAttribute('style', `color:${leaf.color}`); //Other-clustered leaves have a direct color property
        //Other-clustered leaves have two clusters, and corresponding grammar
        const clusterInfo = `between the <span id=cluster1>${cluster1.name}</span> and <span id=cluster2>${cluster2.name}</span> clusters.`;
        document.getElementById('clusterInfo').outerHTML = clusterInfo;
        //Style the clusters
        document.getElementById('cluster1').setAttribute('style', `color:${cluster1.color}`); 
        document.getElementById("cluster1").className += "keyName"; 
        document.getElementById('cluster2').setAttribute('style', `color:${cluster2.color}`);
        document.getElementById("cluster2").className += "keyName"; 
    }
}


//Copied from renderLeaf.js' fillNotes() function
function fillBullet(source, div) {
    document.getElementById(div).innerHTML = ""; //Reset the contents before appending
    //For each note, append the HTML as a <li> tag
    for (const n of source) {
        document.getElementById(div).innerHTML += `<li>${n}</li>`  
    }
}

//Add books given the list div and the book IDs
function addBooks(ideology, listDiv) {
    //Generate ID of the newly constructed bookItem
    const bookIDs = ideology.references;
    let newIdTemplate, newCollIdTemplate;   
    if (listDiv == "leaf-book-list") {
        newIdTemplate = "book-item-l-";
        newCollIdTemplate = "book-category-l-"; //ID structure for custom categories
    }
    else if (listDiv == "branch-book-list") {
        newIdTemplate = "book-item-b-";
        newCollIdTemplate = "book-category-b-"; 
    }
    
    let additionalRefs = []; //Array to track references to get in later loops
    let bookDivs = []; //Array of bookItems which we will then use to add event listeners

    //Add in the bookItem and set its ID
    for (const b in bookIDs) {
        const book = importJSON.find(bookIDs[b]);
        if (book.subsection == true) {
            additionalRefs.push(bookIDs[b]);
        }
        //First add in books which aren't under additional references
        else {
            const newId = newIdTemplate.concat(b); 
            document.getElementById(listDiv).innerHTML += bookItem;
            document.getElementById("book-item-tmp").id = newId;
            fillBookFields(newId, bookIDs[b]); //Fill in each book's fields
        }
    }
    //Add in the books which are classified under "Additional References"
    //Leaf/Branch JSON needs to have both "reference-categories" and "additional-references" to work

    const defaultColl = "Additional references pertaining to more specific tendencies...";
    //First add in the additional reference categories with their IDs  
    if (ideology["reference-categories"].length) { //Need a separate loop
        for (const c in ideology["reference-categories"]) {
            const collText = defaultColl.replace("more specific tendencies", ideology["reference-categories"][c])
            const newCollId = newCollIdTemplate.concat(c);
            let collList = createAdditionalRefCategory(collText, newCollId, listDiv);
            
            //Then fill them in with their respective books    
            for (const b of ideology["additional-references"][c]) {
                //Create new book, give it a unique ID, then fill it
                fillAdditionalRefCategory(collList,newCollId, newIdTemplate, b);
            }
        }
    }
    //Logic for default-categorized additional refs
    else if (additionalRefs.length) { //Code only allows for default collapsibles or custom ones, not both. If default is selected, only one allowed
        const newCollId = newCollIdTemplate.concat("D"); //D is the identifier for defaults
        const collList = createAdditionalRefCategory(defaultColl, newCollId, listDiv);
        for (const b of additionalRefs) {
            fillAdditionalRefCategory(collList, newCollId, newIdTemplate, b);
        }
    }
}

//Create a collapsible for each reference-category
function createAdditionalRefCategory(collText, collId, listDiv) { 
    //Collapsible div
    let addColl = document.createElement("div");
    addColl.id = collId;
    
    //Button for collapsible header
    let collButton = document.createElement("button"); 
    collButton.classList.add("collapsible");

    let collMarker = document.createElement("span");
    collMarker.classList.add("collapse-marker")
    collMarker.innerHTML = "+";
    let collTitle = document.createElement("span");
    collTitle.classList.add("additional-collapsible-title");
    collTitle.innerHTML = collText;

    //Collapsible content
    let collList = document.createElement("div");
    collList.classList.add("additional-book-list");
    collList.classList.add("collapsible-content");
    const catId = collId.slice(-3);
    const listId = "additional-book-list-".concat(catId);
    collList.id = listId;

    //Add title stuff onto button, then button and content div, then add div to list
    collButton.appendChild(collMarker);
    collButton.appendChild(collTitle);
    
    addColl.appendChild(collButton);
    addColl.appendChild(collList);
    document.getElementById(listDiv).appendChild(addColl); 

    return collList; //Return the list div so we can add books to it
}

//Fills in books for additional reference subsections
function fillAdditionalRefCategory(collList, newCollId, newIdTemplate, b) {
    collList.innerHTML += bookItem;
    const catId = newCollId.slice(-1);
    const aBookTemplate = newIdTemplate.concat("a").concat(catId);
    const aNewId = aBookTemplate.concat(b);
    document.getElementById("book-item-tmp").id = aNewId;
    fillBookFields(aNewId, b);
}

function fillBookFields(bookDivId, bookId) {
    //Get the field divs
    const bookDiv = document.getElementById(bookDivId);
    const titleField = bookDiv.getElementsByClassName("book-title")[0]; 
    const pdfButton = bookDiv.getElementsByClassName("pdf-button")[0];
    const epubButton = bookDiv.getElementsByClassName("epub-button")[0];
    const authorField = bookDiv.getElementsByClassName("book-author")[0];
    const lengthField = bookDiv.getElementsByClassName("book-length")[0];
    const difficultyField = bookDiv.getElementsByClassName("book-difficulty")[0];
    const genreField = bookDiv.getElementsByClassName("book-genre")[0]; 
    const typeField = bookDiv.getElementsByClassName("book-type")[0];
    const summaryField = bookDiv.getElementsByClassName("book-summary")[0];
    //Fill in the field divs
    const book = importJSON.find(bookId);
    titleField.innerHTML = book.title;
    generateBadge(book.pdf, pdfButton);
    generateBadge(book.epub, epubButton);
    authorField.innerHTML = book.author;
    lengthField.innerHTML = generateStars(book.length);
    difficultyField.innerHTML = generateStars(book.difficulty); 
    genreField.innerHTML = book.genre;
    typeField.innerHTML = book.type;
    summaryField.innerHTML = book.summary; 
}

//Converts raw number values (in increments of 0.5) as specified by the JSON into unicode stars for presentation
function generateStars(number) {
	if (number == 0) { 	//0 stars signals unknown difficulty
		return `<i>This book has not been vetted yet.</i>`
	}
    const fullStar = "&#9733;"
    const halfStar = "&#x2606;"
    const starInt = Math.floor(number);
    const starFrac = number - starInt;
    
    let stars = "";
    //Add one star per int
    for (const x of Array(starInt).keys()) {
        stars = stars.concat(fullStar);
    }
    
    //If there is a half-value, add a half-star
    if (number-starInt) {
	    stars = stars.concat(halfStar);
    }
    return stars;
}

function generateBadge(hash, div) {
    //Only add a button if the JSON has a link to provide
    if (hash != "") {
        const config = importJSON.find("J-CONFIG");
        const url = config['gateway'].concat(hash);
        div.href = url;
    }
    else {
        div.remove();
    }
}
//Logic for allowing the books to collapse in and out
//Function pulled from the old site, which was likely copied from Stack Overflow years ago. The source is unknown, but it works. 
function setCollapsible() {
    const bookDiv = this.parentElement;
    const markerDiv = this.getElementsByClassName("collapse-marker")[0];

    let content = bookDiv.getElementsByClassName("collapsible-content")[0];
    if (content.style.display === "grid") {
        content.style.display = "none"; 
        markerDiv.innerHTML = "+";
    } 
    else {
        content.style.display = "grid";
        markerDiv.innerHTML = "-";
    }
}



