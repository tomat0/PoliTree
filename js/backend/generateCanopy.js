import * as importJSON from '../backend/importJSON.js';   
import * as genCBlock from '../../components/canopyBlock.js';
import * as rand from './randHelper.js';

//Set up a global variable for canopy question/answer counts which is updated depending on passed parameters
var CANOPY_QUESTION_COUNT = 0; // Number of question blocks per page
var CANOPY_BLOCK_ANSWER_COUNT = 0; //Number of answers per question block
var CANOPY_QUESTION_OPTIONS = 0; //Number of questions per question block

// Presets for the question/answer count for each canopy page
export const CANOPY1_QUESTION_COUNT = 6; 
export const CANOPY1_BLOCK_ANSWER_COUNT = 3; 
export const CANOPY1_QUESTION_OPTIONS = 3;

export const CANOPY2_QUESTION_COUNT = 1;
export const CANOPY2_BLOCK_ANSWER_COUNT = 3;


//Sort the canopy questions into different bins per focus, so we can pull from each bin when drawing questions
function sort() {
    let allQ = importJSON.getCanopy();
    let polQ = [];
    let ideQ = [];
    let matQ = [];
    let othQ = [];
    let tmpQ = []; //Temp buffer to put non-other questions into since directly removing othQ from allQ messes up the for loop
   
    for (const q of allQ) {
        if (q.focus[0] == 'C-OTH') {
            othQ.push(q);
        }
        else {
            tmpQ.push(q);
        }
    }
    allQ = tmpQ;
    for (const q of allQ) {
        switch (q.focus[0]) {
            case "C-POL":
                polQ.push(q);
                break;
            case "C-IDE":
                ideQ.push(q);
                break;
            case "C-MAT":
                matQ.push(q);
                break;
        }
    }
    return [[polQ,ideQ,matQ],othQ];
}

// Logic for drawing Canopy 1 questions: For each question block, randomly draw three questions, one of each focus.
function drawCanopy1(arg) {
    const fullQ = sort();
    const sortedQ = fullQ[0];
    let othQ = fullQ[1];
    let drawnQ = []; //This is the list which will contain the pulled questions
    for (let i = 0; i < CANOPY_QUESTION_COUNT-1; i++) {
        drawnQ.push([]); //Give new bin for each question block
        for (const q of sortedQ) {
            let r = rand.integer(q.length);
            drawnQ[i].push(q[r]);
            q.splice(r,1);
        }
    }

    //Ensure questions are rand.shuffled before returning
    drawnQ.push(othQ);
    for (let l of drawnQ) {
        l = rand.shuffle(l);
    }
    return drawnQ;
}

function drawCanopy2(focusArr) {
   let drawnQ = [];
   const allQ = rand.shuffle(importJSON.getCanopy());
   // For each focus in the focusArr, we're going to draw a question to put into the drawnQ
   for (const f of focusArr) {
        if (f.startsWith("C-")) {
            let tmpQ = [];
            for (const q of allQ) {
                if (q.focus[0] == f) {
                    tmpQ.push(q);
                }
            }
            const r = rand.integer(tmpQ.length);
            let pullItem = tmpQ[r];
            tmpQ = tmpQ.filter(item => item !== pullItem); //Prematurely filter out to avoid duplicates - not a de-dupe
            drawnQ.push(pullItem);
        }
        //For other-clustered Focuses, separate check is needed
        else if (f.startsWith("L-")) {
            let tmpQ = [];
            for (const q of allQ) {
               if (q.focus[0] == "C-OTH") {
                    if (q.focus[1] == f) {
                        tmpQ.push(q);
                    }
               }
            }
            tmpQ = dedupe(tmpQ, drawnQ); //Ensure there's no duplicates between the first set of draws and this one
            
            //If an other-clustered focus has multiple questions a random one will be sent to follow-up
            //Original intention was to send all to followup but it doesn't work as the qoCount parameter passed in can't tell if multiple are coming
            const r = rand.integer(tmpQ.length);
            drawnQ.push(tmpQ[r]);
        }
   }
   drawnQ = [drawnQ];
   return drawnQ;
}

//Remove duplicate elements from array
function dedupe(arr, refArr) {
    //Check all elements in the refArr
    let tmpArr = [];
    for (const i in arr) {
    	if (!refArr.includes(arr[i])) {
          tmpArr.push(arr[i])
      }
    }
    return tmpArr;
}

//Create a question block and give it unique ids
function init(num) {
    iter = num.toString();
}

export function pull(stage, qbCount, abCount, qoCount, focusArr) { 
    //Update parameters to reflect page presets
    CANOPY_QUESTION_COUNT = qbCount;
    CANOPY_BLOCK_ANSWER_COUNT = abCount;
    CANOPY_QUESTION_OPTIONS = qoCount;
    
    //Draw the random assortment of questions to pull into the divs
    let drawnQ = [];
    switch (stage) {
        case 1:
            drawnQ = drawCanopy1();
            break;
        case 2:
            drawnQ = drawCanopy2(focusArr);
            break;
    }

    //List of placeholder div IDs to replace
    const idPH = genCBlock.divIDs(CANOPY_QUESTION_OPTIONS, CANOPY_BLOCK_ANSWER_COUNT);
    
    //Generate the question blocks
    for (let j=0;j < CANOPY_QUESTION_COUNT; j++) {    
        //Create the block, then dynamically create and replace the element IDs
        let iter = j.toString();
        
        //Need one additional slot for the final question block on page 1, VERY hacky solution
        document.getElementById('canopy-q-grid').innerHTML += genCBlock.template;
        if ((stage == 1) && (j == 5)) {
            genCBlock.qSelect(CANOPY_QUESTION_OPTIONS+1);
            idPH.push("q3-cX");
        }
        //Create template block with its components
        else {
            genCBlock.qSelect(CANOPY_QUESTION_OPTIONS);
        }
        genCBlock.ansBlock(CANOPY_BLOCK_ANSWER_COUNT); 
        
        //Fill in the IDs and content of the block template  
        let cDraw = drawnQ.shift(j);
        idPH.forEach((i) => {
            // If it's a question we should set the value and innerHTML to reflect what question option it is
            if (i.startsWith("q")) {
                let index = parseInt(i.charAt(1));
                document.getElementById(i).value = cDraw[index].id;
                document.getElementById(i).innerHTML = cDraw[index].question;
            }
            // If it's an answer we should set the value and innerHTML equal to the same thing (the answer value)
            else if (i.startsWith("a")) {
                let index = parseInt(i.charAt(1));
                // Set our label attributes
                document.querySelector(`#${i} ~ label`).innerHTML = genCBlock.DEFAULT_ANS;
                document.querySelector(`#${i} ~ label`).setAttribute('for', i.replace("X",iter));
                
                // Set our actual input el
                document.getElementById(i).setAttribute('value', genCBlock.DEFAULT_VAL);
                document.getElementById(i).setAttribute('name', 'c-a-block-X'.replace('X', j));
            }
            document.querySelector(`#${i}`).setAttribute('id', i.replace("X",iter)); 
        });
    }
    
    //Add eventListener to update answers every time a new question is selected
    for (let i=0;i < CANOPY_QUESTION_COUNT; i++) {
        let qId = "c-q-block-X".replace("X", i);
        document.getElementById(qId).addEventListener("change", updateAnswers);
    }
    //Add eventListener to update lock on every answer input
    for (const a of document.querySelectorAll('input[type=radio]')) {
        a.addEventListener("input", lockNav);
    }
}
//Every time a question is selected, switch the answers
function updateAnswers() {
    let questionId = this.id;
    let blockId = questionId.slice(-1);
    let answerId = "aX-cY".replace("Y",blockId);  
    let qJSON;

    //Look up the question if this is not default
    if (this.value != genCBlock.DEFAULT_VAL) { 
        qJSON = importJSON.find(this.value);
    }
    //Depending on what the question is, look up the JSON and pull corresponding answers
    for (let i = 0; i < CANOPY_BLOCK_ANSWER_COUNT; i++) {
        let a = answerId.replace("X",i);
        let label;
        let value;

        //If on default question, reset to blank questions and disable forward
        if (this.value == genCBlock.DEFAULT_VAL) {
            label = genCBlock.DEFAULT_ANS;
            value = genCBlock.DEFAULT_VAL;
        }
        else {
            label = qJSON.answers[i].answer;
            value = qJSON.answers[i].answer;
        }
        document.querySelector(`#${a} ~ label`).innerHTML = label; 
        document.getElementById(a).setAttribute('value', value); // Set our actual input el
        document.getElementById(a).checked = false; //Reset selection on each change
    }
    lockNav(); //Ensure nav is locked or unlocked depending on selections
}


//If on default, lock the navigation
export function lockNav() {
    let completed = true; //Flag to track if anything is incomplete
    const grid = document.getElementById('canopy-q-grid'); 
    //First check to see if any questions are unselected 
    for (const q of grid.getElementsByClassName('canopy-q-block-question')) {
        if (q.value == "blank") {
            completed = false;
        }
    }
    //Go through each answer block, if there's a block without any selections, incomplete
    for (const ab of grid.getElementsByClassName('canopy-q-block-answer')) {
        let checked = false;
        for (const a of ab.getElementsByTagName('input')) {
            if (a.checked) {
                checked = true;
            }
        }
        if (!checked) {
            completed = false;
        }
    }

    //Once you're done with the checks, either enable or disable the continue button
    const navButton = document.getElementById("forwardNav");
    if (completed) { //Update icon and enable/disable button 
        navButton.disabled = false;
        navButton.classList.remove("fa-lock");
        navButton.classList.add("fa-arrow-circle-right");
    }
    else { 
        navButton.disabled = true;
        //Switch icon to locked
        navButton.classList.remove("fa-arrow-circle-right");
        navButton.classList.add("fa-lock");
    }
}
