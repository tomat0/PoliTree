// Helper function to generate random integers
export function integer(max) {
    return Math.floor(Math.random() * max);
}

/*
   Helper function to shuffle arrays: credit to AJ Oneal (coolaj86)

       https://stackoverflow.com/users/151312/coolaj86
       https://coolaj86.com/
*/
export function shuffle(array) {
  let currentIndex = array.length,  randomIndex;

  // While there remain elements to shuffle.
  while (currentIndex > 0) {

    // Pick a remaining element.
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex], array[currentIndex]];
  }

  return array;
}
