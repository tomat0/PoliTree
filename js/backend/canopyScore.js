import * as importJSON from '../backend/importJSON.js';   


//Scores are stored in session storage, this function sets up the structure to update
export function init() {
    //Pull focuses and approaches
    const focuses = importJSON.getClusters('focus');
    const approaches = importJSON.getClusters('approach');
    

    //Treat Focus information as a dict, which we will later store
    for (const f of focuses) {
        var fDict = new Object();
        fDict.score = 0;
        fDict.approaches = {};

        //Give each approach a global score and one local to each focus
        for (const a of approaches) {
            sessionStorage.setItem(a.id, 0);
            fDict.approaches[a.id] = 0;
        }
        sessionStorage.setItem(f.id, JSON.stringify(fDict));
        //Set properties for the Other leaves
        for (const l of importJSON.getLeaves("C-OTH")) {
            sessionStorage.setItem(l.id, 0);
        }
    }
    
}

//Calculate scores for each question/answer
export function focusTally(qCount) {  //The answers always follow an ordering format of [C-LFT, C-LIB, C-RIG] 
    for (let i=0;i < qCount; i++) {
        const selectId = "c-q-block-X".replace("X", i);  
        const question = importJSON.find(document.getElementById(selectId).value);
        //Determine Focus by checking focus of each selected question
        if (question.focus[0] != "C-OTH") { //For questions with an Other focus, the focus is answer dependent, logic is in approachTally()
            let fDict = JSON.parse(sessionStorage.getItem(question.focus[0])); //Have to convert back to JSON to get values
            let nextVal = fDict.score + question.weight;
            fDict.score = nextVal;
            sessionStorage.setItem(question.focus[0],JSON.stringify(fDict));
        }
    }
}

//Calculate scores for each focus 
export function approachTally(qNum, aCount) { 
    for (let q=0;q < qNum; q++) {
        //Get selected question, will be used later
        const selectQId = "c-q-block-X".replace("X", q); 
        const question = importJSON.find(document.getElementById(selectQId).value);

        for (let a=0;a < aCount; a++) {
            //Identify which answer is selected, then pull the answer object info from the JSON dicts
            const selectAId = "aY-cX".replace("Y", a).replace("X", q);
            if (document.getElementById(selectAId).checked) {
                const aId = document.getElementById(selectAId).value;
                const aDict = importJSON.getAnswer(question.id,aId);

                for (const p of aDict.approach) {
                    //Increment global counters for Approach
                    let gNext = parseInt(JSON.parse(sessionStorage.getItem(p))) + aDict.weight;
                    sessionStorage.setItem(p,gNext);
                    
                    //Implement Focus-specific counters for Approach 
                    const f = question.focus[0];
                    if (f == "C-OTH") { //If question under Other cluster, use per-answer focus
                        for (const fs of aDict.focus) {
                            let fDict = JSON.parse(sessionStorage.getItem(fs));
                            if (fs.startsWith("C-")) { //Different logic for leaf-focuses, and cluster ones
                                fDict.score += question.weight;
                                fDict['approaches'][p] += aDict.weight;
                                sessionStorage.setItem(fs,JSON.stringify(fDict));
                            }
                            else if (fs.startsWith("L-")) {
                                let fVal = parseInt(sessionStorage.getItem(fs)) + (aDict.weight*question.weight); //The leaf-weights are a product of per-answer weight and per-question weight
                                sessionStorage.setItem(fs, fVal);
                            }
                        }   
                    }
                    else {
                        let fDict = JSON.parse(sessionStorage.getItem(f));
                        fDict['approaches'][p] += aDict.weight;
                        sessionStorage.setItem(f,JSON.stringify(fDict));
                    }
                } 
            }
        }
    }
}

//Maximum of five questions make it to followup, logic for selection has multiple stages
export function funnel() {
    let funnelSet = [];
    let leafFunnel = [];
    
    //Stage 1: Draw questions for regular-clustered Focuses by proportional share of answers (should be up to 3)
    let fScore = {};
    let fSum = 0; 
    for (const f of importJSON.getClusters('focus')) {
        fScore[f.id] = JSON.parse(sessionStorage.getItem(f.id)).score;
        fSum += JSON.parse(sessionStorage.getItem(f.id)).score;
    }
    //Map percentage of focuses to question amounts
    for (const k of Object.keys(fScore)) {
        const percent = fScore[k]/fSum;
        //Map the percentage as shares of 6 to a total of 3 questions on the output
        if (percent < 0.16) { //Between 0/6 and 1/6, return 0 questions
            //pass
        }
        else if (percent < 0.50) { //Between 1/6 and 3/6, return 1 question
            funnelSet.push(k);
        }
        else if (percent < 0.84) { //Between 3/6 and 5/6, return 2 questions
            funnelSet.push(k);
            funnelSet.push(k);
        }
        else { //Between 5/6 and 6/6, return 3 questions
            funnelSet.push(k);
            funnelSet.push(k);
            funnelSet.push(k);
        }

    }    
    const tieSet = getMax(fScore,0); //Checks for ties
    //Stage 2: Check for two-way Focus ties, and get corresponding Other-clustered leaf 
    if (tieSet.length == 2) {
       const checkSet = tieSet;
       for (const l of importJSON.getLeaves("C-OTH")) {
           if (compareContents(checkSet, l.focus)) {
               funnelSet.push(l.id);
           }
       }
    }
    //Stage 2.5: Do the same check for two-way Approach ties
    let aScore = {};
    for (const a of importJSON.getClusters('approach')) {
        aScore[a.id] = sessionStorage.getItem(a.id); //Using global answer score
    }
    let aList = getMax(aScore,0);
    if (aList.length == 2) {
        for (const l of importJSON.getLeaves("C-OTH")) {
            if (compareContents(aList, l.approach)) {
                funnelSet.push(l.id);
            }
        }
    }
    
    //Stage 3: Pick maximum from the Other-clustered Focuses (at most 1)
    let lScore = {}; 
    for (const l of importJSON.getLeaves('C-OTH')) {
        lScore[l.id] = sessionStorage.getItem(l.id);
    }
    funnelSet = funnelSet.concat(getMax(lScore, 0));
    
    return funnelSet;
}

//Return the canopy result for the given user's inputs
export function finalize() {
    //Currently hardcoded to assume one block in runoff
    const cBlock = 'c-q-block-0';
    const aBlock = 'c-a-block-0';

    //Find leaf as intersection of focus and approach in follow-up question
    const question = importJSON.find(document.getElementById(cBlock).value);
    let focus = question.focus; //Currently coded to always assume 1 block in the runoff
    let answer;
    let approach;
    
    //Find current answer and approach of current answer    
    for (const a of document.getElementById(aBlock).children) {
        const ans = a.children[0];
        if (ans.checked) {
            answer = importJSON.getAnswer(question.id, ans.value)
            approach = answer.approach;
        }
    }
    //If answered one of the triggers for a Other-clustered question, return it straight
    if (Object.hasOwn(answer, 'focus')) {
        if (answer.focus[0].startsWith("L-")) {
            return answer.focus[0]; 
        }
        else if (answer.focus[0].startsWith("C-")) {
            focus = answer.focus;
        }
    }
    approach = finalizeApproach(focus, approach);
    //Otherwise search for a focus/intersection match
    for (const l of importJSON.getLeaves("all")) {
        if (compareContents(l.approach,approach) && compareContents(l.focus,focus)) { //Currently will only return an incomplete estimate which doesn't really account for Other-clustered responses
            return l.id;
        }
    }
}

function finalizeApproach(cFocus, cApproach) {
    
    //Stage 1: First look for highest scoring approach on the selected focus
    const fDict = JSON.parse(sessionStorage.getItem(cFocus));
    const approachList = importJSON.getClusters("approach"); 
    let maxList = [];
    let maxDict = {};
    for (const a of approachList) {
        maxDict[a.id] = fDict.approaches[a.id];
    }
    maxList = getMax(maxDict);
    //If a tie, proceed to next stage
    if (maxList.length < 2) {
        return maxList;
    }

    //Stage 2: If tied, move to global measures between the maxes
    else {
        maxDict = {};
        for (const m of maxList) {
            maxDict[m] = parseInt(sessionStorage.getItem(m));
        }
        maxList = getMax(maxDict);
        if (maxList.length < 2) {
            return maxList;
        }
    //Stage 3: If still tied, keep the answer to the selected question
        else {
            return cApproach;
        }
    }
}


//Helper function - for a given dictionary with numerical keys, return the maximum items
function getMax(dict, offset) {
    let curMax = 1;
    let maxList = [];

    for (const k of Object.keys(dict)) {
        if (dict[k] > curMax) {
            maxList = [k];
            curMax = dict[k];
        }
        else if (dict[k] == curMax) {
            maxList.push(k);
        }
    }
    //Will do nothing if offset is zero, but in Stage 4, offset is non-zero
    for (const k of Object.keys(dict)) { 
        if (dict[k] >= curMax - offset && dict[k] > 0) {
            maxList.push(k);
        }
    }
    return [...new Set(maxList)]; //Convert to a set and back to remove duplicate elements
}

/* Helper function - compares two arrays to see if contents are identical.
    * Modified based on code snippet written in GeeksForGeeks:
    * https://www.geeksforgeeks.org/how-to-compare-two-arrays-in-javascript/ 
*/

function compareContents(a,b) {
    a = [...new Set(a)].sort();
    b = [...new Set(b)].sort();
    const compare = (a, b) =>
        a.length === b.length &&
        a.every((element, index) => element === b[index]);
 	return compare(a,b);
}
