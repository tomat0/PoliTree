import J_CONFIG from '../../json/js/config.js';
import B_SLIB from '../../json/js/branches/B-SLIB.js';
import B_MINR from '../../json/js/branches/B-MINR.js';
import B_MUTL from '../../json/js/branches/B-MUTL.js';
import B_NEOL from '../../json/js/branches/B-NEOL.js';
import B_IDEA from '../../json/js/branches/B-IDEA.js';
import B_REAL from '../../json/js/branches/B-REAL.js';
import B_COSM from '../../json/js/branches/B-COSM.js';
import B_PLUR from '../../json/js/branches/B-PLUR.js';
import B_SYNC from '../../json/js/branches/B-SYNC.js';
import B_PALO from '../../json/js/branches/B-PALO.js';
import B_PATR from '../../json/js/branches/B-PATR.js';
import B_THEO from '../../json/js/branches/B-THEO.js';
import B_ELIT from '../../json/js/branches/B-ELIT.js';
import B_TCHN from '../../json/js/branches/B-TCHN.js';
import B_MERT from '../../json/js/branches/B-MERT.js';
import B_IDNT from '../../json/js/branches/B-IDNT.js';
import B_FASC from '../../json/js/branches/B-FASC.js';
import B_MONR from '../../json/js/branches/B-MONR.js';
import B_SYND from '../../json/js/branches/B-SYND.js';
import B_VANG from '../../json/js/branches/B-VANG.js';
import B_LCOM from '../../json/js/branches/B-LCOM.js';
import B_ULFT from '../../json/js/branches/B-ULFT.js';
import B_PACF from '../../json/js/branches/B-PACF.js';
import B_LBMV from '../../json/js/branches/B-LBMV.js';
import B_NMRX from '../../json/js/branches/B-NMRX.js';
import B_POMO from '../../json/js/branches/B-POMO.js';
import B_EGOI from '../../json/js/branches/B-EGOI.js';
import B_PLFT from '../../json/js/branches/B-PLFT.js';
import B_DSOC from '../../json/js/branches/B-DSOC.js';
import B_CNAT from '../../json/js/branches/B-CNAT.js';
import B_LNAT from '../../json/js/branches/B-LNAT.js';
import B_LUDD from '../../json/js/branches/B-LUDD.js';
import B_FUTR from '../../json/js/branches/B-FUTR.js';
import B_NEOR from '../../json/js/branches/B-NEOR.js';
import B_THUM from '../../json/js/branches/B-THUM.js';
import B_ANCP from '../../json/js/branches/B-ANCP.js';
import B_SANR from '../../json/js/branches/B-SANR.js';
import B_UTOP from '../../json/js/branches/B-UTOP.js';
import B_SDEM from '../../json/js/branches/B-SDEM.js';
import B_LCON from '../../json/js/branches/B-LCON.js';
import B_RCNT from '../../json/js/branches/B-RCNT.js';
import C_LFT from '../../json/js/clusters/approach/C-LFT.js';
import C_LIB from '../../json/js/clusters/approach/C-LIB.js';
import C_RIG from '../../json/js/clusters/approach/C-RIG.js';
import C_IDE from '../../json/js/clusters/focus/C-IDE.js';
import C_MAT from '../../json/js/clusters/focus/C-MAT.js';
import C_POL from '../../json/js/clusters/focus/C-POL.js';
import L_LIBT from '../../json/js/leaves/L-LIBT.js';
import L_PROG from '../../json/js/leaves/L-PROG.js';
import L_INTR from '../../json/js/leaves/L-INTR.js';
import L_MODR from '../../json/js/leaves/L-MODR.js';
import L_POPL from '../../json/js/leaves/L-POPL.js';
import L_ACCL from '../../json/js/leaves/L-ACCL.js';
import L_NEWL from '../../json/js/leaves/L-NEWL.js';
import L_COMM from '../../json/js/leaves/L-COMM.js';
import L_INTG from '../../json/js/leaves/L-INTG.js';
import L_ARIS from '../../json/js/leaves/L-ARIS.js';
import L_NATN from '../../json/js/leaves/L-NATN.js';
import L_ANAR from '../../json/js/leaves/L-ANAR.js';
import Q_WLT from '../../json/js/questions/canopy/Q-WLT.js';
import Q_REG from '../../json/js/questions/canopy/Q-REG.js';
import Q_OWN from '../../json/js/questions/canopy/Q-OWN.js';
import Q_PRO from '../../json/js/questions/canopy/Q-PRO.js';
import Q_REL from '../../json/js/questions/canopy/Q-REL.js';
import Q_HIS from '../../json/js/questions/canopy/Q-HIS.js';
import Q_ETH from '../../json/js/questions/canopy/Q-ETH.js';
import Q_MED from '../../json/js/questions/canopy/Q-MED.js';
import Q_CON from '../../json/js/questions/canopy/Q-CON.js';
import Q_LAW from '../../json/js/questions/canopy/Q-LAW.js';
import Q_IDN from '../../json/js/questions/canopy/Q-IDN.js';
import Q_POW from '../../json/js/questions/canopy/Q-POW.js';
import Q_FAM from '../../json/js/questions/canopy/Q-FAM.js';
import Q_DEM from '../../json/js/questions/canopy/Q-DEM.js';
import Q_TCH from '../../json/js/questions/canopy/Q-TCH.js';
import Q_EQU from '../../json/js/questions/canopy/Q-EQU.js';
import Q_CMP from '../../json/js/questions/canopy/Q-CMP.js';
import Q_CNS from '../../json/js/questions/canopy/Q-CNS.js';
import Q_FUT from '../../json/js/questions/canopy/Q-FUT.js';
import F_MINRvANAR from '../../json/js/questions/filter/F-MINRvANAR.js';
import F_DEONvCNSQ from '../../json/js/questions/filter/F-DEONvCNSQ.js';
import F_HOMEvUSUF from '../../json/js/questions/filter/F-HOMEvUSUF.js';
import F_PRSCvDSCR from '../../json/js/questions/filter/F-PRSCvDSCR.js';
import F_MULTvUNIL from '../../json/js/questions/filter/F-MULTvUNIL.js';
import F_COMPvCOOP from '../../json/js/questions/filter/F-COMPvCOOP.js';
import F_AGREvDISG from '../../json/js/questions/filter/F-AGREvDISG.js';
import F_PRSNvSOCL from '../../json/js/questions/filter/F-PRSNvSOCL.js';
import F_FREEvESTB from '../../json/js/questions/filter/F-FREEvESTB.js';
import F_RTRBvRHBL from '../../json/js/questions/filter/F-RTRBvRHBL.js';
import F_ALLOvCMPN from '../../json/js/questions/filter/F-ALLOvCMPN.js';
import F_STRFvHIER from '../../json/js/questions/filter/F-STRFvHIER.js';
import F_DOMNvSEPR from '../../json/js/questions/filter/F-DOMNvSEPR.js';
import F_LEADvPOPL from '../../json/js/questions/filter/F-LEADvPOPL.js';
import F_POLIvETHN from '../../json/js/questions/filter/F-POLIvETHN.js';
import F_DEMOvORGN from '../../json/js/questions/filter/F-DEMOvORGN.js';
import F_DCNTvCENT from '../../json/js/questions/filter/F-DCNTvCENT.js';
import F_CLSSvCPTL from '../../json/js/questions/filter/F-CLSSvCPTL.js';
import F_RSTNvRECN from '../../json/js/questions/filter/F-RSTNvRECN.js';
import F_MORLvPOLT from '../../json/js/questions/filter/F-MORLvPOLT.js';
import F_SELFvCRIT from '../../json/js/questions/filter/F-SELFvCRIT.js';
import F_CULTvDISC from '../../json/js/questions/filter/F-CULTvDISC.js';
import F_INCRvGRAD from '../../json/js/questions/filter/F-INCRvGRAD.js';
import F_REGLvNATL from '../../json/js/questions/filter/F-REGLvNATL.js';
import F_CNFLvCLLB from '../../json/js/questions/filter/F-CNFLvCLLB.js';
import F_ASSMvLIBR from '../../json/js/questions/filter/F-ASSMvLIBR.js';
import F_MAJOvMINO from '../../json/js/questions/filter/F-MAJOvMINO.js';
import F_TECHvHIST from '../../json/js/questions/filter/F-TECHvHIST.js';
import F_ANTIvHYPR from '../../json/js/questions/filter/F-ANTIvHYPR.js';
import F_PSSMvOPTM from '../../json/js/questions/filter/F-PSSMvOPTM.js';
import F_ETHCvIDEO from '../../json/js/questions/filter/F-ETHCvIDEO.js';
import F_INTPvPRSN from '../../json/js/questions/filter/F-INTPvPRSN.js';
import F_INNVvPRSV from '../../json/js/questions/filter/F-INNVvPRSV.js';
import F_INSTvIDEA from '../../json/js/questions/filter/F-INSTvIDEA.js';
import adorno_horkheimer_dialectic_of_enlightement from '../../json/js/references/adorno-horkheimer_dialectic-of-enlightement.js';
import alinsky_rules_for_radicals from '../../json/js/references/alinsky_rules-for-radicals.js';
import althusser_reproduction_of_capitalism from '../../json/js/references/althusser_reproduction-of-capitalism.js';
import anderson_imagined_communities from '../../json/js/references/anderson_imagined-communities.js';
import appiah_ethics_of_identity from '../../json/js/references/appiah_ethics-of-identity.js';
import aquinas_treatise_on_law from '../../json/js/references/aquinas_treatise-on-law.js';
import babbitt_democracy_and_leadership from '../../json/js/references/babbitt_democracy-and-leadership.js';
import badiou_metapolitics from '../../json/js/references/badiou_metapolitics.js';
import bagehot_english_constitution from '../../json/js/references/bagehot_english-constitution.js';
import bap_bronze_age_mindset from '../../json/js/references/bap_bronze-age-mindset.js';
import bastiat_the_law from '../../json/js/references/bastiat_the-law.js';
import baudrillard_illusion_of_the_end from '../../json/js/references/baudrillard_illusion-of-the-end.js';
import bauer_question_of_nationalities from '../../json/js/references/bauer_question-of-nationalities.js';
import beck_globalization from '../../json/js/references/beck_globalization.js';
import beito_the_voluntary_city from '../../json/js/references/beito_the-voluntary-city.js';
import bentham_morals_and_legislation from '../../json/js/references/bentham_morals-and-legislation.js';
import berlin_against_the_current from '../../json/js/references/berlin_against-the-current.js';
import bernstein_preconditions_of_socialism from '../../json/js/references/bernstein_preconditions-of-socialism.js';
import black_abolition_of_work from '../../json/js/references/black_abolition-of-work.js';
import bordiga_science_and_passion from '../../json/js/references/bordiga_science-and-passion.js';
import bossuet_politics_drawn from '../../json/js/references/bossuet_politics-drawn.js';
import burnham_defenders_of_freedom from '../../json/js/references/burnham_defenders-of-freedom.js';
import butterfield_whig_history from '../../json/js/references/butterfield_whig-history.js';
import camatte_capital_and_community from '../../json/js/references/camatte_capital-and-community.js';
import camatte_democratic_mystification from '../../json/js/references/camatte_democratic-mystification.js';
import carlyle_on_heroes from '../../json/js/references/carlyle_on-heroes.js';
import carr_twenty_years_crisis from '../../json/js/references/carr_twenty-years-crisis.js';
import carson_mutualist_political_economy from '../../json/js/references/carson_mutualist-political-economy.js';
import chomsky_responsibility_of_intellectuals from '../../json/js/references/chomsky_responsibility-of-intellectuals.js';
import cole_guild_socialism from '../../json/js/references/cole_guild-socialism.js';
import culp_dark_deleuze from '../../json/js/references/culp_dark-deleuze.js';
import dahl_polyarchy from '../../json/js/references/dahl_polyarchy.js';
import dauve_crisis_to_communisation from '../../json/js/references/dauve_crisis-to-communisation.js';
import de_beauvoir_second_sex from '../../json/js/references/de-beauvoir_second-sex.js';
import debord_society_of_the_spectacle from '../../json/js/references/debord_society-of-the-spectacle.js';
import derrida_dissemination from '../../json/js/references/derrida_dissemination.js';
import drucker_concept_of_the_corporation from '../../json/js/references/drucker_concept-of-the-corporation.js';
import du_bois_black_reconstruction from '../../json/js/references/du-bois_black-reconstruction.js';
import ellul_autopsy_of_revolution from '../../json/js/references/ellul_autopsy-of-revolution.js';
import ellul_technological_society from '../../json/js/references/ellul_technological-society.js';
import erik_liberty_or_equality from '../../json/js/references/erik_liberty-or-equality.js';
import esping_three_worlds from '../../json/js/references/esping_three-worlds.js';
import evola_fascism_viewed_from_the_right from '../../json/js/references/evola_fascism-viewed-from-the-right.js';
import fanon_wretched_of_the_earth from '../../json/js/references/fanon_wretched-of-the-earth.js';
import faye_archeofuturism from '../../json/js/references/faye_archeofuturism.js';
import fisher_capitalist_realism from '../../json/js/references/fisher_capitalist-realism.js';
import foucault_archaeology_of_knowledge from '../../json/js/references/foucault_archaeology-of-knowledge.js';
import fourier_theory_of_four_movements from '../../json/js/references/fourier_theory-of-four-movements.js';
import friedman_program from '../../json/js/references/friedman_program.js';
import fuller_lipinska_proactionary_imperative from '../../json/js/references/fuller-lipinska_proactionary-imperative.js';
import galbraith_american_capitalism from '../../json/js/references/galbraith_american-capitalism.js';
import george_progress_and_poverty from '../../json/js/references/george_progress-and-poverty.js';
import george_protection from '../../json/js/references/george_protection.js';
import giddens_beyond_left_and_right from '../../json/js/references/giddens_beyond-left-and-right.js';
import glazer_melting_pot from '../../json/js/references/glazer_melting-pot.js';
import glossop_world_federation from '../../json/js/references/glossop_world-federation.js';
import godwin_political_justice from '../../json/js/references/godwin_political-justice.js';
import graeber_sahlins_on_kings from '../../json/js/references/graeber-sahlins_on-kings.js';
import gramsci_prison_notebooks from '../../json/js/references/gramsci_prison-notebooks.js';
import guevara_guerilla_warfare from '../../json/js/references/guevara_guerilla-warfare.js';
import habermas_structural_transformation from '../../json/js/references/habermas_structural-transformation.js';
import hanisch_personal_is_political from '../../json/js/references/hanisch_personal-is-political.js';
import hayek_road_to_serfdom from '../../json/js/references/hayek_road-to-serfdom.js';
import heidegger_question_concerning_technology from '../../json/js/references/heidegger_question-concerning-technology.js';
import heumer_political_authority from '../../json/js/references/heumer_political-authority.js';
import hobbes_leviathan from '../../json/js/references/hobbes_leviathan.js';
import hobsbawm_age_of_extremes from '../../json/js/references/hobsbawm_age-of-extremes.js';
import huntington_clash_of_civilizations from '../../json/js/references/huntington_clash-of-civilizations.js';
import ikenberry_liberal_leviathan from '../../json/js/references/ikenberry_liberal-leviathan.js';
import ilyin_legal_consciousness from '../../json/js/references/ilyin_legal-consciousness.js';
import jervis_nuclear_revolution from '../../json/js/references/jervis_nuclear-revolution.js';
import jmp_continuity_and_rupture from '../../json/js/references/jmp_continuity-and-rupture.js';
import kagan_world_america_made from '../../json/js/references/kagan_world-america-made.js';
import kirk_conservative_mind from '../../json/js/references/kirk_conservative-mind.js';
import kropotkin_conquest_of_bread from '../../json/js/references/kropotkin_conquest-of-bread.js';
import krugman_rethinking_international_trade from '../../json/js/references/krugman_rethinking-international-trade.js';
import la_banquise_recollecting_our_past from '../../json/js/references/la-banquise_recollecting-our-past.js';
import laclau_mouffe_hegemony_and_strategy from '../../json/js/references/laclau+mouffe_hegemony-and-strategy.js';
import land_dark_enlightenment from '../../json/js/references/land_dark-enlightenment.js';
import land_fanged_noumena from '../../json/js/references/land_fanged-noumena.js';
import lasch_culture_narcissism from '../../json/js/references/lasch_culture-narcissism.js';
import le_bon_the_crowd from '../../json/js/references/le-bon_the-crowd.js';
import lenin_imperialism from '../../json/js/references/lenin_imperialism.js';
import lenin_state_and_revolution from '../../json/js/references/lenin_state-and-revolution.js';
import lenin_what_is_to_be_done from '../../json/js/references/lenin_what-is-to-be-done.js';
import lijphart_patterns_of_democracy from '../../json/js/references/lijphart_patterns-of-democracy.js';
import lucas_business_cycle from '../../json/js/references/lucas_business-cycle.js';
import lukacs_history_and_class_consciousness from '../../json/js/references/lukacs_history-and-class-consciousness.js';
import luxemberg_reform_or_revolution from '../../json/js/references/luxemberg_reform-or-revolution.js';
import macintyre_after_virtue from '../../json/js/references/macintyre_after-virtue.js';
import mackay_avanessian_accelerationist_reader from '../../json/js/references/mackay+avanessian_accelerationist-reader.js';
import marcuse_one_dimensional_man from '../../json/js/references/marcuse_one-dimensional-man.js';
import marinetti_futurist_cookbook from '../../json/js/references/marinetti_futurist-cookbook.js';
import marx_1844_manuscripts from '../../json/js/references/marx_1844-manuscripts.js';
import marx_capital_1 from '../../json/js/references/marx_capital_1.js';
import mauss_the_gift from '../../json/js/references/mauss_the-gift.js';
import mazzucato_entrepreneurial_state from '../../json/js/references/mazzucato_entrepreneurial-state.js';
import mcquinn_critical_self_theory from '../../json/js/references/mcquinn_critical-self-theory.js';
import mearsheimer_tragedy_of_great_power_politics from '../../json/js/references/mearsheimer_tragedy-of-great-power-politics.js';
import menger_principles_of_economics from '../../json/js/references/menger_principles-of-economics.js';
import meyer_defense_of_freedom from '../../json/js/references/meyer_defense-of-freedom.js';
import michels_political_parties from '../../json/js/references/michels_political-parties.js';
import mills_power_elite from '../../json/js/references/mills_power-elite.js';
import mises_bureaucracy from '../../json/js/references/mises_bureaucracy.js';
import mises_human_action from '../../json/js/references/mises_human-action.js';
import more_diachronic_self from '../../json/js/references/more_diachronic-self.js';
import mosca_ruling_class from '../../json/js/references/mosca_ruling-class.js';
import murray_herrnstein_bell_curve from '../../json/js/references/murray+herrnstein_bell-curve.js';
import nau_conservative_internationalism from '../../json/js/references/nau_conservative-internationalism.js';
import ness_worker_organization from '../../json/js/references/ness_worker-organization.js';
import nietzsche_genealogy_of_morality from '../../json/js/references/nietzsche_genealogy-of-morality.js';
import nisbet_quest_for_community from '../../json/js/references/nisbet_quest-for-community.js';
import nozick_anarchy_state_utopia from '../../json/js/references/nozick_anarchy-state-utopia.js';
import pannekoek_workers_councils from '../../json/js/references/pannekoek_workers-councils.js';
import perlman_against_leviathan from '../../json/js/references/perlman_against-leviathan.js';
import plato_republic from '../../json/js/references/plato_republic.js';
import polanyi_great_transformation from '../../json/js/references/polanyi_great-transformation.js';
import popper_the_open_society from '../../json/js/references/popper_the-open-society.js';
import proudhon_general_idea from '../../json/js/references/proudhon_general-idea.js';
import proudhon_principle_of_federation from '../../json/js/references/proudhon_principle-of-federation.js';
import proudhon_what_is_property from '../../json/js/references/proudhon_what-is-property.js';
import qutb_milestones from '../../json/js/references/qutb_milestones.js';
import rawls_political_liberalism from '../../json/js/references/rawls_political-liberalism.js';
import rawls_theory_of_justice from '../../json/js/references/rawls_theory-of-justice.js';
import reynolds_retromania from '../../json/js/references/reynolds_retromania.js';
import rocker_anarcho_syndicalism from '../../json/js/references/rocker_anarcho-syndicalism.js';
import rothbard_americas_great_depression from '../../json/js/references/rothbard_americas-great-depression.js';
import saint_simon_selected_writings from '../../json/js/references/saint-simon_selected-writings.js';
import sartre_search_for_a_method from '../../json/js/references/sartre_search-for-a-method.js';
import schelling_arms_and_influence from '../../json/js/references/schelling_arms-and-influence.js';
import schmitt_concept_of_the_political from '../../json/js/references/schmitt_concept-of-the-political.js';
import scott_seeing_like_a_state from '../../json/js/references/scott_seeing-like-a-state.js';
import skidelsky_keynes from '../../json/js/references/skidelsky_keynes.js';
import sorel_reflections_on_violence from '../../json/js/references/sorel_reflections-on-violence.js';
import spengler_decline_of_the_west_2 from '../../json/js/references/spengler_decline-of-the-west_2.js';
import stalin_problems_of_leninism from '../../json/js/references/stalin_problems-of-leninism.js';
import stirner_ego_and_its_own from '../../json/js/references/stirner_ego-and-its-own.js';
import sturzo_church_and_state from '../../json/js/references/sturzo_church-and-state.js';
import tainter_complex_societies from '../../json/js/references/tainter_complex-societies.js';
import thoreau_civil_disobedience from '../../json/js/references/thoreau_civil-disobedience.js';
import tocqueville_old_regime from '../../json/js/references/tocqueville_old-regime.js';
import tolstoy_kingdom_of_god from '../../json/js/references/tolstoy_kingdom-of-god.js';
import tonnies_communities_and_society from '../../json/js/references/tonnies_communities-and-society.js';
import tronti_workers_and_capital from '../../json/js/references/tronti_workers-and-capital.js';
import trotsky_permanent_revolution from '../../json/js/references/trotsky_permanent-revolution.js';
import tullock_calculus_of_consent from '../../json/js/references/tullock_calculus-of-consent.js';
import vaneigem_the_revolution_of_everyday_life from '../../json/js/references/vaneigem_the-revolution-of-everyday-life.js';
import veblen_leisure_class from '../../json/js/references/veblen_leisure-class.js';
import waltz_theory_of_international_politics from '../../json/js/references/waltz_theory-of-international-politics.js';
import waring_taylorism_transformed from '../../json/js/references/waring_taylorism-transformed.js';
import wright_real_utopia from '../../json/js/references/wright_real-utopia.js';
import zedong_guerilla_warfare from '../../json/js/references/zedong_guerilla-warfare.js';
import barclay_the_state from '../../json/js/references/barclay_the-state.js';
import bookchin_ecology_of_freedom from '../../json/js/references/bookchin_ecology-of-freedom.js';
import keohane_after_hegemony from '../../json/js/references/keohane_after-hegemony.js';
import schweickart_after_capitalism from '../../json/js/references/schweickart_after-capitalism.js';
import thaler_sunstein_nudge from '../../json/js/references/thaler-sunstein_nudge.js';
import fukuyama_end_of_history from '../../json/js/references/fukuyama_end-of-history.js';
import dewey_public_and_its_problems from '../../json/js/references/dewey_public-and-its-problems.js';
import burke_reflections_on_revolution from '../../json/js/references/burke_reflections-on-revolution.js';
import fukuyama_identity from '../../json/js/references/fukuyama_identity.js';
import oakeshott_politics_of_scepticism from '../../json/js/references/oakeshott_politics-of-scepticism.js';
import singer_most_good_you_can_do from '../../json/js/references/singer_most-good-you-can-do.js';
import sunstein_kahneman_sibony_noise from '../../json/js/references/sunstein-kahneman-sibony_noise.js';
import fishkin_democracy_when_people_are_thinking from '../../json/js/references/fishkin_democracy-when-people-are-thinking.js';
import jackson_prosperity_without_growth from '../../json/js/references/jackson_prosperity-without-growth.js';
import foucault_discipline_and_punish from '../../json/js/references/foucault_discipline-and-punish.js';
import taleb_antifragile from '../../json/js/references/taleb_antifragile.js';
export const all = [J_CONFIG,B_SLIB,B_MINR,B_MUTL,B_NEOL,B_IDEA,B_REAL,B_COSM,B_PLUR,B_SYNC,B_PALO,B_PATR,B_THEO,B_ELIT,B_TCHN,B_MERT,B_IDNT,B_FASC,B_MONR,B_SYND,B_VANG,B_LCOM,B_ULFT,B_PACF,B_LBMV,B_NMRX,B_POMO,B_EGOI,B_PLFT,B_DSOC,B_CNAT,B_LNAT,B_LUDD,B_FUTR,B_NEOR,B_THUM,B_ANCP,B_SANR,B_UTOP,B_SDEM,B_LCON,B_RCNT,C_LFT,C_LIB,C_RIG,C_IDE,C_MAT,C_POL,L_LIBT,L_PROG,L_INTR,L_MODR,L_POPL,L_ACCL,L_NEWL,L_COMM,L_INTG,L_ARIS,L_NATN,L_ANAR,Q_WLT,Q_REG,Q_OWN,Q_PRO,Q_REL,Q_HIS,Q_ETH,Q_MED,Q_CON,Q_LAW,Q_IDN,Q_POW,Q_FAM,Q_DEM,Q_TCH,Q_EQU,Q_CMP,Q_CNS,Q_FUT,F_MINRvANAR,F_DEONvCNSQ,F_HOMEvUSUF,F_PRSCvDSCR,F_MULTvUNIL,F_COMPvCOOP,F_AGREvDISG,F_PRSNvSOCL,F_FREEvESTB,F_RTRBvRHBL,F_ALLOvCMPN,F_STRFvHIER,F_DOMNvSEPR,F_LEADvPOPL,F_POLIvETHN,F_DEMOvORGN,F_DCNTvCENT,F_CLSSvCPTL,F_RSTNvRECN,F_MORLvPOLT,F_SELFvCRIT,F_CULTvDISC,F_INCRvGRAD,F_REGLvNATL,F_CNFLvCLLB,F_ASSMvLIBR,F_MAJOvMINO,F_TECHvHIST,F_ANTIvHYPR,F_PSSMvOPTM,F_ETHCvIDEO,F_INTPvPRSN,F_INNVvPRSV,F_INSTvIDEA,adorno_horkheimer_dialectic_of_enlightement,alinsky_rules_for_radicals,althusser_reproduction_of_capitalism,anderson_imagined_communities,appiah_ethics_of_identity,aquinas_treatise_on_law,babbitt_democracy_and_leadership,badiou_metapolitics,bagehot_english_constitution,bap_bronze_age_mindset,bastiat_the_law,baudrillard_illusion_of_the_end,bauer_question_of_nationalities,beck_globalization,beito_the_voluntary_city,bentham_morals_and_legislation,berlin_against_the_current,bernstein_preconditions_of_socialism,black_abolition_of_work,bordiga_science_and_passion,bossuet_politics_drawn,burnham_defenders_of_freedom,butterfield_whig_history,camatte_capital_and_community,camatte_democratic_mystification,carlyle_on_heroes,carr_twenty_years_crisis,carson_mutualist_political_economy,chomsky_responsibility_of_intellectuals,cole_guild_socialism,culp_dark_deleuze,dahl_polyarchy,dauve_crisis_to_communisation,de_beauvoir_second_sex,debord_society_of_the_spectacle,derrida_dissemination,drucker_concept_of_the_corporation,du_bois_black_reconstruction,ellul_autopsy_of_revolution,ellul_technological_society,erik_liberty_or_equality,esping_three_worlds,evola_fascism_viewed_from_the_right,fanon_wretched_of_the_earth,faye_archeofuturism,fisher_capitalist_realism,foucault_archaeology_of_knowledge,fourier_theory_of_four_movements,friedman_program,fuller_lipinska_proactionary_imperative,galbraith_american_capitalism,george_progress_and_poverty,george_protection,giddens_beyond_left_and_right,glazer_melting_pot,glossop_world_federation,godwin_political_justice,graeber_sahlins_on_kings,gramsci_prison_notebooks,guevara_guerilla_warfare,habermas_structural_transformation,hanisch_personal_is_political,hayek_road_to_serfdom,heidegger_question_concerning_technology,heumer_political_authority,hobbes_leviathan,hobsbawm_age_of_extremes,huntington_clash_of_civilizations,ikenberry_liberal_leviathan,ilyin_legal_consciousness,jervis_nuclear_revolution,jmp_continuity_and_rupture,kagan_world_america_made,kirk_conservative_mind,kropotkin_conquest_of_bread,krugman_rethinking_international_trade,la_banquise_recollecting_our_past,laclau_mouffe_hegemony_and_strategy,land_dark_enlightenment,land_fanged_noumena,lasch_culture_narcissism,le_bon_the_crowd,lenin_imperialism,lenin_state_and_revolution,lenin_what_is_to_be_done,lijphart_patterns_of_democracy,lucas_business_cycle,lukacs_history_and_class_consciousness,luxemberg_reform_or_revolution,macintyre_after_virtue,mackay_avanessian_accelerationist_reader,marcuse_one_dimensional_man,marinetti_futurist_cookbook,marx_1844_manuscripts,marx_capital_1,mauss_the_gift,mazzucato_entrepreneurial_state,mcquinn_critical_self_theory,mearsheimer_tragedy_of_great_power_politics,menger_principles_of_economics,meyer_defense_of_freedom,michels_political_parties,mills_power_elite,mises_bureaucracy,mises_human_action,more_diachronic_self,mosca_ruling_class,murray_herrnstein_bell_curve,nau_conservative_internationalism,ness_worker_organization,nietzsche_genealogy_of_morality,nisbet_quest_for_community,nozick_anarchy_state_utopia,pannekoek_workers_councils,perlman_against_leviathan,plato_republic,polanyi_great_transformation,popper_the_open_society,proudhon_general_idea,proudhon_principle_of_federation,proudhon_what_is_property,qutb_milestones,rawls_political_liberalism,rawls_theory_of_justice,reynolds_retromania,rocker_anarcho_syndicalism,rothbard_americas_great_depression,saint_simon_selected_writings,sartre_search_for_a_method,schelling_arms_and_influence,schmitt_concept_of_the_political,scott_seeing_like_a_state,skidelsky_keynes,sorel_reflections_on_violence,spengler_decline_of_the_west_2,stalin_problems_of_leninism,stirner_ego_and_its_own,sturzo_church_and_state,tainter_complex_societies,thoreau_civil_disobedience,tocqueville_old_regime,tolstoy_kingdom_of_god,tonnies_communities_and_society,tronti_workers_and_capital,trotsky_permanent_revolution,tullock_calculus_of_consent,vaneigem_the_revolution_of_everyday_life,veblen_leisure_class,waltz_theory_of_international_politics,waring_taylorism_transformed,wright_real_utopia,zedong_guerilla_warfare,barclay_the_state,bookchin_ecology_of_freedom,keohane_after_hegemony,schweickart_after_capitalism,thaler_sunstein_nudge,fukuyama_end_of_history,dewey_public_and_its_problems,burke_reflections_on_revolution,fukuyama_identity,oakeshott_politics_of_scepticism,singer_most_good_you_can_do,sunstein_kahneman_sibony_noise,fishkin_democracy_when_people_are_thinking,jackson_prosperity_without_growth,foucault_discipline_and_punish,taleb_antifragile];
/* 
The file importJSON.js.sample is not meant to be directly run, but edited and then pulled into importJSON.js.
importJSON.js is dynamically generated from the json-js-converter.py script. It's not meant to be directly edited.
importJSON.js.sample is used to edit the functions which will then be pulled into the file.
*/

//Custom Error Types

export class InvalidURLError extends Error {};

// Return a list of all the canopy question JSONs
export function getCanopy() {
    let subset = [];
    for (const p in all) {
       let json = all[p];
       if (json.id.startsWith('Q-')) { //If a JSON ID starts w/ Q, that means its a canopy question
            subset.push(json);
       }
    }
    return subset;    
}


// Return a list of all the Cluster JSONs, pass in "approach" to get approach, pass in "focus" to get focuses
export function getClusters(arg) {
    let subset = [];
    for (const p in all) {
       let json = all[p];
       if (json.id.startsWith('C-')) { //If a JSON ID starts w/ CF, that means its a Focus cluster
            subset.push(json);
       }
    }
    if (arg === undefined) {
        return subset;
    }
    else {
        let selectSet = []; 
        for (const s of subset) {
            if (s.type == arg) {
                selectSet.push(s);
            }
        }
        return selectSet;
    } 
}



// Return a list of all the canopy question JSONs
export function getApproaches() {
    const superset = getClusters();
    let subset = [];
    for (const p in superset) {
       if (json.type == 'approach') { //If a JSON ID starts w/ Q, that means its a canopy question
            subset.push(json);
       }
    }
    return subset;    
}


// Given an ID string, find the corresponding JSON
export function find(id) {
    for (const p in all) {
        let json = all[p];
        if (json.id == id) {
            return json;
        }
    }
    throw new InvalidURLError(`Invalid JSON ID: ${id}`); //Throw error if JSON ID not found
}

//Grab the answer data given the question ID and answer text
export function getAnswer(qId, aText) {
    const question = find(qId);
    for (const a of question.answers) {
        if (a.answer == aText) {
            return a;
        }
    }
}

export function getLeaves(arg) { //Filter options are "all", "C-OTH", and the focus/approach IDs
    let subset = [];
    for (const p in all) {
       let json = all[p];
       if (json.id.startsWith('L-')) { 
           //Allow user to filter by approach and focus, having a 2-length focus/approach leaf falls under other
           if (arg == "all") {
                subset.push(json);
           }
           else if (json.focus.length > 1 || json.approach.length > 1) {
                if (arg == 'C-OTH') {
                    subset.push(json);
                }
           }
           else if (getClusters('focus').includes(arg)){
               if (json.focus[0] == arg) {
                    subset.push(json);
               }
           }
           else if (getClusters('approach').includes(arg)){
               if (json.approach[0] == arg) {
                    subset.push(json);
               }
           }
       }
    }
    return subset;
}

//getFilter()
//getLeaf()
//getBranch()
