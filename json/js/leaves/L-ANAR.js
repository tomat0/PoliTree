const json = {
    "id": "L-ANAR",
    "name": "Anarchist",
    "other": false,
    "focus": [
        "C-POL"
    ],
    "approach": [
        "C-LFT"
    ],
    "icon": "anarchist_a.png",
    "quote": "\u201cAs man seeks justice in equality, so society seeks order in anarchy.\u201d \u2015 Pierre-Joseph Proudhon",
    "description": [
        "<u>Hierarchy</u> is inherently unbalanced, and economic/social abuses stem from this unjustified gap in power.",
        "In order to combat <u>hierarchy</u>, no source of authority should be left unscrutinized or yielded to.",
        "The idea of a person's value simply exists to reinforce <u>hierarchy</u>."
    ],
    "branches": [
        "B-SANR",
        "B-PACF",
        "B-LBMV"
    ],
    "filter_questions": [
        "F-MAJOvMINO",
        "F-RSTNvRECN",
        "F-MORLvPOLT"
    ],
    "tiebreaker": "F-RSTNvRECN",
	"tiebreaker-branch": "B-PACF",
    "references": [
        "godwin_political-justice",
        "barclay_the-state"
    ],
    "reference-categories": []
}

export default json;