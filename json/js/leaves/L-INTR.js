const json = {
    "id": "L-INTR",
    "name": "Internationalist",
    "other": false,
    "focus": [
        "C-POL"
    ],
    "approach": [
        "C-LIB"
    ],
    "icon": "internationalist_globe-2.png",
    "quote": "\u201cOur strategy for peace with freedom must also be based on strength -- economic strength and military strength.\u201d \u2015 Ronald Reagan",
    "description": [
        "The security of our greater global community relies on our ability to defuse <u>conflict</u> whether through diplomatic or forceful means.",
        "The preservation and expansion of democracy justifies <u>conflict</u> with those who oppose that.",
        "Equality of people is only true under a democratic system, which must be defended against freedom's enemies through <u>conflict</u>."
    ],
    "branches": [
        "B-IDEA",
        "B-REAL",
        "B-COSM"
    ],
    "filter_questions": [
        "F-PRSCvDSCR",
        "F-MULTvUNIL",
        "F-COMPvCOOP"
    ],
    "tiebreaker": "F-MULTvUNIL",
    "tiebreaker-branch": "B-COSM",
    "references": [
        "schelling_arms-and-influence",
        "jervis_nuclear-revolution"
    ],
    "reference-categories": []
}

export default json;