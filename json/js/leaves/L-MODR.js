const json = {
    "id": "L-MODR",
    "name": "Moderate",
    "other": false,
    "focus": [
        "C-IDE"
    ],
    "approach": [
        "C-LIB"
    ],
    "icon": "moderate_dove-2.png",
    "quote": "\u201cHuman progress has never been shaped by commentators, complainers or cynics.\u201d \u2015 Tony Blair",
    "description": [
        "A healthy democracy requires that its members agree on a certain set of <u>norms</u> on what is and is not acceptable political behavior.",
        "Reform is preferable to radical political change, as it respects the <u>norms</u> which keeps our society stable while allowing for productive change.",
        "Your value is as a citizen, whose input and political participation should be judged according to how well it upholds <u>norms</u>."
    ],
    "branches": [
        "B-PLUR",
        "B-SYNC",
        "B-LCON",
        "B-RCNT"
    ],
    "filter_questions": [
		"F-INSTvIDEA",
		"F-INNVvPRSV",
        "F-AGREvDISG"
    ],
    "references": [
    	"dewey_public-and-its-problems",
        "habermas_structural-transformation",
        "tocqueville_old-regime"
    ],
    "reference-categories": [],
    "tiebreaker": "",
    "tiebreaker-branch": ""
}

export default json;