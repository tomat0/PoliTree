const json = {
    "id": "L-NEWL",
    "name": "New Left",
    "other": false,
    "focus": [
        "C-IDE"
    ],
    "approach": [
        "C-LFT"
    ],
    "icon": "newleft_conversation-2.png",
    "quote": "\u201cWe do not need to be able to say what \u201chuman nature\u201d is in order to be able to say that some training is \u201cagainst human nature.\u201d \u2015 Paul Goodman ",
    "description": [
        "Our material and political narratives are ultimately constructions of ideological <u>conditioning</u>.",
        "It is only when we turn the critical lens against said <u>conditioning</u> that we are able to act freely.",
        "Our society's understanding of value is <u>conditioned</u>."
    ],
    "branches": [
        "B-NMRX",
        "B-POMO",
        "B-EGOI",
        "B-PLFT"
    ],
    "filter_questions": [
        "F-SELFvCRIT",
        "F-CULTvDISC",
        "F-ETHCvIDEO"
    ],
    "references": [
        "chomsky_responsibility-of-intellectuals",
        "marcuse_one-dimensional-man",
        "laclau+mouffe_hegemony-and-strategy"
    ],
    "reference-categories": [],
    "tiebreaker": "",
    "tiebreaker-branch": ""
}
export default json;