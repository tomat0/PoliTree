const json = {
    "id": "L-INTG",
    "name": "Integralist",
    "other": false,
    "focus": [
        "C-IDE"
    ],
    "approach": [
        "C-RIG"
    ],
    "icon": "integralist_scroll-2.png",
    "quote": "\u201cThe virtuous man is driven by responsibility, the non-virtuous man is driven by profit.\u201d \u2015 Confucius",
    "description": [
        "There is an objective, consistent, and transcendent moral <u>good</u> that we should strive to replicate.",
        "Just as we replicate the good on an individual level, society must also be modeled after this <u>good</u>.",
        "The value of all things are measured with respect to this standard of <u>good</u>."
    ],
    "branches": [
        "B-PALO",
        "B-PATR",
        "B-THEO"
    ],
    "filter_questions": [
        "F-PRSNvSOCL",
        "F-RTRBvRHBL",
        "F-FREEvESTB"
    ],
    "tiebreaker": "F-FREEvESTB",
    "tiebreaker-branch": "B-THEO",
    "references": [       
        "babbitt_democracy-and-leadership",
        "aquinas_treatise-on-law",
        "macintyre_after-virtue"
    ],
    "reference-categories": []
}

export default json;