const json = {
    "id": "L-NATN",
    "name": "Nationalist",
    "other": false,
    "focus": [
        "C-POL"
    ],
    "approach": [
        "C-RIG"
    ],
    "icon": "nationalist_flag-2.png",
    "quote": "\u201cEverything within the nation, nothing outside of the nation, nothing against the nation.\u201d \u2015 Benito Mussolini",
    "description": [
        "In order for a society to function, it's members must have a sense of belonging and <u>identity</u>.",
        "A strong leader is needed to serve as a representation and model of the greater national <u>identity</u>.",
        "Your value is determined by how well you blend with the community and fulfill the role expected of an individual of your <u>identity</u>."
    ],
    "branches": [
        "B-IDNT",
        "B-FASC",
        "B-MONR"
    ],
    "filter_questions": [
        "F-DOMNvSEPR",
        "F-LEADvPOPL",
        "F-POLIvETHN"
    ],
    "tiebreaker": "F-POLIvETHN",
    "tiebreaker-branch": "B-IDNT",
    "references": [
        "tonnies_communities-and-society",
        "schmitt_concept-of-the-political",
        "ilyin_legal-consciousness"
    ],
    "reference-categories": []
}

export default json;