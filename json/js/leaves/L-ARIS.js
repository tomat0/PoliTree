const json = {
    "id": "L-ARIS",
    "name": "Aristocrat",
    "other": false,
    "focus": [
        "C-MAT"
    ],
    "approach": [
        "C-RIG"
    ],
    "icon": "aristocrat_ribbon-2.png",
    "quote": "\u201cWe regard society as the ensemble and union of men engaged in useful work. We can conceive of no other kind of society.\u201d \u2015 Henri de Saint-Simon",
    "description": [
        "Resource distribution should be viewed as a reward to those most <u>deserving</u> of possession.",
        "Political power is held as a responsibility and reward for the most <u>deserving</u>; this could mean the hardest working, the most suited, or the most cunning."
    ],
    "branches": [
        "B-ELIT",
        "B-TCHN",
        "B-MERT"
    ],
    "filter_questions": [
        "F-INTPvPRSN",
        "F-ALLOvCMPN",
        "F-STRFvHIER"
    ],
    "tiebreaker": "F-STRFvHIER",
    "tiebreaker-branch": "B-ELIT",
    "references": [
        "hobbes_leviathan",
        "le-bon_the-crowd",
        "plato_republic"
    ],
    "reference-categories": []
}

export default json;