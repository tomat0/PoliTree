const json = {
    "id": "L-ACCL",
    "name": "Accelerationist",
    "other": true,
    "color": "#879f9b",
    "focus": [
        "C-MAT",
        "C-IDE"
    ],
    "approach": [
        "C-RIG",
        "C-LFT"
    ],
    "icon": "accelerationist_arrows-2.png",
    "quote": "\u201cThe process is not to be critiqued. The process is the critique, feeding back into itself as it escalates. The only way forward is through which means further in.\u201d \u2015 Nick Land",
    "description": [
        "Our culture is full of contradiction, and that contradiction is increasingly clear the more we <u>progress</u>.",
        "Ultimately, our <u>progress</u>, whether political or technological can only result in one conclusion, and our goal should be to use all means possible to accelerate that process."
    ],
    "branches": [
        "B-LUDD",
        "B-FUTR",
        "B-NEOR",
        "B-THUM"
    ],
    "filter_questions": [
        "F-TECHvHIST",
        "F-PSSMvOPTM",
        "F-ANTIvHYPR"
    ],
    "references": [
        "mackay+avanessian_accelerationist-reader",
        "heidegger_question-concerning-technology",
        "culp_dark-deleuze"
    ],
    "reference-categories": [],
    "tiebreaker": "",
    "tiebreaker-branch": ""
}
export default json;