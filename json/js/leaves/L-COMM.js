const json = {
    "id": "L-COMM",
    "name": "Communist",
    "other": false,
    "focus": [
        "C-MAT"
    ],
    "approach": [
        "C-LFT"
    ],
    "icon": "communist_hammer-sickle-2.png",
    "quote": "\u201cThe history of all previous societies has been the history of class struggles.\u201d \u2015 Karl Marx",
    "description": [
        "The development of <u>production</u> is the driving force behind all other social phenomena.",
        "<u>Production</u> must be considered holistically; any critique which considers it only in part is insufficient.",
        "The value the market assigns to things in prices are not an accurate reflection of their true nature."
    ],
    "branches": [
        "B-VANG",
        "B-SYND",
        "B-LCOM",
        "B-ULFT"
    ],
    "filter_questions": [
        "F-DEMOvORGN",
        "F-DCNTvCENT",
        "F-CLSSvCPTL"
    ],
    "references": [
        "hobsbawm_age-of-extremes",
        "marx_1844-manuscripts",
        "marx_capital_1"
    ],
    "reference-categories": [],
    "tiebreaker": "",
    "tiebreaker-branch": ""
}
export default json;