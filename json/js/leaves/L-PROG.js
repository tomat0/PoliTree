const json = {
    "id": "L-PROG",
    "name": "Progressive",
    "other": true,
    "color": "#EAA4DE",
    "focus": [
        "C-MAT",
        "C-POL"
    ],
    "approach": [
        "C-LIB",
        "C-LFT"
    ],
    "icon": "progressive_rose-2.png",
    "quote": "\u201cThe test of our progress is not whether we add more to the abundance of those who have much; it is whether we can provide enough for those who have little.\u201d \u2015 Franklin Delano Roosevelt",
    "description": [
        "The flaws in our political system is the result of social and economic <u>inequality</u>.",
        "We need to constantly reflect upon and reform our society to ensure the minimization of <u>inequality</u>.",
        "The value of people varies due to inequality, and our goal as a society should be to ensure all people are <u>equal</u>."
    ],
    "branches": [
        "B-SLIB",
        "B-SDEM",
        "B-DSOC",
        "B-UTOP"
    ],
    "filter_questions": [
        "F-INCRvGRAD",
        "F-REGLvNATL",
        "F-CNFLvCLLB"
    ],
    "references": [
        "rawls_theory-of-justice",
        "george_progress-and-poverty",
        "du-bois_black-reconstruction"
    ],
    "reference-categories": [],
    "tiebreaker": "",
    "tiebreaker-branch": ""
}
export default json;