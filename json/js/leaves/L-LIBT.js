const json = {
    "id": "L-LIBT",
    "name": "Libertarian",
    "other": false,
    "focus": [
        "C-MAT"
    ],
    "approach": [
        "C-LIB"
    ],
    "icon": "libertarian_key-2.png",
    "quote": "\u201cUnderlying most arguments against the free market is a lack of belief in freedom itself.\u201d \u2015 Milton Friedman",
    "description": [
        "One's right to <u>ownership</u> is the foundation on which all other rights rest.",
        "Issues with our society come from attacks on our right to <u>own</u> property, if we want to improve we must defend it.",
        "Through equality of opportunity, people are inherently granted the same potential to prove their own value."
    ],
    "branches": [
        "B-ANCP",
        "B-MUTL",
        "B-MINR",
        "B-NEOL"
    ],
    "filter_questions": [
        "F-MINRvANAR",
        "F-DEONvCNSQ",
        "F-HOMEvUSUF"
    ],
    "references": [
        "hayek_road-to-serfdom",
        "bastiat_the-law",
        "menger_principles-of-economics"
    ],
    "reference-categories": [],
    "tiebreaker": "",
    "tiebreaker-branch": ""
}
export default json;