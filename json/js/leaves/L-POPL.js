const json = {
    "id": "L-POPL",
    "name": "Populist",
    "other": true,
    "color": "#c4d7ff",
    "focus": [
        "C-POL",
        "C-IDE"
    ],
    "approach": [
        "C-RIG",
        "C-LIB"
    ],
    "icon": "populist_fist-2.png",
    "quote": "\u201cTrue democracy is one where the government does what the people want and defends only one interest: that of the people.\u201d \u2015  Juan Domingo Per\u00f3n",
    "description": [
        "The nation is built upon democratic ideals, with the purpose of serving the <u>popular</u> majority.",
        "A strong leadership that protects the interests of the <u>people</u> ensures that our values are upheld and defended against tyranny."
    ],
    "branches": [
        "B-CNAT",
        "B-LNAT"
    ],
    "filter_questions": [
        "F-ASSMvLIBR"
    ],
    "references": [
        "mills_power-elite",
        "alinsky_rules-for-radicals"
    ],
    "reference-categories": [],
    "tiebreaker": "",
    "tiebreaker-branch": ""
}
export default json;