const json = {
	"id": "C-LIB",
	"name": "Advance",
	"type": "approach",
	"color": "#EFE49E",
	"leaves": ["L-LIBT", "L-INTR", "L-MODR"],
	"other": ["L-PROG", "L-POPL"]
}

export default json;