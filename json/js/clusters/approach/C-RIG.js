const json = {
	"id": "C-RIG",
	"name": "Restore",
	"type": "approach",
	"color": "#7994CE",
	"leaves": ["L-INTG", "L-ARIS", "L-NATN"],
	"other": ["L-POPL", "L-ACCL"]
}

export default json;