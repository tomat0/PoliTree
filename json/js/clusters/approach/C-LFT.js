const json = {
	"id": "C-LFT",
	"name": "Abolish",
	"type": "approach",
	"color": "#CC8491",
	"leaves": ["L-COMM", "L-ANAR", "L-NEWL"],
	"other": ["L-PROG", "L-ACCL"]
}

export default json;