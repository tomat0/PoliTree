const json = {
	"id": "C-POL",
	"name": "Political",
	"type": "focus",
	"color": "#7c7491",
	"leaves": ["L-INTR", "L-NATN", "L-ANAR"],
	"other": ["L-POPL", "L-PROG"]
}

export default json;