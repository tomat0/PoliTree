const json = {
	"id": "C-IDE",
	"name": "Ideological",
	"type": "focus",
	"color": "#C6855B",
	"leaves": ["L-MODR", "L-INTG", "L-NEWL"],
	"other": ["L-PROG", "L-POPL"]
}

export default json;