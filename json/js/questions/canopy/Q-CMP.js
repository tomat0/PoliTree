const json = {
	"id": "Q-CMP",
	"focus": ["C-MAT"],
	"weight": 1,
	"question": "Competition in markets...?",
	"answers": [
			{
				"weight": 1,
				"answer": "creates a race to the bottom, where profit comes before all else", 
				"approach": ["C-LFT"]
			},
			{
				"weight": 1,
				"answer": "needs to be preserved, as a free market is a fair market",
				"approach": ["C-LIB"]
			},
			{
				"weight": 1,
				"answer": "never lasts long, as there's always clear winners and losers",
				"approach": ["C-RIG"]
			}
		]

}

export default json;