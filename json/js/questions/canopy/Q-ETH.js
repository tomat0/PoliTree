const json = {
	"id": "Q-ETH",
	"focus": ["C-IDE"],
	"weight": 1,
	"question": "Ethical codes are created as...?",
	"answers": [
			{
				"answer": "an unnecessary set of burdens driving individuals away from their self-interest", 
				"weight": 1,
				"approach": ["C-LFT"]
			},
			{
				"answer": "frameworks set by societies instructing their members on how to best cooperate with each other",
				"weight": 1,
				"approach": ["C-LIB"]
			},
			{
				"answer": "a transcendent and consistent set of principles preventing humanity from caving to its natural desires",
				"weight": 1,
				"approach": ["C-RIG"]
			}
		]

}

export default json;