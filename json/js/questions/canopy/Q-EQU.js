const json = {
	"id": "Q-EQU",
	"weight": 1,
	"focus": ["C-OTH", "L-PROG"],
	"question": "The ideal of equality...?",
	"answers": [
			{
				"answer": "serves as the foundation of justice", 
				"weight": 1,
				"focus": ["L-PROG"],
				"approach": ["C-LFT"]
			},
			{
				"weight": 1,
				"answer": "not a concern, as prosperity ends up naturally trickling down",
				"focus": ["C-MAT"],
				"approach": ["C-LIB"]
			},
			{
				"weight": 1,
				"answer": "is a myth invented by the weak to restrain the strong",
				"focus": ["C-POL"],
				"approach": ["C-RIG"]
			}
		]

}

export default json;