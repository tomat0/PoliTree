const json = {
	"id": "Q-REG",
	"focus": ["C-MAT"],
	"weight": 1,
	"question": "The presence of regulation...?",
	"answers": [
			{
				"answer": "is important to stop companies from abusing their power", 
				"weight": 1,
				"approach": ["C-LFT"]
			},
			{
				"answer": "is often a burden on entrepreneurs and stifles innovation",
				"weight": 1,
				"approach": ["C-LIB"]
			},
			{
				"answer": "guides the market towards the long-term good rather than short-term profit",
				"weight": 1,
				"approach": ["C-RIG"]
			}
		]
}

export default json;