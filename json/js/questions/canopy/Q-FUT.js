const json = {
	"id": "Q-FUT",
	"weight": 1,
	"focus": ["C-OTH", "L-ACCL"],
	"question": "The current age is...?",
	"answers": [
			{
				"answer": "lacking in imagination, being stuck repeating the ideas of past and present", 
				"weight": 1,
				"focus": ["L-ACCL"],
				"approach": ["C-LFT"]
			},
			{
				"answer": "the best the world has ever been, thanks to the scientific and moral progress we made as a society",
				"weight": 1,
				"focus": ["L-PROG"],
				"approach": ["C-LIB"]
			},
			{
				"answer": "heading to collapse, out of the ashes of which traditional, time-tested forms of organization will return",
				"weight": 1,
				"focus": ["L-ACCL"],
				"approach": ["C-RIG"]
			}
		]

}

export default json;