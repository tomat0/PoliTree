const json = {
	"id": "Q-MED",
	"focus": ["C-IDE"],
	"question": "The influence of mass media...?",
	"weight": 1,
	"answers": [
			{
				"answer": "supplants authentic experience with sterile, consumerist imagery", 
				"weight": 1,
				"approach": ["C-LFT"]
			},
			{
				"answer": "works best when it gives the spotlight to open and civil discussion",
				"weight": 1,
				"approach": ["C-LIB"]
			},
			{
				"answer": "creates a key cultural battleground between moral and immoral values",
				"weight": 1,
				"approach": ["C-RIG"]
			}
		]

}

export default json;