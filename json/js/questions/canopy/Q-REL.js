const json = {
	"id": "Q-REL",
	"focus": ["C-IDE"],
	"question": "The role of religion in society...?",
	"weight": 1,
	"answers": [
			{
				"answer": "controls people by instilling a sense of fear and other-worldliness", 
				"weight": 1,
				"approach": ["C-LFT"]
			},
			{
				"answer": "can give people a sense of meaning and community if kept within certain boundaries",
				"weight": 1,
				"approach": ["C-LIB"]
			},
			{
				"answer": "orients people towards the divine rather than chasing temporary pleasures",
				"weight": 1,
				"approach": ["C-RIG"]
			}
		]

}

export default json;