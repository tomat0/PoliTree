const json = {
	"id": "Q-POW",
	"focus": ["C-POL"],
	"weight": 1,
	"question": "Power is...?",
	"answers": [
			{
				"answer": "inherently dangerous, and cannot be trusted in the hands of any one person or group", 
				"weight": 1,
				"approach": ["C-LFT"]
			},
			{
				"answer": "to be vested in a democratic state to prevent its abuse",
				"weight": 1,
				"approach": ["C-LIB"]
			},
			{
				"answer": "an expression of will, and should not concern itself with the grievances of the weak",
				"weight": 1,
				"approach": ["C-RIG"]
			}
		]

}

export default json;