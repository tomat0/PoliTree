const json = {
	"id": "Q-TCH",
	"focus": ["C-OTH", "L-ACCL"],
	"question": "The evolution of technology...?",
	"weight": 1,
	"answers": [
			{
				"answer": "is destroying both society and nature", 
				"weight": 1,
				"focus": ["L-ACCL", "C-MAT"],
				"approach": ["C-LFT"]
			},
			{
				"answer": "will allow us to transcend our biological limits and solve the world's biggest problems",
				"weight": 1,
				"focus": ["L-ACCL", "C-MAT"],
				"approach": ["C-LIB"]
			},
			{
				"answer": "has potential for good, but can be abused if we are not careful",
				"weight": 1,
				"focus": ["C-MAT"],
				"approach": ["C-RIG"]
			}
		]

}

export default json;