const json = {
	"id": "Q-LAW",
	"focus": ["C-POL"],
	"question": "The law and its institutions exist to...?",
	"weight": 1,
	"answers": [
			{
				"answer": "allow one group to unfairly exercise control over another", 
				"weight": 1,
				"approach": ["C-LFT"]
			},
			{
				"answer": "provide citizens assurance that their disputes will be resolved fairly",
				"weight": 1,
				"approach": ["C-LIB"]
			},
			{
				"answer": "instruct people in proper morals and allegiances",
				"weight": 1,
				"approach": ["C-RIG"]
			}
		]

}

export default json;