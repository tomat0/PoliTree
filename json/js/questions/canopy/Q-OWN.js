const json = {
	"id": "Q-OWN",
	"focus": ["C-MAT"],
	"question": "An ideal system of property ownership should...?",
	"weight": 1,
	"answers": [
			{
				"answer": "motivate people to properly maintain and invest in improving their property", 
				"weight": 1,
				"approach": ["C-LIB"]
			},
			{
				"answer": "give the worker autonomy over what and how they produce", 
				"weight": 1,
				"approach": ["C-LFT"]
			},
			{
				"answer": "encourage humanity to pool together their collective capacity as opposed to dividing things up", 
				"weight": 1,
				"approach": ["C-LFT"]
			},
			{
				"answer": "reward the fittest and root out the least capable", 
				"weight": 1,
				"approach": ["C-RIG"]
			}
		]
}

export default json;