const json = {
	"id": "Q-FAM",
	"focus": ["C-POL"],
	"question": "The nuclear family is...?",
	"weight": 1,
	"answers": [
			{
				"weight": 1,
				"answer": "an oppressive system which suffocates natural human love", 
				"approach": ["C-LFT"]
			},
			{
				"weight": 1,
				"answer": "an effective way to organize a household as long as checks exist to prevent abuse",
				"approach": ["C-LIB"]
			},
			{
				"weight": 1,
				"answer": "a traditional hierarchical unit which serves as the building block of a nation",
				"approach": ["C-RIG"]
			}
		]

}

export default json;