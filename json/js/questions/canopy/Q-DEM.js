const json = {
	"id": "Q-DEM",
	"weight": 1,
	"focus": ["C-OTH", "L-POPL"],
	"question": "'Rule by the people' should mean...?",
	"answers": [
			{
				"weight": 1,
				"answer": "individuals and communities deciding their own rules rather than being beholden to a larger state",
				"focus": ["C-POL"], 
				"approach": ["C-LFT"]
			},
			{
				"weight": 1,
				"answer": "a system which allows people to vote for which politicians and policies they want",
				"focus": ["C-POL"],
				"approach": ["C-LIB"]
			},
			{
				"weight": 1,
				"answer": "having a leader who stands up for the interests of the common man and not the elites",
				"focus": ["L-POPL", "C-POL"],
				"approach": ["C-RIG"]
			}
		]

}

export default json;