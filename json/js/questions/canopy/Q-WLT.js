const json = {
	"id": "Q-WLT",
	"focus": ["C-MAT"],
	"question": "Wealth and resources should be distributed...?",
	"weight": 1,
	"answers": [
			{
				"answer": "in a fashion which encourages competition and innovation", 
				"weight": 1,
				"approach": ["C-LIB"]
			},
			{
				"answer": "collectively, with an interest in social over individual wellbeing", 
				"weight": 1,
				"approach": ["C-LFT"]
			},
			{
				"answer": "to those most capable of taking it for themselves", 
				"weight": 1,
				"approach": ["C-RIG"]
			},
			{
				"answer": "with a focus on overall efficiency over evenness", 
				"weight": 1,
				"approach": ["C-LIB", "C-RIG"]
			}
		]
}

export default json;