const json = {
	"id": "Q-PRO",
	"focus": ["C-MAT"],
	"weight": 1,
	"question": "Production in the industrial era...?",
	"answers": [
			{
				"answer": "has become an end unto itself, completely losing sight of its original purpose", 
				"weight": 1,
				"approach": ["C-LFT"]
			},
			{
				"answer": "has provided us with the tools for organizing society in a more rational fashion", 
				"weight": 1,
				"approach": ["C-RIG"]
			},
			{
				"answer": "has helped improve countless lives by granting consumers access to a wider variety of goods", 
				"weight": 1,
				"approach": ["C-LIB"]
			}
		]
}

export default json;