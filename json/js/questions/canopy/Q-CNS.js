const json = {
	"id": "Q-CNS",
	"weight": 1,
	"focus": ["C-IDE"],
	"question": "Restrictions on speech...?",
	"answers": [
			{
				"weight": 1,
				"answer": "can't be judged without accounting for the surrounding social context and forms of oppression", 
				"approach": ["C-LFT"]
			},
			{
				"weight": 1,
				"answer": "should generally be avoided, with some exceptions",
				"approach": ["C-LIB"]
			},
			{
				"weight": 1,
				"answer": "are necessary when the moral fabric of society is threatened",
				"approach": ["C-RIG"]
			}
		]

}

export default json;