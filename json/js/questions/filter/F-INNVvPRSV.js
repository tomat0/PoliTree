const json = {
        "id": "F-INNVvPRSV",
        "leaf": "L-MODR",
        "set": "2B",
        "question": "Does combatting polarization require us to preserve our institutions or innovate them?",
        "passage-a": "We are entering an era where we are more polarized than ever, as people come to divide themselves into camps and see the other side as evil.  How did this happen? All the recent changes we've seen with technology, media, and the economy has also had effects on our politics. Our system hasn't evolved to deal with these problems, and it's created a situation where our politicians are incentivized to cater to extremism. The solution is not an inspirational speech – it is change in process. We need must modernize our system in a way that modernizes our leaders' incentives, so they can win by serving the people rather than just their own party.",
        "passage-b": "When we empower the people to govern, we inevitably empower the sophists and swindlers among them. Divisiveness and demagoguery go hand in hand: as rival parties struggle for power, the common good and individual rights take a backseat to winning at all costs. This is a phenomenon as old as democracy, and something which our founders went to great lengths to design a system to prevent. Our best defense against demagoguery is to to create a culture of institutionalism, moderation, and respect for the law. Many men will come and go with “quick fixes” to our troubles, but when we open the door to haphazardly picking apart our system, we also end up undermining people's faith in it. Our best hope for defending democracy is upholding the very same institutions and norms which has kept it afloat all this time.",
        "source-a": "https://edition.cnn.com/2021/10/03/opinions/polarization-democracy-open-primaries-yang/index.html",
        "source-b": "https://www.nationalaffairs.com/publications/detail/demagoguery-in-america",
        "branch-a": "B-RCNT",
        "branch-b": "B-LCON"
        
}










export default json;