const json = {
        "id": "F-MINRvANAR",
        "leaf": "L-LIBT",
        "set": "1A",
        "question": "Is the existence of the state a necessary evil?",
        "passage-a": "Without a government, people would be vulnerable to predators and therefore would have to find ways to protect themselves. Under anarchy, life becomes a war of all against all — nasty, chaotic, and short. The strong overpower the weak, taking everything the victims have, but even they will stop prospering once there’s nothing left to take. Nobody produces because they’re scared the product will be stolen from them. Disorganized rule by bandits produces a society in which nobody prospers because nobody is productive.",
        "passage-b": "The important thing to note about the State is that it inherently remains the only actor in society which gets its income through coercion (taxation). Every other law-abiding citizen and group earns their income on voluntary terms: either through exchange or gifts. Given the virtually unchecked power of the State, it will seek to maximize its wealth and power, inevitably expanding beyond its limits. Even with 'small governments', there’s no serious institutional checks to prevent their growth. History demonstrates that power corrupts, and that any power granted will quickly be abused.",
        "source-a": "https://www.independent.org/pdf/tir/tir_08_3_1_holcombe.pdf",
        "source-b": "https://mises.org/library/state-versus-liberty",
        "branch-a": "F-DEONvCNSQ",
        "branch-b": "F-HOMEvUSUF"
}










export default json;