const json = {
        "id": "F-MORLvPOLT",
        "leaf": "L-ANAR",
        "set": "C",
        "question": "Is moral or political power more important in enacting social change?",
        "passage-a": "Moral power acts on man’s will to build communities, increase wealth, and educate people. It is timeless, appealing to his will, passions, and sentiment. Political power envies this influence, as it is artificial, mechanical, and completely incapable of doing good on its own. It can only punish, not restore; it can levy taxes and lock people up but it cannot move their hearts. This is why throughout history, the moral has been the force driving social change while the political has only sought to obstruct it. This former can be found anywhere, independent of all classes, circumstances, and processes. Whereas the latter crumbles under its own weight, only able to maintain its legitimacy by crushing the helpless.",
        "passage-b": "The fact of the matter is that change can only occur when there is the strength to realize it; this strength is meaningless if we continue to focus on convincing people rather than arming them. That is because our world, there are vested interests in keeping the masses oblivious to the truth, and this will remain the case until an event of physical, not just ideological significance occurs. Initially, only a small portion of the population will be able to see past the propaganda, and for those few, it is necessary to rebel against the social order and make their voices heard with violence. If we do not act with haste, we may end up letting our best opportunities slip through our fingers.",
        "source-a": "http://www.adinballou.org/moralpower.shtml",
        "source-b": "https://www.marxists.org/archive/malatesta/1921/09/haste.htm",
        "branch-a": "B-PACF",
        "branch-b": "B-SANR"
        
}










export default json;