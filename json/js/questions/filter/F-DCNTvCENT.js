const json = {
        "id": "F-DCNTvCENT",
        "leaf": "L-COMM",
        "set": "2A",
        "question": "Should political power be centralized throughout the course of a revolution?",
        "passage-a": "To have a circle of professional revolutionaries to be responsible for select activities does not mean they will be “doing all the thinking” for the movement’s masses. It’s actually the opposite: professional revolutionaries come from the grassroots, and it’s with years of training within the struggle that one is able to develop the skills needed. When the rank and file carry out their struggle, they won’t be just following orders, but will be actively thinking of such training. Centralizing these select activities doesn’t mean the whole movement is centralized, the masses still have an important role to play. If anything, delegating this burden to a select committee helps mass participation increase.",
        "passage-b": "It’s necessary to restructure society around the principle of free association: the freedom of communities and individuals to choose who they ally and break apart with without having to worry about being overruled by others. Once we establish this right, infighting will become unnecessary. Without fear of coercion in the name of “unity”, individuals and groups will naturally unite under their inherently common interest. Under the name of liberty, these new networks of communes, regions, and peoples will find the strength, productivity, and resilience to carry out their revolutionary mission.",
        "source-a": "https://www.marxists.org/archive/lenin/works/1901/witbd/iv.htm",
        "source-b": "https://www.marxists.org/reference/archive/bakunin/works/1866/catechism.htm",
        "branch-a": "B-VANG",
        "branch-b": "B-SYND"
        
}










export default json;