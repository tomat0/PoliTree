const json = {
        "id": "F-DEONvCNSQ",
        "leaf": "L-LIBT",
        "set": "2A",
        "question": "What justifies the defense of individual rights and liberties?",
        "passage-a": "Libertarianism is a doctrine directed towards the conduct of men in this world; its merit can only be judged by the concrete effect it has on people’s lives. We support liberty because it’s the optimal arrangement for a human society. Property is good because it has been shown to maximize human well-being. Constructs such as rights and law are meaningless unless they can be practically enforced. A system constructed out of pure principle may make for a good intellectual exercise, but it’s bound to fall apart once it has to be applied to the real world.",
        "passage-b": "Rights are a moral concept, the link between ethics and politics. Tyrannical societies are based upon a principle of collectivism, one that subordinates man and morality to that of the state. Individual rights are the antithesis of this, they assert the liberty of all men as something that even the state is subordinate to. The source of these rights are not to be found in legal code or social agreements, but rather instead the very nature of man’s identity. In order for a man to survive, he must have a right to live, a right to think, a right to act, a right to work. To violate his rights means to force him to act irrationally, which is why it can only be accomplished with physical force.",
        "source-a": "https://web.archive.org/web/20210421232000/https://libertyunbound.com/the-two-libertariansisms/",
        "source-b": "https://courses.aynrand.org/works/mans-rights/",
        "branch-a": "B-NEOL",
        "branch-b": "B-MINR"
        
}










export default json;