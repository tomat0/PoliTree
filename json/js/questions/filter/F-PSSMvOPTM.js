const json = {
        "id": "F-PSSMvOPTM",
        "leaf": "L-ACCL",
        "set": "2B",
        "question": "Is technological progress something to be optimistic about?",
        "passage-a": "As long as issues such as disease and poverty continue to persist, there will continue to be a need to innovate. There’s no off-switch we can flick to halt the progress of technology, for better or for worse it’s here and we need to learn to embrace it. The engine of technological progress provides good and bad outputs in parallel, but any attempt to simply turn away from it is inevitably bound to fail. What we can do instead is guide the direction of technological development to ensure that the positive effects are felt while the negative ones are mitigated.",
        "passage-b": "As technology progresses, the complexity of the larger social machine does too, requiring a much more delicate coordination of the parts within the larger social machine. A lot of modern technology remains inaccessible to the average person, leaving it in control of a select few organizations. This in turn gives them the power to dictate our environment, and ultimately, our lives. It doesn’t matter whether or not it’s backed by good intentions or not, as the lines between “guidance” and “manipulation” are very easily blurred.",
        "source-a": "https://transpolitica.org/2015/07/24/four-political-futures-which-will-you-choose/",
        "source-b": "https://theanarchistlibrary.org/library/ted-kaczynski-progress-versus-liberty",
        "branch-a": "B-THUM",
        "branch-b": "B-LUDD"
        
}










export default json;