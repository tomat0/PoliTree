const json = {
        "id": "F-CNFLvCLLB",
        "leaf": "L-PROG",
        "set": "2A",
        "question": "Are the interests of the ruling and working classes inherently at odds?",
        "passage-a": "The working class and the ruling class have nothing in common. There can be no peace so long as hunger and want are found among millions of the working people and the few, who make up the employing class, have all the good things of life. A power struggle between these two groups must be waged until workers across the world can organize and build a fairer world. Through collective action, an injury to one becomes an injury to all, providing a basis by which workers can resist the forces of capitalism." ,
        "passage-b": "In order to attain harmony, we must unite all people rather than pitting one against another. To that end, we must preach our message to the common man and provide rebuttals to those skeptical of our vision for the future. Socialism is only frightening to the public when it is presented in rigid and inflammatory terms. What divides us is not conflicting interests but simple misunderstanding; it’s only once we answer to the needs of peoples of all classes that we can achieve social unity.",
        "source-a": "https://iww.org.uk/preamble/",
        "source-b": "https://www.marxists.org/archive/considerant/manifesto/ch02.htm",
        "branch-a": "B-DSOC",
        "branch-b": "B-UTOP"
        
}










export default json;