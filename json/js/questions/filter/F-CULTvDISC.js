const json = {
        "id": "F-CULTvDISC",
        "leaf": "L-NEWL",
        "set": "2A",
        "question": "Is an analysis of culture or language more useful towards understanding how society operates?",
        "passage-a": "The issue with the concept of structure is that we assume it's centered around a fixed idea; the concept of an unorganized structure seems completely alien to us. If structure lacks a center, we cannot rely on a person, event, or doctrine to explain its origins, yet looking around us we still unmistakably see it in action. In our attempts to study myth, art, and literature, we run into the issue that it’s an open system: there’s no end to culture, and no end to cultural criticism. Whenever we think we have unraveled it, it tangles itself once more. The central meaning of these works evolve alongside us and the terms we conceive them in. Beyond this lies a critique which situates itself outside of the realm of philosophy, which is able to account for both our historical and linguistic limits.",
        "passage-b": "The secularization of society has not lead to a cultural anarchy as many thought, but rather instead cultural uniformity. Films, magazines, and radio all help make up a larger system in which everything has the same shape and follows the same rhythm. And as this advances, the structure of our society, its conflicts, and motivations become all the more apparent in the media it produces. Taking a step back, one can see the character of capitalism present within it: the distillation of art down to a set of formulas and cliches, the excessive categorization of audiences, and the rapid phasing in and out of fads.",
        "source-a": "http://hydra.humanities.uci.edu/derrida/sign-play.html",
        "source-b": "https://www.marxists.org/reference/archive/adorno/1944/culture-industry.htm",
        "branch-a": "B-POMO",
        "branch-b": "B-NMRX"
        
}










export default json;