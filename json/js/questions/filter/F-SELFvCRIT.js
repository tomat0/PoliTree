const json = {
        "id": "F-SELFvCRIT",
        "leaf": "L-NEWL",
        "set": "1A",
        "question": "Does a radical critique of society require a holistic outlook?",
        "passage-a": "Society can never be understood as a mere sum of individuals but rather instead as the relationships which connect said people. Any course of action which relates to the whole of society and not just individual issues, needs a theory of the totality. Movements which seek drastic change can only do so if they understand the structural relationships beneath. While they may not be tangible, they can still be studied rationally, in relation to a broader reality.",
        "passage-b": "Ideologies often reverse the role between the subject and object: objects are personified while people are treated as objects. It never goes beyond a cursory glance at the totality, separating the world in front of us and the world beyond our grasp. It demands we sacrifice our subjectivity and self-interest in the name of the greater cause. True consciousness is the consciousness of the commonplace, being aware of one’s everyday routine.",
        "source-a": "http://autodidactproject.org/other/adorno34_soc_4.html",
        "source-b": "https://theanarchistlibrary.org/library/larry-law-revolutionary-self-theory",
        "branch-a": "F-CULTvDISC",
        "branch-b": "F-ETHCvIDEO"
        
}










export default json;