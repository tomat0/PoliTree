const json = {
        "id": "F-ANTIvHYPR",
        "leaf": "L-ACCL",
        "set": "2B",
        "question": "Do historians today put too little or too much emphasis on 'change'?",
        "passage-a": "It’s not that the media and universities have a left-wing bias as much as it is that the left is biased towards these professions. What unites politicians, journalists, and historians is the assumption that there is some mysterious force which guarantees that the world will always improve. This narrative justifies the actions of progressive politicians, who in turn endorse and legitimize those who came up with the narrative. That’s why when they speak of change, they can do so without fear: they believe they are the heirs to this tradition of change. For them, it’s circular: change is good because it is inevitable, and inevitable because it is good.",
        "passage-b": "What has dominated our politics the most in recent times is the overall cultural and emotional atmosphere. The optimism of previous generations has been replaced with an overwhelming sense of hopelessness. We’ve reached a point where even imagining a different world is unthinkable. But why should it be this way? If there’s no progressing further, why has so much trouble been taken to destroy our confidence? What if where we are now marked not the end of history, but rather instead a stumble in its beginning? By dreaming up and creating new futures, we create the possibility of breaking out of this cycle.",
        "source-a": "https://www.unqualified-reservations.org/2007/06/hillary-fires-back/",
        "source-b": "https://my-blackout.com/2019/04/25/mark-fisher-acid-communism-unfinished-introduction/",
        "branch-a": "B-NEOR",
        "branch-b": "B-FUTR"
        
}










export default json;