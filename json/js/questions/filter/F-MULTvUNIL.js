const json = {
        "id": "F-MULTvUNIL",
        "leaf": "L-INTR",
        "set": "B",
        "question": "Is there a responsibility to get the approval of other countries when making foreign policy decisions?",
        "tiebreaker": "B-IDEA",
        "passage-a": "In the present day, the main threat to our world order isn’t from some “evil empire”. Rather instead, it’s a collection of other, more nuanced problems: the spread of nuclear weapons, terrorism, disease, and climate change. No country can face this alone; our world is too large and complex to keep under control. These issues can only be solved with collective effort; our ability and willingness to enter into global agreements has an impact which goes beyond any one country’s borders.",
        "passage-b": "The reality is that small countries have no agency in the larger world order. Geography and power end up limiting their foreign policy to basic self-defense. However, there remain a few countries which are capable of leveraging real influence. Success on this front is not determined by how many treaties a leader can sign, but instead the actual exercise of power not constrained by a web of arbitrary international agreements and restrictions.",
        "source-a": "https://www.ft.com/content/18d8f8b6-fa2f-11de-beed-00144feab49a",
        "source-b": "https://www.washingtonpost.com/archive/opinions/2001/06/08/the-new-unilateralism/20dcb60e-e8af-4a87-837c-e4cf85e96756/",
        "branch-a": "B-COSM",
        "branch-b": "B-IDEA"
        
}










export default json;