const json = {
        "id": "F-TECHvHIST",
        "leaf": "L-ACCL",
        "set": "1A",
        "question": "What phenomenon is the evolution of our civilization most tied to?",
        "passage-a": "The degree of personal freedom that exists in a society is determined more by the technological structure of the society than by any laws or ideas. Technological progress is powerful because marches in only one direction; it can’t be reversed. With every innovation, people become more dependent, only able to do without it if replaced by a new invention. It’s not just people, but the system as a whole (imagine what would happen if we got rid of computers). Thus things can move in only one direction, toward greater technologization. It repeatedly forces freedom to take a step back, but technology can never take a step back — short of total overthrow.",
        "passage-b": "Politics is downstream from culture, which in turn is downstream from how we relate ourselves to history. In a democracy, power depends on public opinion, and public opinion depends on history. History is what gets taught in textbooks to everyone from a young age. If facts were all we wanted from history, we’d need databases, not historians. Since history is by definition written by historians, the long-term direction of our politics is in the hands of historians and the narratives that they paint.",
        "source-a": "https://www.washingtonpost.com/wp-srv/national/longterm/unabomber/manifesto.text.htm",
        "source-b": "https://www.unqualified-reservations.org/2007/06/hillary-fires-back/",
        "branch-a": "F-PSSMvOPTM",
        "branch-b": "F-ANTIvHYPR"
        
}










export default json;