const json = {
        "id": "F-RTRBvRHBL",
        "leaf": "L-INTG",
        "set": "B",
        "question": "What principles should we base criminal justice on?",
        "passage-a": "The goals of punishment are as follows: giving the criminal what he deserves, encouraging him to change his ways, and discouraging others from following in his footsteps. At the center of this is retribution; we cannot speak of deterrence or correction until the offender truly feels ashamed of his actions. Retribution need not always be applied or be the only measure applied, but the authority to exact it must remain present for the crimes which do warrant punishment.",
        "passage-b": "Justice is underwritten by the violence of both the velvet and iron gloves of social, economic and political power. But justice does not heal or bring wholeness, it does not repair the broken, it does not restore the lost or the dead. The human heart longs for more than justice. Often its demands for ”Justice!” are a cry for the very things that justice cannot deliver. Conflict and violence are symptoms for the various fractures in our society; it may be inevitable, but that does not change our obligation to help those in need and promote healing in our communities." ,
        "source-a": "https://www.thepublicdiscourse.com/2011/09/4033/",
        "source-b": "https://www.cpjustice.org/public/capital_commentary/article/889",
        "branch-a": "B-THEO",
        "branch-b": "B-PATR"
        
}










export default json;