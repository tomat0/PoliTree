const json = {
        "id": "F-MAJOvMINO",
        "leaf": "L-ANAR",
        "set": "A",
        "question": "Is the concept of majority rule inherently oppressive?",
        "passage-a": "Democracy gives us control over our lives by allowing everyone to shape the social arrangements under which we live. The belief that decisions should only be made via consensus is dangerously misguided, often resulting in the majority being held hostage by a dissident minority. Not every person has to agree with a decision, but they should be willing to go along with it regardless. There is an innate political obligation towards orderly resolution of disputes, without that commitment, democracy is impossible.",
        "passage-b": "For everyone else, democracy is tyranny, forcing minorities to abide by the will of the majority (or even plurality). As long as the group in power retains that status, other groups have just as little say as in a dictatorship. The illusion of universal participation ends up being used to justify any action, no matter how oppressive. The losing votes of a minority validate the victories of the majority, while non-participation is seen as tacit consent; there is no winning. As demonstrated throughout history, change has typically come due to the initiative of minorities while majorities only served to resist.",
        "source-a": "https://theanarchistlibrary.org/library/james-herod-majority-rule",
        "source-b": "https://theanarchistlibrary.org/library/moxie-marlinspike-and-windy-hart-audio-anarchy-radio-an-anarchist-critique-of-democracy",
        "branch-a": "B-SANR",
        "branch-b": "B-LBMV"
        
}










export default json;