const json = {
        "id": "F-STRFvHIER",
        "leaf": "L-ARIS",
        "set": "C",
        "question": "Should an organization pay mind to only its best-performing members?",
        "passage-a":  "Productivity works like a lattice rather than a pyramid; it depends on the results of all performers, not just the best. Rewarding only the best performers can often backfire. When you ignore the nuances of how people perform under different circumstances, you end up with an environment which creates insecurity and discourages those who face setbacks. We should instead focus on bringing out the best in all members: assessing which roles each individual is best suited to and what assistance they can receive in getting caught up to speed.",
        "passage-b":  "“Inclusion” is currently fashionable, with a great deal of trouble going towards silencing anyone who speaks of inherent differences in ability between people. Despite all these efforts to promote diversity and erase bias, none of these efforts have at all yielded real-world results. Yet curiously, when these same people actually have skin in the game, they tend to accept that it’s better to have the most capable people working for them. That’s why all the same companies which speak so openly about these ideals are also the first to ignore them when it comes to selecting their own leadership.",
        "source-a": "https://web.archive.org/web/20230607233542/https://hbr.org/2010/06/the-false-theory-of-meritocrac",
        "source-b": "https://web.archive.org/web/20210307111600/https://jacobitemag.com/2017/08/10/a-question-of-merit/",
        "branch-a": "B-TCHN",
        "branch-b": "B-ELIT"
        
}










export default json;