const json = {
        "id": "F-FREEvESTB",
        "leaf": "L-INTG",
        "set": "C",
        "tiebreaker": "B-THEO",
        "question": "Should the government allow free expression and practice to differing religions?",
        "passage-a": "Freedom ranks as a political end, along with justice; it is a demand of justice itself, a means whereby we achieve the highest good, a unity as a people. As men are rational creatures, they must be able to assume responsibility for their own actions. When this right is denied, society is shaken by its foundations and unable to maintain order. The government is first and foremost concerned with the good of the community, which can only be realized with the freedom for any citizen to live a decent life.",
        "passage-b": "Heresy has no rights. The death of the soul is worse than the freedom of disobedience. It is man’s nature to stray from the narrow path of truth the second his restraints loosen. It brings with it not just individual ruin but also societal ruin. With full freedom of expression, laws and traditions come under attack and civilizations crumble. It’s ridiculous to defend the right to publish evil ideas with the assumption that the free publication of good ideas will counter them. Should we legalize poison just because antidotes exist?",
        "source-a": "https://www.library.georgetown.edu/woodstock/murray/1964e",
        "source-b": "https://www.papalencyclicals.net/greg16/g16mirar.htm",
        "branch-a": "B-PALO",
        "branch-b": "B-THEO"
        
}










export default json;