const json = {
        "id": "F-REGLvNATL",
        "leaf": "L-PROG",
        "set": "2B",
        "question": "Is nationalizing key industries necessary for a healthy market?",
        "passage-a": "Regulation is using the law to structure markets in a way where companies fairly compete and provide the public the best they can. Markets aren’t made from magic, we created them and decided their rules in a fashion which best serves us. These rules help promote competition and protect the people involved. If a monopoly goes unchecked in its power, it will care less about providing useful service and more about abusing its power. This is just as true for government monopolies. And even when a monopoly plays fair, a competitive market is just plain faster at moving around resources to where it’s most needed.",
        "passage-b": "In times of economic crisis, the federal government has a variety of tools it can use to fix things. One of the most underrated of these tools is nationalization: the government taking full control of a company or industry. Private companies tend to focus on their own profits, which in some situations can lead to them neglecting their responsibilities. This is especially a problem in industries like rail, energy, and healthcare which are vital parts of our nation’s infrastructure; when the company messes up, the whole country suffers. By having the government take control in these cases, we make these industries accountable to the public rather than shareholders, and allow for all sorts of social reform and programs to be implemented into industry.",
        "source-a": "https://democracyjournal.org/magazine/42/competitive-egalitarianism-how-to-structure-markets/",
        "source-b": "https://jacobin.com/2019/11/nationalization-is-as-american-as-apple-pie",
        "branch-a": "B-SLIB",
        "branch-b": "B-SDEM"
        
}










export default json;