const json = {
        "id": "F-LEADvPOPL",
        "leaf": "L-NATN",
        "set": "B",
        "question": "Is the core of a nation to be found in its leadership or its masses?",
        "passage-a": "People have a tendency to come together to form nations, but a nation is not merely a sum of individuals. True democracy goes beyond just the democracy of the majority, it understands national identity in terms of quality rather than quantity. It is through the conscience of the few that the true popular will can be expressed. The life of this nation rests not in a race or ruler, but rather instead a people historically perpetuating their existence, unified by an idea and empowered by a will to live.",
        "passage-b": "The important thing about leadership is its symbolic role. The very fact that, that by whatever series of historical accidents, citizens view a nation as legitimate is what determines its legitimacy. The source of the state’s authority is in the minds of its subjects. It is, however, in their intellects, not in their wills. A man can loathe his country, but that won't change the fact that he is forced to recognize it. The concept of a “people” is something that is derived from the existence of authority, there's nothing inherent which gives it its character.",
        "source-a": "http://www.worldfuturefund.org/wffmaster/Reading/Germany/mussolini.htm",
        "source-b": "https://bonald.wordpress.com/in-defense-of-monarchy/",
        "branch-a": "B-FASC",
        "branch-b": "B-MONR"
        
}










export default json;