const json = {
        "id": "F-INCRvGRAD",
        "leaf": "L-PROG",
        "set": "1A",
        "question": "How should a movement go about utilizing their political leverage?",
        "passage-a": "Any political effort must rest on some vision of a goal and some idea of how to bring it about. Prescription requires description. It’s not enough to demand better health. Achieving that requires some account of how the body operates, especially if radical intervention is proposed. If the aim is a decent life for everyone, then one must understand how the economy works. The last thing a leftist should want to do is destroy the wealth they hope to redistribute. If the goal is decent living for everyone, there’s technical and political aspects that need to be considered; if we appeal to the overlap between those who want to preserve capitalism and those who want to reform it to be more fair, we can create a winning coalition.",
        "passage-b": "The issue with reformism is that it fails to account for the fact that power is not primarily located in the legislature. Even in most robust of democracies, it’s private businesses which end up influencing public policy, and punishing politicians which don’t cooperate. To a movement that wishes to address this at the root, power can’t be found in just winning elections or passing law, rather instead carefully using those victories towards being able to build a movement that goes beyond capitalism. The coalition doesn’t define us, but rather instead gives us an in to make structural changes which make both the government and the workplace more democratic.",
        "source-a": "https://www.niskanencenter.org/socialists-for-capitalism/",
        "source-b": "https://www.jacobinmag.com/2018/08/democratic-socialism-social-democracy-nordic-countries",
        "branch-a": "F-REGLvNATL",
        "branch-b": "F-CNFLvCLLB"
        
}










export default json;