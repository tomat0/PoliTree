const json = {
        "id": "F-HOMEvUSUF",
        "leaf": "L-LIBT",
        "set": "2A",
        "question": "Do people have an inherent right to own land?",
        "passage-a": "It’s easy enough to say that every person has the right to his own body and work, but that still leaves us with a remaining question: who has the right to own natural resources? A farmer might own the fruit he grew, but who owns the land it grew on? Well, it belongs to the pioneer, the first man to put in the work to cultivate it. He may not have created the land, but he did make it useful through his efforts. Just like how the sculptor owns the statue he creates out of clay, the pioneer owns the resource upon which he builds his enterprise. All of the things made by men have only been possible because we were allowed to take natural resources. If we can't even guarantee this right, how can we say that anyone owns anything? Depriving a person of the former can only mean depriving them of the latter.",
        "passage-b": "When faced with the question of ownership of land, we begin to see the issue: it goes beyond the simple right of the producer. The farmer did not create the soil, it belongs to all. Yes, whoever tilled the soil has a right to compensation, but that compensation comes when it’s time for harvest. Even if he continued to cultivate it yearly, it would not change the fact that his ownership isn’t perpetual. The economy can exist completely independent of property: this fact has been demonstrated repeatedly throughout history. Humanity received the Earth as tenants, not proprietors. so who are we to say who can and can't access it? Our natural right extends to simple usage, anything beyond that has no basis outside of just pure law.",
        "source-a": "https://mises.org/library/justice-and-property-rights-failure-utilitarianism",
        "source-b": "http://www.ozarkia.net/bill/anarchism/library/ToPSummary-Proudhon.html#_ftn1",
        "branch-a": "B-ANCP",
        "branch-b": "B-MUTL"
        
}










export default json;