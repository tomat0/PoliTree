const json = {
        "id": "F-ALLOvCMPN",
        "leaf": "L-ARIS",
        "set": "B",
        "question": "Should resources and power be granted to those who haven't earned it?",
        "passage-a":  "Focusing on what’s fair distracts from our goal of helping everyone thrive. It promotes looking over one’s shoulder at how others are doing, wanting them to be worse off than us. Income isn’t a zero-sum game, success is found collectively rather than competitively. The sooner we turn away from questions of who deserves what and towards matters of growth and prosperity, the sooner we will be able to create conditions in which everyone can rise and flourish.",
        "passage-b":  "People are lazy by nature, only overcoming their laziness in order to obtain things which bring them pleasure. We give up part of what we create in order to fulfill the needs and desires we otherwise could not. This necessity, the law of self-interest, is the only one we understand and follow. Despite that, there remain those who contribute nothing but wish to consume the same as those who work. They spend their lives as parasites and thieves, coercing others into providing for them. The purpose of government is to prevent this sort of behavior from threatening industry: anything further and it quickly becomes the very evil it is designed to stop.",
        "source-a": "https://www.nationalaffairs.com/publications/detail/justice-inequality-and-the-poor",
        "source-b": "http://la.utexas.edu/users/hcleaver/368/368simonprinciples.html",
        "branch-a": "B-TCHN",
        "branch-b": "B-MERT"
        
}










export default json;