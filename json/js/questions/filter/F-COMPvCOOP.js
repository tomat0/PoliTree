const json = {
        "id": "F-COMPvCOOP",
        "leaf": "L-INTR",
        "set": "C",
        "question": "Is it moreso in countries' interests to cooperate or compete on the international stage?",
        "passage-a": "Countries need to be able to work together in order to accomplish their common goals and balance competing interests. This is often done because they know that by coming together and deciding on a set of rules, all countries can benefit. In our modern day, almost all nations on this planet are interconnected. What happens thousands of miles away will eventually have an effect in our home countries. Issues such as climate change won’t stop at any borders, making cooperation more important than ever today.",
        "passage-b": "While it can be argued that on the domestic scale governments are able to maintain order, the same cannot be said on the international level. There exists no international body with the power to regulate conflict between nations; the only rule that they abide by is the rule of self-interest. Proper global harmony may be a pipe-dream, but at the very least, a sense of order can be realized by maintaining a balance of power between these interests.",
        "source-a": "https://multilateralism.org/the-alliance/",
        "source-b": "https://www.e-ir.info/2011/08/29/realist-and-constructivist-approaches-to-anarchy/",
        "branch-a": "B-COSM",
        "branch-b": "B-REAL"
        
}










export default json;