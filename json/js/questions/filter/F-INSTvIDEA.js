const json = {
        "id": "F-INSTvIDEA",
        "leaf": "L-MODR",
        "set": "1A",
        "question": "Are ideas or institutions more important in bringing about political change?",
        "passage-a": "It is unquestionable that in the rules, norms, and practices of politics, that these are things based on ideas: for example, you cannot have a democracy without some idea of equality. Politics is not just about distributing resources or balancing power, but also a debate about the interpretation, direction, and meaning of social life. These are questions which require ideas to answer, and that is why any political institution finds its legitimacy rooted in ideas. Ideas set into motion actions, ideas give solutions, but they also generate new problems and challenges: the history of human society is a history of ideas. Policy links these ideas to politics, giving a venue whereby different actors can put forward their principles, goals, and analyses in order to determine and solve the problems most relevant to us today.",
        "passage-b": "Institutions are the rules of the game in a society; they give guidelines to how human beings are supposed to interact with each other. Institutions serve to structure our incentives towards a common good, and thus one must understand institutions in order to understand political change. Institutions set the rules for what individuals and groups can or cannot do, and the ways in which they can compete to affect political change. Political parties, churches, schools, all of these are groups of individuals who have some common interest, but they assert those interests and interact with the rest of the world is greatly shaped by what rules are set by institutions.",
        "source-a": "https://www.redalyc.org/journal/329/32971992001/html/",
        "source-b": "https://assets.cambridge.org/97805213/94161/excerpt/9780521394161_excerpt.pdf",
        "branch-a": "F-AGREvDISG",
        "branch-b": "F-INNVvPRSV"
        
}










export default json;