const json = {
        "id": "F-DOMNvSEPR",
        "leaf": "L-ARIS",
        "set": "A",
        "tiebreaker": true,
        "question": "Is it possible for nations to co-exist peacefully?",
        "passage-a": "Every unique ethnicity has a basic right to govern itself and the territory that belongs to it. Multiculturalism is based upon this absurd idea that cultures and ethnicities can be mixed while retaining their autonomy. Even in free societies, groups engage in identity politics, seeking to secure the well-being of their kind at the expense of others. When vastly different peoples are forced to live side by side, they either end up clashing over their irreconcilable differences or lose their identity in the process of homogenization. The only way to prevent this is to give each ethnicity the physical and cultural distance necessary to maintain their cultural autonomy.",
        "passage-b": "Civilization has always been a monopoly of the in-group, relegating outsiders to the status of barbarians. Not all peoples are equal and it’s naive to expect that nations will settle for an equal distribution of power. The status quo is constantly under flux, with new victors constantly looking to dethrone and take the place of the old in-groups. This is why that throughout history, the question of national borders has always been settled by force. It is a waste of time to accommodate the grievances of those who lose out when the only existing justice is the justice of the strong.",
        "source-a": "https://web.archive.org/web/20200830125734/https://demokracija.eu/focus/daniel-friberg-ethnopluralism-is-rooted-in-the-simple-idea-that-every-unique-ethnicity-has-the-right-to-govern-itself-on-territory-which-has-historically-belonged-to-it.html",
        "source-b": "http://www.fascism-archive.org/Dennis/TDWARchapter15.html",
        "branch-a": "B-IDNT",
        "branch-b": "B-FASC"
        
}










export default json;