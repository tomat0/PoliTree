const json = {
        "id": "F-ETHCvIDEO",
        "leaf": "L-NEWL",
        "set": "B",
        "question": "Is it ideology or morality which stops us from properly seeing the world for what it is?",
        "passage-a": "Morality gets in the way of transparent communication and coherent collective action. To “do the right thing” is to ignore one's own self-interest in the name of some abstract ideal. Human nature is about satisfying our own needs, and when we convince ourselves otherwise it's alienating. Wherever morality exists, communication is replaced by manipulation. Instead of speaking to me, a moralist tries to manipulate me by speaking to all the ways in which society has conditioned me, hoping that his brand of ideology may give him a hold on my thought and behavior. The manipulator skillfully picks up on my guilt and uses it to control me. The only really transparent, and thus revolutionary, communication is that which takes place when our selves and our desires are out in the open, when no morals, ideals, or constraints cloud the air. We will be amoralists, or we will be manipulators and manipulated. The only coherent organization is that in which we unite as individuals who are conscious of our desires, unwilling to give an inch to mystification and constraint, and unafraid to act freely in our own interests.",
        "passage-b": "The hallmark of ideology is that it begins from an answer or a conceptual framework and attempts to work backward from there. In order to resist ideology, we must start from questions rather than answers. Answers can alienate or stupefy, but questions seduce. Once enamored of a question, people will fight their whole lives to answer it. Questions precede answers and outlast them: every answer only perpetuates the question that begot it. Our task consists not of an answer, but a question: what does it mean to be free? This single word offers endless points of departure, endless mysteries. To speak of liberation as a project is an ideological way of thinking. But the more unreachable any utopia is, the more it motivates us to change how we live today. There is no “higher” reality than that of lived experience. Those who bury themselves in theory mistake their abstractions for real-world phenomena, when in fact they are only generalizations derived from individual experiences. Resistance means ceasing to ceasing to regard our ideas as possessing meaning apart from the ways we are able to put them into practice.",
        "source-a": "https://theanarchistlibrary.org/library/jay-amrod-and-lev-chernyi-beyond-character-and-morality",
        "source-b": "https://theanarchistlibrary.org/library/crimethinc-against-ideology-a-lecture-by-crimethinc",
        "branch-a": "B-EGOI",
        "branch-b": "B-PLFT"
        
}










export default json;