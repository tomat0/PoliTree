const json = {
        "id": "F-DEMOvORGN",
        "leaf": "L-COMM",
        "set": "1A",
        "question": "Is democracy an essential principle of socialism?",
        "passage-a": "Democracy is the rule of the majority. As long as the will of the majority remained unclear, people continued to be offered a bourgeois government under the disguise of democracy. But, that is changing now: their will is becoming increasingly clear. It’s to be found in these mass organizations, so how then can one oppose transferring all power to said organizations? To say otherwise is to reject that will of the majority, to reject democracy. It’d be to impose upon the people a government which can never realize that democratic ideal, which can never make decisions based on truly free, truly popular elections.",
        "passage-b": "Democracy as a pure principle has no inherent value. There is nothing about it which implies necessity or justice. In practice, democracy is nothing more than a form of organization which crudely presumes that the majority is always right and the minority is always wrong. When we speak of a socialist democracy, we mistakenly assume that the workers in question are a homogenous group; raw numbers alone do not make history. In fact an expanded electoral system can have the effect of discouraging workers from taking other forms of action.",
        "source-a": "https://www.marxists.org/archive/lenin/works/1917/jul/18.htm",
        "source-b": "https://www.marxists.org/archive/bordiga/works/1922/democratic-principle.htm",
        "branch-a": "F-DCNTvCENT",
        "branch-b": "F-CLSSvCPTL"
        
}










export default json;