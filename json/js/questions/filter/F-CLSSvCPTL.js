const json = {
        "id": "F-CLSSvCPTL",
        "leaf": "L-COMM",
        "set": "2B",
        "question": "Is communism still realized through the course of class-centered struggles?",
        "passage-a": "Within the course of the struggle emerges a new outlook, if only through realizing that things have changed, that we need to find another way. This is what gives revolutions a character that transcend class. This will be accentuated in the case of future communist revolutions, because it won't be just the activity of a certain class, but of all of humanity rising up against capital. The old theory goes that the working-class was supposed to destroy the capitalism to liberate the industrial forces behind it: only after this would communism begin. However, we have seen that capitalism does not imprison industry, but rather instead raises it to new heights: industry exists for the benefit of capital, not humanity.",
        "passage-b": "Under capitalism, the working-class can only be expected to assert its interests as a class. This has to be the goal until capitalist society suffers complete breakdown; said breakdown will have been brought about by this struggle for class-interests. It’s only at this point that we can begin talking about class-abolition and an alternative society. It’s precisely through the working class wielding political power that they can enforce transitional measures to bring about communism. This cannot be done overnight; only when classes no longer exist will the need for class struggle and political power disappear.",
        "source-a": "http://theanarchistlibrary.org/library/jacques-camatte-against-domestication",
        "source-b": "https://www.leftcom.org/en/articles/2014-10-07/the-period-of-transition-and-its-dissenters",
        "branch-a": "B-ULFT",
        "branch-b": "B-LCOM"
        
}










export default json;