const json = {
        "id": "F-INTPvPRSN",
        "leaf": "L-ARIS",
        "set": "A",
        "question": "Are interpersonal or personal skills more valuable in a person?",
        "passage-a": "A rational, objective pursuit of power ends up converging on the social good. Power can be used to accomplish all sorts of goals. Typically, the more savvy a person is, the likelier they will be to seek out that influence. Those who understand power instrumentally are better equipped to make alliances based on strategic rather than ideological considerations. A good ally need not share your ideals, only your interests. Our society is deceptive in that it's discouraged to speak about power in naked terms: we prefer to imagine positions are entirely based on “merit”. While talent does play a role, this view completely misses the games of competition and alliances which are central to politics. The purpose of power is not merely to serve as a “reward” to the deserving, but to facilitate cooperation and manage competition. Exactly who the winners and losers are matters less than the fact that people know where their place in the hierarchy is.",
        "passage-b": "Our culture puts a lot of weight on “people skills”. In many workplaces, the path to success is being well-liked: shaking hands, making small talk, and giving toasts. But when you strip away all the pretensions of office life and get to the work itself, you begin to see how this behavior can quickly become manipulative. It's a lot easier to act competent than it is to actually be competent. Just don’t talk a lot in meetings, and when you do you ask questions of the people who made assertions, or repeat and praise good points others made. That way you can safely piggyback off of the boldness and risk-taking of others. You find excuses to offload tedious work to others, and only take up the tasks which will get you credit and accolades. A society which prioritizes charm one where people use charm to get others to do their work. It may benefit the person doing it, but it harms the organization as a whole: risk-taking and innovation are discouraged in favor of office politics.",
        "source-a": "https://medium.com/@samo.burja/empire-theory-part-ii-power-dynamics-864b9eef4200",
        "source-b": "https://fortune.com/2023/01/30/male-mediocrity-at-work-strategic-incompetence/",
        "branch-a": "B-ELIT",
        "branch-b": "B-MERT"
        
}










export default json;