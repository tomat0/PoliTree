const json = {
        "id": "F-AGREvDISG",
        "leaf": "L-MODR",
        "set": "2A",
        "question": "Does productive discourse focus on both sides' disagreement or agreement?",
        "passage-a": "Complete tolerance can only exist if men are indifferent to each other, while complete intolerance leads to wars of extermination; both of these attitudes threaten the fabric of society. There’s always going to be differences in people’s interests which can only be resolved through a struggle; not a struggle between good and evil but two interpretations of good. Situations like this call for a competition of ideas in order to determine which side has more merit. Only the narrowest, one-track minds fail to understand that conflict followed by compromise is healthy. No man is more cowardly than the one who is incapable of conducting disputes with fairness, goodwill, and a constructive attitude towards defeat.",
        "passage-b": "We accomplish results by opening a dialogue between those on all sides of an issue; every viewpoint has validity and can be brought to the table to contribute to a solution. In order to progress as a society we must move past our differences and discover the common ground hiding beneath most disputes. This approach does not ignore diverse viewpoints, but rather folds them into the larger discussion. It is through the practice of constructive dialogue and interaction that we can create the space for innovative solutions to the problems facing us.",
        "source-a": "https://oll.libertyfund.org/title/stephen-liberty-equality-fraternity-lf-ed#Stephen_0021_308",
        "source-b": "https://ivn.us/2014/05/28/transpartisanship-new-idea-bring-people-together/",
        "branch-a": "B-SYNC",
        "branch-b": "B-PLUR"
        
}










export default json;