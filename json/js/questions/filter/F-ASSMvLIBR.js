const json = {
        "id": "F-ASSMvLIBR",
        "leaf": "L-POPL",
        "set": "A",
        "question": "By what process should minority groups attain equality?",
        "passage-a": "Ideally, in a democratic society, all men are created equal and raised to the same plane of opportunity. It’s only natural that freedom leads to equality as the low begin to imitate the high. As people begin traveling more, buying the same products, and speaking the same language, it becomes more and more difficult for segregation to continue. When immigrants arrive, they arrive looking for economic opportunity. Through assimilation, they are able to accomplish this despite the discrimination they face. Once they have achieved economic independence, they are able to comfortably re-embrace their heritage, contributing their original culture to the larger melting pot. In this sense, assimilation has not repressed nationality, it has liberated it.",
        "passage-b": "In a 'civilized' country, nationalism provides the most effective way of defending one’s culture. The colonized, without a nation to represent them, find their culture defenseless against assimilation. The nation is a necessity: it is the spring from which creativity and life flow, the means by which people are able to leave their impact on the world: without it, culture withers. When colonized peoples assert their national sovereignty through political struggles, a process of cultural reincarnation is kickstarted. There can be no other outcome, the very character of a truly popular struggle guarantees it. Natives who yearn for the culture of their homeland can’t be content with a mere national consciousness, they must be willing to take upon various forms of struggle in the name of national liberation." ,
        "source-a": "http://www.expo98.msu.edu/people/Kallen.htm",
        "source-b": "https://www.marxists.org/subject/africa/fanon/national-culture.htm",
        "branch-a": "B-CNAT",
        "branch-b": "B-LNAT"
        
}










export default json;