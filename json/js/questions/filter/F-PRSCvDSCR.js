const json = {
        "id": "F-PRSCvDSCR",
        "leaf": "L-INTR",
        "set": "A",
        "question": "From what standpoint should a country conduct its foreign policy?",
        "passage-a": "It must be seen to that all people are able to live with an assurance of safety, justice, and freedom. We intervene when we see violations of these rights, and will continue to until the wrongdoings are corrected and prevented from occurring again. Unless we take swift action to see that justice is done to others, justice will never be done to us. The world’s problem is our problem, and we stand as partners in this interest. Towards the realization of this ideal we will continue to fight and fight until the world becomes a place of equality and peace instead of one of conquest.",
        "passage-b": "At the center of the moral dilemma is this: values are universal, but still have to be implemented as part of a gradual process. We must conduct ourselves remembering that prudence is the prime virtue; world peace cannot be achieved by simply taking a universal ideal and forcing it upon the rest of the world; instead, there must be a strategy in place that is informed by the historical and cultural circumstances of each region. If the goal overshadows the reality, we find ourselves unable to answer to practical concerns; for substantiative change to occur we must be willing to temper our expectations and compromise where the situation calls for it.",
        "source-a": "https://avalon.law.yale.edu/20th_century/wilson14.asp",
        "source-b": "https://www.henryakissinger.com/speeches/the-limits-of-universalism/",
        "branch-a": "B-IDEA",
        "branch-b": "B-REAL"
        
}










export default json;