const json = {
        "id": "F-POLIvETHN",
        "leaf": "L-NATN",
        "set": "C",
        "tiebreaker": "B-IDNT",
        "question": "How does ethnicity factor into national identity?",
        "passage-a": "As I see it, our traditional duty tells us to honor our ancestors, but it doesn’t bother with the question of how they are selected, which is just taken for granted. The virtue of obedience tells us to just focus on deference to established authorities without trying to answer the unanswerable question of why political boundaries should be here rather than there or why the crown should go to this dynasty rather than that.",
        "passage-b": "People base their identity on narratives. All nations, regardless of political composition, have relied on some form of myth and heritage. This collective identity can be found in language, culture, territory, a feeling of belonging and a desire to live together. But its basis depends on what the individual primarily sees in themselves. Due to the nature of identity, what is constant is that individuals have an innate preference towards those they deem as being part of the same group.",
        "source-a": "https://bonald.wordpress.com/2016/06/10/cartesian-meditations-on-racial-identity/",
        "source-b": "https://s3-eu-west-1.amazonaws.com/alaindebenoist/pdf/on_identity.pdf",
        "branch-a": "B-MONR",
        "branch-b": "B-IDNT"
        
}










export default json;