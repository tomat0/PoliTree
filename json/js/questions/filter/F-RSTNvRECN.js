const json = {
        "id": "F-RSTNvRECN",
        "leaf": "L-ANAR",
        "set": "B",
        "question": "Are minorities justified in using violence against their oppressors?",
        "tiebreaker": "B-PACF",
        "passage-a": "Some pacifists say that they do fight back, just nonviolently: this is delusional. When faced with a bully you fight back, because that discourages him from going after you in the future; meekness does nothing but make it easier for the torment to continue. Just look at whenever there is protests: the police are reluctant to go after rioters, but they have an easy time barricading peaceful protesters. Ultimately, pacifism is shallow; it’s based upon an opposition to this general and meaningless concept of violence: not sexism, not racism, not authoritarianism, but a non-divisive challenge to violence is what they choose to focus on. Equating the violence of the oppressors and the violence of the oppressed is reductionist and ensures the pacifist’s own social standing at the expense of the victims’.",
        "passage-b": "Nonviolent resistance is not simply cowardice, passivity or complacency; it does resist. The nonviolent resister is just as opposed to the evil that he is standing against as the violent resister but he resists without violence. This method is non-aggressive physically but aggressive spiritually. Our enemy is the system that perpetuates oppression, not the individuals caught up in that system. Violence ends in bitterness, nonviolence ends in reconciliation and the birth of a new community. A boycott isn’t an end, but rather instead a means by which the oppressor feels a sense of shame, the end is redemption. The struggle is not just between the oppressor and oppressed, but justice and injustice. If we find victory, it will be a victory for not just the afflicted groups, but all of humanity.",
        "source-a": "https://theanarchistlibrary.org/library/peter-gelderloos-how-nonviolence-protects-the-state",
        "source-b": "https://faculty.atu.edu/cbrucker/Amst2003/Texts/Nonviolence.pdf",
        "branch-a": "B-LBMV",
        "branch-b": "B-PACF"
        
}










export default json;