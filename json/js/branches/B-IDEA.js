const json = {
    "id": "B-IDEA",
    "name": "Idealist",
    "leaf": "L-INTR",
    "subbranches": {
        "Neoconservatism": "",
        "Liberal Internationalism": ""
    },
    "definition": "<b>Idealists</b> view military and economic force as something that can be leveraged towards humanitarian ends, spreading their countries' political and economic values.",
    "notes": [
        "Both neoconservatism and liberal internationalism are considered types of idealism: what separates them is the values that they wish to promote. Neoconservatives focus on free markets and anti-communism, while liberal internationalists tend to focus on world peace and civil rights.",
        "Idealists are also marked by their strong defence of liberal democracy, which distinguishes them from socialist and fascist interventionists.",
        "Idealism is usually expressed from the perspective of the hegemon country, meaning that it can't be applied universally; most literature discusses the role of the United States specifically."
    ],
    "references": [
        "kagan_world-america-made",
        "fukuyama_end-of-history",
        "nau_conservative-internationalism",
        "ikenberry_liberal-leviathan"
    ],
    "reference-categories": []
}
export default json;