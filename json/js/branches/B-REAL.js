const json = {
    "id": "B-REAL",
    "name": "Realist",
    "leaf": "L-INTR",
    "subbranches": {
        "Offensive Realism": "",
        "Defensive Realism": ""
    },
    "definition": "<b>Realists</b> view each state as fundamentally self-interested and believe the best approach to foreign policy is by looking for an optimal balance of power rather than pursuing a universally moral world.",
    "notes": [
        "In their analysis, realists start from the point of 'how things are' rather than 'how things ought to be'.",
        "Realists can still have altruistic motivations, it's just underscored by a level of pessimism regarding the state of international politics.",
        "Classical realism regarded power struggles as a consequence of human nature, while neorealist theories tend to regard war as a consequence of the existing global system."
    ],
    "references": [
        "carr_twenty-years-crisis",
        "waltz_theory-of-international-politics",
        "mearsheimer_tragedy-of-great-power-politics"
    ],
    "reference-categories": []
}
export default json;