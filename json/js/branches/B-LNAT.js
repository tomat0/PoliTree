const json = {
    "id": "B-LNAT",
    "name": "Left Nationalist",
    "leaf": "L-POPL",
    "subbranches": {
        "Third-Worldism": ""
    },
    "definition": "<b>Left Nationalists</b> believe that capitalism and imperialism go hand in hand, calling for colonized nations to adopt a socialist economic model but also maintain a distinct national identity.",
    "notes": [
        "Left nationalism should be distinguished from conventional nationalism; each stems from countries which hold different roles in the global hegemony, and as a result develop vastly different outlooks on the world.",
        "Left-nationalists are often Marxist-Leninists, due to Lenin's early and influential contribution to the theory of imperialism.",
        "Unlike traditional Marxism however, left nationalism is typically much more insurrectionary, putting less focus on economic determinism and more focus on rebellion itself."
    ],
    "references": [
        "lenin_imperialism",
        "bauer_question-of-nationalities",
        "zedong_guerilla-warfare"
    ],
    "reference-categories": []
}
export default json;