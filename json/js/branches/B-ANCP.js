const json = {
    "id": "B-ANCP",
    "name": "Anarchocapitalist",
    "leaf": "L-LIBT",
    "subbranches": {
        "Agorism": ""
    },
    "definition": "<b>Anarcho-capitalists</b> believe that society can function without a government, with the free-market and private associations able to provide all necessary public services.",
    "notes": [
        "Unlike left-wing anarchists, anarchocapitalists believe that free-market capitalism is a force for good, and that society should be organized around it.",
        "Anarcho-capitalists believe in a 'non-aggression principle' where people have the right to do anything as long as it does not infringe on another person or their property.",
        "Due to them being heavily anti-coercion, their methods are often non-violent, either practicing counter-economics through black-market activity or choosing not to associate with statist society."
    ],
    "references": [
        "heumer_political-authority",
        "mises_human-action",
        "beito_the-voluntary-city"
    ],
    "reference-categories": []
}
export default json;