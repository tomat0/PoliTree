const json = {
    "id": "B-PLUR",
    "name": "Pluralist",
    "leaf": "L-MODR",
    "subbranches": {},
    "definition": "<b>Pluralists</b> advocate for a marketplace of ideas, believing that the resulting diversity of discourse is essential to maintaining a democratic culture.",
    "notes": [
        "Pluralism differs from Syncretism in that pluralism relies on discourse between wildly varying individuals for decision-making, whereas Syncretism centers the discussion around an explicit fusion of ideas.",
        "In this sense, pluralism isn't necessarily a form of government on its own, but rather instead an approach to solving problems."
    ],
    "references": [
        "berlin_against-the-current",
        "dahl_polyarchy",
        "lijphart_patterns-of-democracy"
    ],
    "reference-categories": []
}
export default json;