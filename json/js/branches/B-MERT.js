const json = {
    "id": "B-MERT",
    "name": "Meritocrat",
    "leaf": "L-ARIS",
    "subbranches": {},
    "definition": "Meritocrats believe that one should 'earn' their social/economic position by contributing to society via honest means (hard work, innovation, etc.).",
    "notes": [
        "Merit is referred to in the context of productive achievement: hard work, talent, and creativity are some examples of virtues that fall under 'merit'.",
        "Meritocracy is a normative position; a meritocrat does not necessarily have to believe the current society rewards merit, but they do believe that an ideal one does."
    ],
    "references": [
        "saint-simon_selected-writings",
        "veblen_leisure-class"
    ],
    "reference-categories": []
}
export default json;