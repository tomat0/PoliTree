const json = {
    "id": "B-FASC",
    "name": "Fascist",
    "leaf": "L-NATN",
    "subbranches": {
        "Italian Fascism": "",
        "National Socialism": "",
        "Arab Nationalism": ""
    },
    "definition": "<b>Fascists</b> view nationalism as a political religion, believing that a truly great leader must unite all people under one will and identity.",
    "notes": [
        "Fascists support a leadership that is strong both in image and in power in order to better unite and empower the community.",
        "Fascists typically adhere to the concept of palingenesis, or national rebirth; to the fascist, the nation has been deteriorated but is on the verge of a ressurection.",
        "The fascist typically is hostile to groups that threaten the sense of national unity, whether it be by their existence or actions."
    ],
    "references": [
        "bap_bronze-age-mindset",
        "sorel_reflections-on-violence",
        "carlyle_on-heroes",
        "evola_fascism-viewed-from-the-right"
    ],
    "reference-categories": []
}
export default json;