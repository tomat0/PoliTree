const json = {
    "id": "B-THUM",
    "name": "Transhumanist",
    "leaf": "L-ACCL",
    "subbranches": {
        "Extropianism": "",
        "Post-Humanism": ""
    },
    "definition": "<b>Transhumanists</b> seek to embrace the development of emerging technologies to push humanity and society beyond their natural limits.",
    "notes": [
        "Most transhumanist-adjacent ideologies tend to have a utopian bent, so there often is a focus on finding technological solutions to existing problems. Transhumanist fiction also exists to help envision what such a society may look like.",
        "It's difficult to pinpoint a specific political bent for transhumanists as there are varying interpretations on the ideological implications of expanding technology in such a fashion."
    ],
    "references": [
        "fuller-lipinska_proactionary-imperative",
        "more_diachronic-self"
    ],
    "reference-categories": []
}
export default json;