const json = {
    "id": "B-NEOL",
    "name": "Neoliberal",
    "leaf": "L-LIBT",
    "subbranches": {},
    "definition": "<b>Neoliberalism</b> believe that free markets are the most efficient way to bring about prosperity, and solve various social problems.",
    "notes": [
        "On non-economic issues, neoliberals are a lot more flexible than other libertarians, with a wide variety of compatible positions.",
        "Unlike other libertarians, neoliberals may support certain forms of economic intervention such as austerity and monetary policy.",
        "Unlike classical liberals, neoliberals see globalization favorably: this leads them to often support free trade and immigration."
    ],
    "references": [
        "george_protection",
        "friedman_program",
        "lucas_business-cycle"
    ],
    "reference-categories": []
}
export default json;