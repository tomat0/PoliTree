const json = {
    "id": "B-PACF",
    "name": "Pacifist",
    "leaf": "L-ANAR",
    "subbranches": {},
    "definition": "Pacifists believe that politics is currently founded upon the language of and use of violence, and that any meaningful systemic change first requires that we reconsider how we approach conflict rather than resorting to insurrection.",
    "notes": [
        "As states tend to uphold their authority and order through coercion, the majority of pacifist thinkers tend to be anarchist.",
        "Pacifism isn't passive, action is still taken avoiding violent means, usually involving civil disobedience and leading by example."
    ],
    "references": [
        "tolstoy_kingdom-of-god",
        "ellul_autopsy-of-revolution",
        "thoreau_civil-disobedience"
    ],
    "reference-categories": []
}
export default json;