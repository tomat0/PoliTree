const json = {
    "id": "B-SDEM",
    "name": "Social Democrat",
    "leaf": "L-PROG",
    "subbranches": {
        "Post-Keynesian": ""
    },
    "definition": "<b>Social Democrats</b> wish to actively use the state to steer the direction of the market in a fashion which meets both political and economic goals.",
    "notes": [
        "Social democrats are typically agnostic on the socialism/capitalism debate, focusing on widespread prosperity over overall growth, and state-ownership of enterprise over worker-ownership.",
        "Social democracy exists as an ideal worked towards, with some countries adopting aspects of it in a gradual fashion which balances economic sustainability with their goals.",
        "Social democracy distinguishes itself from social liberalism in that it pushes for the state to take a more active role in the market, which could include: nationalizing industry, pushing for full employment, and high rates of taxation."
    ],
    "references": [
        "mazzucato_entrepreneurial-state",
        "esping_three-worlds",
        "skidelsky_keynes"
    ],
    "reference-categories": []
}
export default json;