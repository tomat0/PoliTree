const json = {
    "id": "B-RCNT",
    "name": "Radical Centrist",
    "leaf": "L-MODR",
    "subbranches": {
        "Rationalist Movement": ""
    },
    "definition": "<b>Radical centrists</b> believe that the polarization and irrationality found in politics can be fixed by finding innovative ways to reform social institutions.",
    "notes": [
        "Radical centrism is a broad term used to refer to a variety of things, but in this context refers to proponents of deliberative democracy.",
        "Radical centrists are reformists, but look to reform the structure of political institutions themselves as opposed to just policies."
    ],
    "references": [
        "fishkin_democracy-when-people-are-thinking",
        "sunstein-kahneman-sibony_noise",
        "singer_most-good-you-can-do"
    ],
    "reference-categories": []
}
export default json;