const json = {
    "id": "B-PLFT",
    "name": "Post-Leftist",
    "leaf": "L-NEWL",
    "subbranches": {
        "Anti-Civ": ""
    },
    "definition": "<b>Post-Leftists</b> believe that the traditional leftist reliance upon mass organizations and grand theories gets in the way of being able to speak to everyday experience or truly understand freedom.",
    "notes": [
        "Because the post-left openly opposes the idea of an ideological tradition, the label applies to a wide array of beliefs and theories.",
        "Post-leftists believe that in order to truly be authentic, revolution should be fun and that seriousness is often used as a form of control.",
        "While influenced by egoists, post-leftists are not necessarily always egoists; autonomy is a concept that can be interpreted in an altruistic fashion."
    ],
    "references": [
        "black_abolition-of-work",
        "vaneigem_the-revolution-of-everyday-life",
        "perlman_against-leviathan"
    ],
    "reference-categories": []
}
export default json;