const json = {
    "id": "B-PATR",
    "name": "Paternalist",
    "leaf": "L-INTG",
    "subbranches": {
        "Christian Democracy": "",
        "Liberation Theology": "",
        "Progressive Conservatism": ""
    },
    "definition": "<b>Paternalists</b> see society as a collective organism, whose spiritual and material wellbeing are deeply connected. They believe that a virtuous society must both set standards for and provide for all of its members.",
    "notes": [
        "<b>The specifics and interpretations of morality/virtue vary from religion to religion.</b>",
        "Paternalism manifests in different ways across the spectrum. Christian democrats are typically moderate, liberation theology tends to be associated with the far-left, whereas national conservatism is more aligned with the far right.",
        "A lot of Western literature dealing with the topic comes from a Catholic/Christian viewpoint, however movements within other religions have been known to take upon these stances.",
        "What sets apart a paternalist from other conservatives is their belief that the government (or larger society in general) has the responsibility to promote the economic and moral wellbeing of all citizens."
    ],
    "references": [
        "polanyi_great-transformation",
        "lasch_culture-narcissism",
        "sturzo_church-and-state"
    ],
    "reference-categories": []
}
export default json;