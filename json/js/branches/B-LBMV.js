const json = {
    "id": "B-LBMV",
    "name": "Liberation Movements",
    "leaf": "L-ANAR",
    "subbranches": {
        "Intersectionality": "",
        "Radical Feminism": "",
        "Queer Liberation": "",
        "Animal Rights": "",
        "Anti-Colonialism": ""
    },
    "definition": "<b>Liberation Movements</b> focus on the various hierarchies and discriminations various marginalized groups face, often calling for radical action.",
    "notes": [
        "This branch deals in terms of <b>identity politics</b>, meaning that the specifics of one's worldview varies based on the identity in question.",
        "Liberation movements often operate off of a framework of 'kyriarchy', which is a catch-all term for the various ways in which social structures interact to reinforce power dynamics. ",
        "Forms of organization and action varies, some taking to a lens of critical theory, others taking to more performative forms of protest.",
        "Some movements are intersectional/overlapping, while others turn their focus towards only one group specifically.",
        "Because most intersectional frameworks rely on a concept known as standpoint theory (the idea that one's knowledge is tied to their personal experience), different authors have varying focuses and responses to other marginalized groups."
    ],
    "references": [
        "hanisch_personal-is-political",
        "de-beauvoir_second-sex",
        "fanon_wretched-of-the-earth"
    ],
    "reference-categories": []
}

export default json;