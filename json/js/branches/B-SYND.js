const json = {
    "id": "B-SYND",
    "name": "Syndicalist",
    "leaf": "L-COMM",
    "subbranches": {
        "De-Leonism": "",
        "Anarcho-syndicalism": ""
    },
    "definition": "<b>Syndicalists</b> support the transferring of control over production to labor unions, pushing revolution through a federated form of organization.",
    "notes": [
        "Syndicalists hold contrasting views on Marxism: some, such as De Leon, incorporated Marxian elements into their theory, while others rejected Marx's ideas as overly authoritarian.",
        "Syndicalists, despite being a labor-centered movement, share similar roots with anarchists: the theories of both derive inspiration from the works of Bakunin and Proudhon.",
        "Syndicalists are often anti-parlimentarian due to the centralized nature of political parties, preferring to funnel action through local unions."
    ],
    "references": [
        "rocker_anarcho-syndicalism",
        "proudhon_principle-of-federation",
        "ness_worker-organization"
    ],
    "reference-categories": []
}
export default json;