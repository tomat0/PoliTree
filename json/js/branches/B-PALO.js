const json = {
    "id": "B-PALO",
    "name": "Traditionalist Conservative",
    "leaf": "L-INTG",
    "subbranches": {
        "Fusionism/Neoconservatism": "",
        "Paleoconservatism": "",
        "Distributism": ""
    },
    "definition": "Traditionalist conservatives (or classical conservatives) take a bottom-up approach to politics, believing that order is best maintained through custom and traditional authority as opposed to sweeping government programmes.",
    "notes": [
        "<b>The specifics and interpretations of morality/virtue vary from religion to religion.</b>",
        "Unlike theocrats, conservatives prefer to rely on culture as opposed to law to steer the direction of society.",
        "While conservatives are skeptical of socialism and federal government, they do not necessarily hold individualism as a principle.",
        "Paleoconservatives support an isolationist foreign policy, whereas fusionists are strong believers in internationalism."
    ],
    "references": [
        "kirk_conservative-mind",
        "meyer_defense-of-freedom",
        "nisbet_quest-for-community"
    ],
    "reference-categories": []
}
export default json;