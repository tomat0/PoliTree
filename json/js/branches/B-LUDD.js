const json = {
    "id": "B-LUDD",
    "name": "Neo-Luddite",
    "leaf": "L-ACCL",
    "subbranches": {},
    "definition": "<b>Neo-Luddites</b> believe that the development of technology and its surrounding system has spiralled out of control, eroding natural human life in the process.",
    "notes": [
        "Neo-Luddites have various reasons for their opposition to technology, some radical, some reactionary.",
        "Neo-Luddites focus their critique on the rational logic underlying technology and the social relations which accompany it, rather than just the technologies themselves.",
        "Contrary to popular belief, the original Luddites did not have an ideological opposition to technology, but were instead a loose group of artisans displaced by industrialization. This means that Neo-Luddites do not necessarily share a heritage with their namesake."
    ],
    "references": [
        "ellul_technological-society",
        "scott_seeing-like-a-state",
        "tainter_complex-societies"
    ],
    "reference-categories": []
}
export default json;