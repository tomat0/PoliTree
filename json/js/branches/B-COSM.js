const json = {
    "id": "B-COSM",
    "name": "Cosmopolitanist",
    "leaf": "L-INTR",
    "subbranches": {
        "World Federalism": ""
    },
    "definition": "Cosmopolitanists wish to integrate all of humanity into a universal community, encouraging cooperation between nations as opposed to war.",
    "notes": [
        "There is debate among cosmopolitanists as to structure: some simply wish to increase involvement in existing international bodies like the United Nations while others look to form a world-government."
    ],
    "references": [
        "beck_globalization",
        "appiah_ethics-of-identity",
        "keohane_after-hegemony",
        "glossop_world-federation"
    ],
    "reference-categories": []
}
export default json;