const json = {
    "id": "B-ELIT",
    "name": "Elitist",
    "leaf": "L-ARIS",
    "subbranches": {},
    "definition": "Elitists believe that politics is about managing power struggles, and what often makes a good society comes down to the personal qualities of the people in charge.",
    "notes": [
        "Elitists tend to be heavily anti-libertarian, since the egalitarian ideals that arose out of the Enlightenment directly oppose their worldview."
    ],
    "references": [
        "michels_political-parties",
        "mosca_ruling-class",
        "burnham_defenders-of-freedom"
    ],
    "reference-categories": []
}
export default json;