const json = {
    "id": "B-DSOC",
    "name": "Democratic Socialist",
    "leaf": "L-PROG",
    "subbranches": {
        "Market Socialism": "",
        "Green politics": ""
    },
    "definition": "<b>Democratic socialists</b> support a society where the working class is responsible for economic and political decisions, but believe that reforming existing institutions is the best way to do so.",
    "notes": [
        "What distinguishes a democratic socialist a social democrat is that their end goal is still the eventual elimination of capitalism.",
        "The exact application of democratic socialism varies with the current institutions in place, so often times it acts more as a guiding philosophy than a specific methodology.",
        "Unlike other socialists, democratic socialists are still committed to the principles of liberalism and believe reform rather than revolution to be the best strategy."
    ],
    "references": [
        "bernstein_preconditions-of-socialism",
        "cole_guild-socialism",
        "schweickart_after-capitalism",
        "jackson_prosperity-without-growth"
    ],
    "reference-categories": []
}
export default json;