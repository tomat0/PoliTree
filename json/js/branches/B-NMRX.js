const json = {
    "id": "B-NMRX",
    "name": "Neo-Marxist",
    "leaf": "L-NEWL",
    "subbranches": {
        "Structural Marxism": "",
        "Marxism-Humanism": "",
        "Critical Theory (Frankfurt School)": "",
        "Post-Marxism": ""
    },
    "definition": "<b>Neo-Marxists</b> look to understand the effects of capitalism in both a philosophical and cultural context, looking to show how capitalism affects society as a whole, not just the working class.",
    "notes": [
        "Neo-Marxism is a rather broad term, and as a result, there isn't a consistent political doctrine or set of interpretations that can be properly considered 'neo-Marxist'."
    ],
    "references": [
        "lukacs_history-and-class-consciousness",
        "adorno-horkheimer_dialectic-of-enlightement",
        "sartre_search-for-a-method",
        "gramsci_prison-notebooks",
        "althusser_reproduction-of-capitalism"
    ],
    "reference-categories": []
}
export default json;