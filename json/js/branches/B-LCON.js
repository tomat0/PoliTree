const json = {
    "id": "B-LCON",
    "name": "Liberal Conservative",
    "leaf": "L-MODR",
    "subbranches": {},
    "definition": "<b>Liberal conservatives</b> view existing democratic institutions and norms as something to be continually and actively defended against extremism, sectarianism, and demagogues.",
    "notes": [
        "Liberal conservatives are not necessarily social conservatives, what they seek to conserve is the culture and structure of democracy itself.",
        "Liberal conservatives are not entirely opposed to change, but cautious about what unintended consequences it may bring. In some cases, reform may be necessary for the health of society."
    ],
    "references": [
        "burke_reflections-on-revolution",
        "oakeshott_politics-of-scepticism",
        "fukuyama_identity"
    ],
    "reference-categories": []
}
export default json;