const json = {
    "id": "B-SYNC",
    "name": "Syncretist",
    "leaf": "L-MODR",
    "subbranches": {
        "Third Way": ""
    },
    "definition": "Syncretists (or syncretic liberals) see the best solutions as often the ones which freely pick and combine elements from various 'ideologically pure' platforms.",
    "notes": [
        "While the pluralist sees the competition of ideas as healthy, the syncretist sees their combination as more useful towards creating solutions.",
        "The syncretist doesn't exalt the middle position, but rather instead draw inspiration from a variety of sources."
    ],
    "references": [
        "rawls_political-liberalism",
        "popper_the-open-society",
        "giddens_beyond-left-and-right"
    ],
    "reference-categories": []
}
export default json;