const json = {
    "id": "B-IDNT",
    "name": "Identitarian",
    "leaf": "L-NATN",
    "subbranches": {
        "Ethnopluralism": "",
        "Supremacism": ""
    },
    "definition": "<b>Identitarians</b> believe human beings have a natural affinity for those of the same ethnicity, and a stable society is one which is as homogenous as possible.",
    "notes": [
        "This branch deals in terms of <b>identity politics</b>, meaning that the specifics of one's worldview varies based on the identity (in this case, ethnic identity) in question.",
        "Identitarians hold varying definitions and standards for race, some seeing blood-relation literally while others see it as something more abstract and metaphorical.",
        "While a lot of identitarian writings center around the Western viewpoint, identitarian sects can be found across the world under different names.",
        "Because the identitarian takes a defensive stance, the identitarian is almost always the majority ethnic group in their nation, attempting to stave off the loss of that majority status."
    ],
    "references": [
        "spengler_decline-of-the-west_2",
        "badiou_metapolitics",
        "faye_archeofuturism"
    ],
    "reference-categories": []
}
export default json;