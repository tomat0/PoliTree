const json = {
    "id": "B-TCHN",
    "name": "Technocrat",
    "leaf": "L-ARIS",
    "subbranches": {
        "Corporatism": "",
        "Ordoliberalism": "",
        "Socialism With Chinese Characteristics": ""
    },
    "definition": "<b>Technocrats</b> support the micromanagement of the economy by experts and specialists; they believe that a proper execution is capable of bringing a net good to society as a whole.",
    "notes": [
        "The concept of merit is more nuanced in a technocracy, more suited to what role a person is suited to given their talents rather than a linear conception of worth.",
        "Political authority is not selected democratically in a technocracy, but rather instead based on one's expertise/ability to contribute with said posiition.",
        "Technocracy can be implemented in various forms of government, from bureaucratic oligarchy to monarchy, to a republic."
    ],
    "references": [
        "waring_taylorism-transformed",
        "bentham_morals-and-legislation",
        "drucker_concept-of-the-corporation"
    ],
    "reference-categories": []
}
export default json;