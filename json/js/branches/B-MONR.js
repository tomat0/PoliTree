const json = {
    "id": "B-MONR",
    "name": "Monarchist",
    "leaf": "L-NATN",
    "subbranches": {
        "Divine-Right Monarchism": "",
        "Enlightened Absolutism": "",
        "Constitutional Monarchism": "",
        "Falangism": ""
    },
    "definition": "<b>Monarchists</b> believe that a nation is best unified by a single, sovereign ruler who the people show their loyalty to.",
    "notes": [
        "The difference between a monarchist and a fascist is that fascists are populists while monarchists are elitists. Fascists believe their leader is channeling the 'popular will' whereas monarchists stress obedience to authority for its own sake.",
        "Although monarchies have typically been hereditary, a familial line of succession isn't an essential characteristic."
    ],
    "references": [
        "erik_liberty-or-equality",
        "graeber-sahlins_on-kings",
        "bagehot_english-constitution"
    ],
    "reference-categories": []
}
export default json;