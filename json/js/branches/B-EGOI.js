const json = {
    "id": "B-EGOI",
    "name": "Egoist",
    "leaf": "L-NEWL",
    "subbranches": {
        "Egoist-Communism": ""
    },
    "definition": "<b>Egoists</b> are critical of moral codes, believing that a society where everyone openly pursues their own self-interest is one which is truly free.",
    "notes": [
        "Egoism can be interpreted multiple ways: some egoists see self-interest as a personal choice, others see it as a universal good, and others believe all interests should serve them specifically.",
        "Because of the self-centered nature, egoist theory is often polemical and deconstructive rather than constructive; in other words, the construction is typically expected to be filled in by the individual."
    ],
    "references": [
        "nietzsche_genealogy-of-morality",
        "stirner_ego-and-its-own",
        "mcquinn_critical-self-theory"
    ],
    "reference-categories": []
}
export default json;