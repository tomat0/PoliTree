const json = {
    "id": "B-FUTR",
    "name": "Futurist",
    "leaf": "L-ACCL",
    "subbranches": {
        "Left-Accelerationism": ""
    },
    "definition": "<b>Futurists</b> see our inability to think past the present as the very thing that maintains the present; to the futurist, envisioning a future is in and of itself a revolutionary act.",
    "notes": [
        "The term futurism originates with a 20th century art movement that attempted to move past tradition through hypermodern art. The movement itself was a relic of its time, but it proved inspirational.",
        "While the original futurists aligned themselves with the Italian fascists, a left-wing strain has emerged too, especially out of the works of Fisher."
    ],
    "references": [
        "reynolds_retromania",
        "land_fanged-noumena",
        "marinetti_futurist-cookbook"
    ],
    "reference-categories": []
}
export default json;