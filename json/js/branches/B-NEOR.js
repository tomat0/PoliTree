const json = {
    "id": "B-NEOR",
    "name": "Neo-Reactionary",
    "leaf": "L-ACCL",
    "subbranches": {
        "Neo-Feudalism": ""
    },
    "definition": "<b>Neo-Reactionaries</b> oppose the progressive ideals of the modern era (such as democracy and equality), believing that freedom and democracy are entirely incompatible.",
    "notes": [
        "Neo-reactionaries look to fuse the technological and economic progress of capitalism with the hierarchical and elitist values of feudal society.",
        "Neo-reactionaries reject the common idea that history progresses towards freedom and rationality, believing that the days of democracy are numbered.",
        "Neo-reactionaries often support combining current corporate structures with older forms of government in order to form what they see as the model of the future."
    ],
    "references": [
        "land_dark-enlightenment",
        "butterfield_whig-history",
        "murray+herrnstein_bell-curve",
        "taleb_antifragile"
    ],
    "reference-categories": []
}

export default json;