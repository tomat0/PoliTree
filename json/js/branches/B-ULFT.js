const json = {
    "id": "B-ULFT",
    "name": "Ultra-Left",
    "leaf": "L-COMM",
    "subbranches": {
        "Autonomism": "",
        "Situationist Internationale (historical)": "",
        "Communization Theory": ""
    },
    "definition": "<b>Ultra-Leftists</b> believe that communism must center itself around the day-to-day struggles and routines of workers rather than broader questions of historical and political organization.",
    "notes": [
        "Many of those referred to as 'ultra-left' disown a singular doctrine or legacy, making it more difficult to identify.",
        "Traditional socialists have tended to give more weight to leadership and organization in revolution whereas the ultra-left puts more emphasis on spontaneous forms of resistance in times of crisis.",
        "Unlike anarchists, the rejection of centralized forms of organization is not due to a libertarian principle, but rather instead a rejection of bureaucracy and mediation."
    ],
    "references": [
        "camatte_capital-and-community",
        "la-banquise_recollecting-our-past",
        "tronti_workers-and-capital",
        "debord_society-of-the-spectacle",
        "dauve_crisis-to-communisation"
    ],
    "reference-categories": []
}
export default json;