const json = {
    "id": "B-MUTL",
    "name": "Mutualist",
    "leaf": "L-LIBT",
    "subbranches": {
        "Geolibertarianism": ""
    },
    "definition": "<b>Mutualism</b> is a form of pro-market anarchism which holds that property and land belong to those who actually perform the work upon it.",
    "notes": [
        "To mutualists, there are two forms of property that have been lumped under the same term. Legitimate property is justified through labor, while illegitimate property is maintained through coercion and force.",
        "Mutualists often employ a strategy of dual power, creating parallel, libertarian institutions while still in the context of coercive society.",
        "Mutualists have been known to draw from both left and right wing influences, some moreso than others."
    ],
    "references": [
        "proudhon_what-is-property",
        "proudhon_general-idea",
        "carson_mutualist-political-economy"
    ],
    "reference-categories": []
}
export default json;