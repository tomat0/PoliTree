const json = {
    "id": "B-POMO",
    "name": "Postmodernist",
    "leaf": "L-NEWL",
    "subbranches": {},
    "definition": "<b>Postmodernists</b> believe that the language and frameworks we have used to make sense of reality has given us a false impression of the world.",
    "notes": [
        "Some examples of the concepts postmodernists are skeptical about include: either-or language, group identity, historical progress, universal morality, and objective truth.",
        "Many postmodernists focus on analyzing language, as they believe the way we speak implicitly reveals many assumptions we take for granted."
    ],
    "references": [
        "baudrillard_illusion-of-the-end",
        "foucault_archaeology-of-knowledge",
        "foucault_discipline-and-punish",
        "derrida_dissemination"
    ],
    "reference-categories": []
}

export default json;