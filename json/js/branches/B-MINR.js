const json = {
    "id": "B-MINR",
    "name": "Classical Liberal",
    "leaf": "L-LIBT",
    "subbranches": {
        "Neoclassical Liberalism": "",
        "Paleolibertarianism": "",
        "Objectivism": "",
        "Libertarian Paternalism": ""
    },
    "definition": "<b>Classical liberals</b> believe that economic freedom and personal freedom are inseparable. They support a small state and argue that one's right to property and personal choice are inalienable rights.",
    "notes": [
        "Classical liberals believe only in 'negative rights': things society is not allowed to do to you. Unlike regular liberals they do not believe in 'positive rights', things society is entitled to provide for you.",
        "Classical liberalism does not oppose the state on a fundamental level, but rather seeks to minimize its role and hold it accountable with systematic checks and constitutions.",
        "Classical liberals have varying views on social justice, with some believing the market can be leveraged to enact social change and others distrusting the concept altogether."
    ],
    "references": [
        "nozick_anarchy-state-utopia",
        "mises_bureaucracy",
        "rothbard_americas-great-depression",
        "thaler-sunstein_nudge"
    ],
    "reference-categories": []
}
export default json;