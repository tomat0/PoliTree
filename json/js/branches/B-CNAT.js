const json = {
    "id": "B-CNAT",
    "name": "Civic Nationalist",
    "leaf": "L-POPL",
    "subbranches": {},
    "definition": "<b>Civic Nationalists</b> believe that basis of a nation is best found in it shared civic and political values, as opposed to ethnicity.",
    "notes": [
        "Civic nationalists welcome at least some level of multiculturalism, with the extent varying among them.",
        "Civic nationalists almost always support a form of representative democracy as a form of government.",
        "Unlike ethnonationalists, civic nationalists are neutral towards the topic of race."
    ],
    "references": [
        "anderson_imagined-communities",
        "glazer_melting-pot",
        "huntington_clash-of-civilizations"
    ],
    "reference-categories": []
}
export default json;