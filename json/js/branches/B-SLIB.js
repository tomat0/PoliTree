const json = {
    "id": "B-SLIB",
    "name": "Social Liberal",
    "leaf": "L-PROG",
    "subbranches": {},
    "definition": "<b>Social Liberals</b> support a market economy, but one in which the government passively intervenes in order to keep the market in check and provide economic opportunity to the needy.",
    "notes": [
        "Social liberals are not socialist, but rather instead support the government passively intervening via subsidies, welfare, or regulation.",
        "Social liberals differ from social democrats in that they still rely on private businesses to guide the direction of industry.",
        "Free-trade is a question of contention among social liberals, with some favoring protectionism while others oppose it."
    ],
    "references": [
        "galbraith_american-capitalism",
        "tullock_calculus-of-consent",
        "krugman_rethinking-international-trade"
    ],
    "reference-categories": []
}
export default json;