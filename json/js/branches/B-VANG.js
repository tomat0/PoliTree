const json = {
    "id": "B-VANG",
    "name": "Vanguardist",
    "leaf": "L-COMM",
    "subbranches": {
        "Marxism-Leninism (Stalinism)": "",
        "Trotskyism": "",
        "Maoism": ""
    },
    "definition": "<b>Vanguardists</b> (or Leninists) support a centralized, yet transparent political party of class-conscious individuals to help carry out the revolutionary process.",
    "notes": [
        "Various splits have occurred through history: in the USSR, the disagreements of Stalin and Trotsky created two camps, and during the rise of China, the disagreement between Mao and Stalin's successors created another major divide.",
        "In order to guide the transition from capitalism to proper communism, vanguardists will often advocate for a centralized state to shift power to the workers.",
        "During this lower-stage phase of socialism, workers are compensated based upon their contribution, as production becomes the property of society as a whole.",
        "Vanguardists have varying views on international politics, some advocating for utilizing the transition state to assist foreign revolutions, while others stress the importance of building internal strength first."
    ],
    "references": [
        "lenin_state-and-revolution",
        "lenin_what-is-to-be-done",
        "stalin_problems-of-leninism",
        "trotsky_permanent-revolution",
        "jmp_continuity-and-rupture"
    ],
    "reference-categories": []
}
export default json;