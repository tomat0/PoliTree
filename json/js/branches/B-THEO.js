const json = {
    "id": "B-THEO",
    "name": "Theocrat",
    "leaf": "L-INTG",
    "subbranches": {
        "Christian Reconstructionism": "",
        "Traditional Catholicism": "",
        "Islamism": ""
    },
    "definition": "<b>Theocrats</b> believe that the path to creating a more moral society is having a strong central government to actively enforce religious law.",
    "notes": [
        "This branch deals in terms of <b>identity politics</b>, meaning that the specifics of one's worldview varies based on the identity (in this case, religious identity) in question.",
        "For the theocrat, the ruler is meant to act as the relay between divine law and human society."
    ],
    "references": [
        "bossuet_politics-drawn",
        "qutb_milestones"
    ],
    "reference-categories": []
}
export default json;