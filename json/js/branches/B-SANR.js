const json = {
    "id": "B-SANR",
    "name": "Social Anarchist",
    "leaf": "L-ANAR",
    "subbranches": {
        "Anarchocommunism": "",
        "Communalism": ""
    },
    "definition": "<b>Social anarchists</b> see anarchism not just as a statement of freedom, but also as the responsibility of the individual towards the collective good; with societal organization beginning at the local community.",
    "notes": [
        "Social anarchism doesn't necessarily oppose government alone, but more specifically vertical forms of authority. They generally promote horizontal and democratic institutions as alternatives to statism.",
        "Social anarchists are more collectivist than individualist anarchists; they oppose self-interest as the ultimate good, believing what's good for society as a whole to be the ultimate good.",
        "While social anarchists take a lot of inspiration from syndicalism, their focus more primarily political and less economic.",
        "There's a good deal of overlap between social anarchist and environmental movements, although this is not essential or exclusive."
    ],
    "references": [
        "mauss_the-gift",
        "kropotkin_conquest-of-bread",
        "bookchin_ecology-of-freedom"
    ],
    "reference-categories": []
}
export default json;