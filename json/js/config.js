const json = {
		"id": "J-CONFIG",
        "gateway": "ipfs://",
        "icon-path": "./resources/icons/",
        "default-additional-references-message": "more specific tendencies"
}

export default json;