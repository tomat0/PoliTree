const json = {
    "id": "lucas_business-cycle",
    "title": "Studies in Business-Cycle Theory",
    "author": "Robert E. Lucas Jr.",
    "section": "B-NEOL",
    "subsection": false,
    "pdf": "bafykbzaced25fbeo5h4ekjkd3bwr3rv4hhzjdje4qgmhr24zhm44ltqlhymlm",
    "epub": "",
    "length": 2.5,
    "difficulty": 2.5,
    "genre": "Economics",
    "type": "Theory",
    "summary": "A collection of papers by Lucas providing an alternative interpretation of economic fluctuations, focusing on long-term trends and non-discretionary factors."
}
export default json;