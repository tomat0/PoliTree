const json = {
    "id": "dauve_crisis-to-communisation",
    "title": "From Crisis to Communisation",
    "author": "Giles Dauve",
    "section": "B-ULFT",
    "subsection": true,
    "pdf": "bafykbzacea2chbejdpmaw3yvljw4w2hxf7azzo4emrbaqxrhbcxlobxceytyi",
    "epub": "bafykbzacedbl2fsplizy2pvidefqg3x6a2t3posjx55osa6bfxqse6ge3mpai",
    "length": 1,
    "difficulty": 1.5,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Dauve more extensively discusses the relation between theory, practice, and revolution in the wake of the insurrections of the late 20th century, stressing the importance of communism not just as a goal, but as an active, yet spontaneous process."
}
export default json;