const json = {
    "id": "baudrillard_illusion-of-the-end",
    "title": "The Illusion of the End",
    "author": "Jean Baudrillard",
    "section": "B-POMO",
    "subsection": false,
    "pdf": "bafykbzacea23c5yrykyaoabtma5rinkklqywjzmnkepfnarjjsmtopuh6ky3u",
    "epub": "",
    "length": 1,
    "difficulty": 2,
    "genre": "History",
    "type": "Critique",
    "summary": "Confronted with the stalling of revolutionary movements worldwide, Baudrillard rejects the 'end of history' narrative, arguing instead that there never truly was a teleological history to begin with."
}
export default json;