const json = {
    "id": "marx_1844-manuscripts",
    "title": "Economic and Philosophic Manuscripts of 1844",
    "author": "Karl Marx",
    "section": "L-COMM",
    "subsection": false,
    "pdf": "bafykbzacedf6tvzg4eqdv5o6qctfsvfzfwwvrm7rqgknt7wekdfbedyigipf6",
    "epub": "bafykbzaceawzqfvweuxz6jzyl7zjcqdfjanktmhjpzjcgqtderhexkeoqmkos",
    "length": 0.5,
    "difficulty": 1.5,
    "genre": "Sociology",
    "type": "Theory",
    "summary": "A loose collection of writings by a younger Marx on various topics related to political economy and their direct effects on the lives of workers."
}
export default json;