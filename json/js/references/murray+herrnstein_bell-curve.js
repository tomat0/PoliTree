const json = {
    "id": "murray+herrnstein_bell-curve",
    "title": "The Bell Curve: Intelligence and Class Structure in American Life",
    "author": "Charles Murray and Richard J. Herrnstein",
    "section": "B-NEOR",
    "subsection": false,
    "pdf": "bafykbzaceaerewraytmdb7blgvoyxy5ewpsmvnfrq2kdzb5fua2ecax65fhko",
    "epub": "bafykbzacedtmjoejwu47t6rnbkxtkdv3kymgdspfxhxzabuise2fpsbeenyuc",
    "length": 4.5,
    "difficulty": 3.5,
    "genre": "Psychology",
    "type": "Theory",
    "summary": "This controversial book discusses the nature of intelligence, factors that contribute to disparities in intelligence, and what society can do as a whole to raise its collective intelligence."
}
export default json;