const json = {
    "id": "huntington_clash-of-civilizations",
    "title": "The Clash of Civilizations and the Remaking of the World Order",
    "author": "Samuel P. Huntington",
    "section": "B-CNAT",
    "subsection": false,
    "pdf": "bafykbzacecg67endod4gahfxarofqktxcwxxorkbwn5nj3ot7qalhte54stpm",
    "epub": "bafykbzacecfganz73fzce324mebkifrll2t6ie6kfayl3z3av224r33hq5dbg",
    "length": 2.5,
    "difficulty": 2,
    "genre": "International Relations",
    "type": "Critique",
    "summary": "Huntington contends that as globalization progresses, conflict will begin to arise along civilizational lines due to cultural differences that cannot be simply assimilated."
}
export default json;