const json = {
    "id": "carson_mutualist-political-economy",
    "title": "Studies in Mutualist Political Economy",
    "author": "Kevin Carson",
    "section": "B-MUTL",
    "subsection": false,
    "pdf": "bafykbzacec75fvlzd5nm6bq6y4iocv2n6mwvu2v5kgkfabxh3zxk6axra65ry",
    "epub": "bafykbzaceaqsjtc4cnmfaxns7vvhaioqmnbsm7k7alfurkgnzykobeuanysva",
    "length": 1,
    "difficulty": 2.5,
    "genre": "Economics",
    "type": "Theory",
    "summary": "Carson gives an answer to the devlopments in economics over the past century by analyzing, critiquing, and incorporating Austrian economics into mutualist tradition."
}
export default json;