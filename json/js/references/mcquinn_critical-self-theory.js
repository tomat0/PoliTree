const json = {
    "id": "mcquinn_critical-self-theory",
    "title": "Critical Self-Theory & the Non-Ideological Critique of Ideology",
    "author": "Jason McQuinn",
    "section": "B-EGOI",
    "subsection": false,
    "pdf": "",
    "epub": "",
    "length": 0.5,
    "difficulty": 3,
    "genre": "Politics",
    "type": "Critique",
    "summary": "McQuinn attacks previous forms of leftist critique, developing an anti-ideological alternative to critical theory that centers on the individual's perception."
}
export default json;