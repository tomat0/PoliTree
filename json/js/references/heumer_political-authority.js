const json = {
    "id": "heumer_political-authority",
    "title": "The Problem of Political Authority",
    "author": "Michael Heumer",
    "section": "B-ANCP",
    "subsection": false,
    "pdf": "bafykbzacecxk7hofhb4vzhbgxt2icc3mjdkpltodyudaruve7io2uv2ntbugw",
    "epub": "",
    "length": 1.5,
    "difficulty": 0,
    "genre": "Politics",
    "type": "Critique",
    "summary": "Huemer re-examines social contract theory in order to question the legitimacy of the state itself and other assumptions regarding political authority."
}
export default json;