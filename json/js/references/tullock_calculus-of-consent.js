const json = {
    "id": "tullock_calculus-of-consent",
    "title": "The Calculus of Consent",
    "author": "James M. Buchanan and Gordon Tullock",
    "section": "B-SLIB",
    "subsection": false,
    "pdf": "bafykbzacea2mo5bnyrctvutbx6ppykocyetmrbacqqvbkumpah4rcbrxplmcq",
    "epub": "",
    "length": 1,
    "difficulty": 2,
    "genre": "Economics",
    "type": "Theory",
    "summary": "Tullock and Buchanan analyze democratic game theory from an economic lens, producing an elaborate theory of how special interests operate within democracies."
}
export default json;