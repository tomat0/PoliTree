const json = {
    "id": "ilyin_legal-consciousness",
    "title": "On the Essence of Legal Consciousness",
    "author": "Ivan Il'in",
    "section": "L-NATN",
    "subsection": false,
    "pdf": "bafykbzaceauxyyzh6muaf6dnfuvlkfduzeltrgrbnqhjsisypqqh2karpnvp6",
    "epub": "bafykbzacecwcvfrtbgt2kqu2uxqje6l2zccznrsjiubjlm4sp4gwvdn3ak5g2",
    "length": 2.5,
    "difficulty": 2,
    "genre": "Politics",
    "type": "Theory",
    "summary": "In this work of legal theory, Il'in attempts to answer the fundamental questions of where 'rule of law' comes from, and how people are to respect authority. He argues that innate within the people is a collective understanding of what law is, and how the state's job is to assert this conception of law through authority."
}

export default json;