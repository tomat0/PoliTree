const json = {
    "id": "meyer_defense-of-freedom",
    "title": "In Defense of Freedom",
    "author": "Frank Meyer",
    "section": "B-PALO",
    "subsection": false,
    "pdf": "bafykbzaceaqpuvokuowx2ctrrchclycpwybii2fpboouysb2mj2bewrfa7y2c",
    "epub": "",
    "length": 1.5,
    "difficulty": 1,
    "genre": "Politics",
    "type": "Seminal",
    "summary": "Meyer challenges the notion of social conservatism and libertarianism being mutually exclusive struggles, laying out a case for their synthesis and shared struggle."
}
export default json;