const json = {
    "id": "stalin_problems-of-leninism",
    "title": "Problems of Leninism",
    "author": "Joseph V. Stalin",
    "section": "B-VANG",
    "subsection": true,
    "pdf": "bafykbzacea3shdchnpllfl5n5er5wzkoc6gg24asdtpgdw5qt5k2xzsm27ejy",
    "epub": "",
    "length": 4.5,
    "difficulty": 2,
    "genre": "Politics",
    "type": "Defense",
    "summary": "Inheriting the legacy of Lenin, Stalin looks to address the controversies of his time and defends his interpretations and applications of Leninist theory."
}
export default json;