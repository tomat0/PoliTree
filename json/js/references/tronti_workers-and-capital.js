const json = {
    "id": "tronti_workers-and-capital",
    "title": "Workers and Capital",
    "author": "Mario Tronti",
    "section": "B-ULFT",
    "subsection": true,
    "pdf": "bafykbzacedqxre7jam5cx3oredhv3zwrd3tdgugkab3e7xnazrbapj36lbjzi",
    "epub": "bafykbzacea3kvfaowrhuepnvxg4zxocy7aeuxny53ycfn3nyc72pd4ldmh6m2",
    "length": 1.5,
    "difficulty": 1.5,
    "genre": "Politics",
    "type": "Tactics",
    "summary": "Tronti reconstructs a theory of communist strategy, primarily focusing on questions of revolutionary organization and the autonomists' reinterpretation of the proletariat."
}
export default json;