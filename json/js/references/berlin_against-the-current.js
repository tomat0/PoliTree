const json = {
    "id": "berlin_against-the-current",
    "title": "Against The Current: Essays in The History of Ideas",
    "author": "Isaiah Berlin",
    "section": "B-PLUR",
    "subsection": false,
    "pdf": "bafykbzacec7hassw2glaion3xuaez6w5y2tumlv7nbf7fv7e6xdopyaidpnw6",
    "epub": "bafykbzaceawkjv2n2durgnddktpmawusbrdhr54p4eua2itb5tx3dcad6bsgm",
    "length": 2.5,
    "difficulty": 1,
    "genre": "History",
    "type": "Critique",
    "summary": "Berlin looks at various influential figures throughout history and their position on the political margins of their time; from this, he makes the argument that it is necessary to protect the minority opinion."
}
export default json;