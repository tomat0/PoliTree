const json = {
    "id": "foucault_archaeology-of-knowledge",
    "title": "Archaeology of Knowledge",
    "author": "Michel Foucault",
    "section": "B-POMO",
    "subsection": false,
    "pdf": "bafykbzaceda33pxf6jl5wl3y4qtutqod3o4xb5kruvbd2q6wa2o6krqp7geta",
    "epub": "bafykbzaceds3lrjy2ecplqzu2et7civfh7pc4frmzozsl2ii2hf2hr7eifbia",
    "length": 1.5,
    "difficulty": 2.5,
    "genre": "Philosophy",
    "type": "Theory",
    "summary": "Foucault deals with questions of knowledge and ideas through the lens of language and discussion, distingushing his methodology from the purely systemic analysis others take."
}
export default json;