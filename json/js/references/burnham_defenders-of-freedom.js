const json = {
    "id": "burnham_defenders-of-freedom",
    "title": "The Machiavellians: Defenders Of Freedom",
    "author": "James Burnham",
    "section": "B-ELIT",
    "subsection": false,
    "pdf": "bafykbzaced65dawk5swufanrneerdhetjlseofbbyvmnaprx5hxx4xuq6pkqi",
    "epub": "bafykbzacecvv4mg2gpia2mno4totlb5cjoykk5qgel5qfnbzekab6ln6frw3g",
    "length": 0.5,
    "difficulty": 1,
    "genre": "History",
    "type": "Defense",
    "summary": "Burnham looks back at the history of 'elite theories' and their various scholars, purporting that each of them were Machiavellian in their own way."
}
export default json;