const json = {
    "id": "macintyre_after-virtue",
    "title": "After Virtue",
    "author": "Alasdair MacIntyre",
    "section": "L-INTG",
    "subsection": false,
    "pdf": "bafykbzacecbab4lbg36qtnmk7lo3we36bhmenvp3ro3ttm2stu5kjzdc6gbdw",
    "epub": "bafykbzaceatqqw7c2xqt7tfawhyzvuy7tjar5lioxal7g37yoopftrpk5vreu",
    "length": 1,
    "difficulty": 1,
    "genre": "Ethics",
    "type": "Critique",
    "summary": "MacIntyre tangles with the Enlightenment's inability to grapple with moral issues, and the various ways in which it has affected modern society."
}
export default json;