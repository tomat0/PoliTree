const json = {
    "id": "friedman_program",
    "title": "A Program for Monetary Stability",
    "author": "Milton Friedman",
    "section": "B-NEOL",
    "subsection": false,
    "pdf": "bafykbzaced4kftbtwprhxownwslow3qrctcbyzckdwbnbnj6ehgenghw6cgic",
    "epub": "bafk2bzaceblgp2yhnn6pakxxg2sml45mxvzrcogt27ywfbn5to4f576bsrzqq",
    "length": 1,
    "difficulty": 3,
    "genre": "Economics",
    "type": "Theory",
    "summary": "Friedman discusses the importance of the money supply on an economy's health, and steps a government can take to effectively manage it."
}
export default json;