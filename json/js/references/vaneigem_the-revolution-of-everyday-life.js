const json = {
    "id": "vaneigem_the-revolution-of-everyday-life",
    "title": "The Revolution of Everyday Life",
    "author": "by Raoul Vaneigem",
    "section": "B-PLFT",
    "subsection": false,
    "pdf": "bafykbzacecboaznnbe4ghqwcbviqasfqqhwc5sbs6xvcohvfl3oeyokv67hvk",
    "epub": "bafk2bzacednrqxfq4war3s3os4qsf2jkwyp2tofhzq4jsq5sppghdka6zsq5c",
    "length": 1,
    "difficulty": 1.5,
    "genre": "Politics",
    "type": "Tactics",
    "summary": "Vaneigem stresses a politics centered around the individual and the idea of 'everyday life' in order to escape the chains of society."
}
export default json;