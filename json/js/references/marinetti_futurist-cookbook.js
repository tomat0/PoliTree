const json = {
    "id": "marinetti_futurist-cookbook",
    "title": "The Futurist Cookbook",
    "author": "Filippo Tommaso Marinetti",
    "section": "B-FUTR",
    "subsection": false,
    "pdf": "",
    "epub": "bafykbzaceacoxlv4xfosz63fkwgl5q4mvr4tflkb74eoikudilt5nrrwdeuxs",
    "length": 2.5,
    "difficulty": 1,
    "genre": "History",
    "type": "Perspective",
    "summary": "A relic of early 20th century Futurism, Marinetti attempts to take something as seemingly mundane as cooking and utilize it as a platform to break free of society's reactionary conventions."
}
export default json;