const json = {
    "id": "ellul_technological-society",
    "title": "The Technological Society",
    "author": "Jacques Ellul",
    "section": "B-LUDD",
    "subsection": false,
    "pdf": "bafykbzacecyp3334t5kh552tpluoax4fq53iftekvr2uxz7k2i5ud5su56okg",
    "epub": "bafykbzacedcifod54xduoaotrcwlc4qdsp6fvyvco4wkjvhe3mvy2nlw22f6o",
    "length": 3,
    "difficulty": 2,
    "genre": "Technology",
    "type": "Theory",
    "summary": "Ellul discusses the relationship between machinery and broader society, outlining technology as the manifestation of technique, a force that seeks to rationalize everything, human and non-human."
}
export default json;