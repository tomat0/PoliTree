const json = {
    "id": "alinsky_rules-for-radicals",
    "title": "Rules for Radicals: A Pragmatic Primer for Realistic Radicals",
    "author": "Saul Alinsky",
    "section": "L-POPL",
    "subsection": false,
    "pdf": "bafykbzaceaqxorcl22tczfwe452ynnrfaqamp2yarxln22zxag6ym6lmqipkc",
    "epub": "bafk2bzacec464mgxpqnol7myfes2navxy2cr4z7vc7iqkijpt7ez5ns2wxr4u",
    "length": 1.5,
    "difficulty": 0.5,
    "genre": "Politics",
    "type": "Tactics",
    "summary": "Alinsky creates a rather thorough guide to grassroots movements, both highlighting its importance in wielding political power and also outlining the components essential to creating one."
}
export default json;