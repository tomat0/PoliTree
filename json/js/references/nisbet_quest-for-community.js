const json = {
    "id": "nisbet_quest-for-community",
    "title": "The Quest For Community",
    "author": "Robert Nisbet",
    "section": "B-PALO",
    "subsection": false,
    "pdf": "bafykbzacecspvn5t45vnc5a4axhfhpyvxks6cqmrt3xsga2aao55a5n4lqlj6",
    "epub": "bafykbzacebnl4mwlqrdopcrcp7azaemzatjbht7kktk3axdtpsld6mxzt46ku",
    "length": 2.5,
    "difficulty": 1,
    "genre": "Politics",
    "type": "Critique",
    "summary": "Nisbet takes the stance that the destruction of traditional familial bonds has inadverdently led to a decline in liberty: the individual supplants the local, giving room for the state to dictate new social relations."
}
export default json;