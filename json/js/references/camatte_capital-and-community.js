const json = {
    "id": "camatte_capital-and-community",
    "title": "Capital and Community",
    "author": "Jacques Camatte",
    "section": "B-ULFT",
    "subsection": false,
    "pdf": "",
    "epub": "",
    "length": 1.5,
    "difficulty": 2.5,
    "genre": "Economics",
    "type": "Critique",
    "summary": "Against many orthodox Marxists, Camatte re-centers the focus of Marx's critique on the commodity and its accompanying social relations."
}
export default json;