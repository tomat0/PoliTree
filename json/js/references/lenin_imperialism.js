const json = {
    "id": "lenin_imperialism",
    "title": "Imperialism, The Highest Stage of Capitalism",
    "author": "Vladmir Lenin",
    "section": "B-LNAT",
    "subsection": false,
    "pdf": "bafykbzacec7hzztqh4ocdcatsnd5a5g2ocl24sl33vxctoa2klhwzn2br2uiw",
    "epub": "bafk2bzacec2bkmcnz4svzaaxrkqkeox77ucsfwiwt63mo6ohdoq7w35k7ngwq",
    "length": 1,
    "difficulty": 1.5,
    "genre": "Economics",
    "type": "Critique",
    "summary": "In this book, Lenin hones in on the economic nature of imperialism, and the crucial role an empire's international relations can play within capitalism."
}
export default json;