const json = {
    "id": "bastiat_the-law",
    "title": "The Law",
    "author": "Fr\u00e9d\u00e9ric Bastiat",
    "section": "L-LIBT",
    "subsection": false,
    "pdf": "bafykbzacealgpnxyjms2zr53bckfaenbxwno2nt2rgf563tbvukuiwtehji5w",
    "epub": "",
    "length": 1,
    "difficulty": 0.5,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Bastiat writes critically regarding the nature of law, its core purpose, and the dangers and injustices associated with the abuse of it."
}
export default json;