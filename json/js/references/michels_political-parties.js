const json = {
    "id": "michels_political-parties",
    "title": "Political Parties",
    "author": "Robert Michels",
    "section": "B-ELIT",
    "subsection": false,
    "pdf": "bafykbzacednhp3vhgop4bwtdh52hl7lyzddgfqmcrfexccpqtq4gys3a4ivdg",
    "epub": "bafykbzacebygszccvzrz7k6kdi4lp34uq3tprl63ojgxj5naxc2rw66th55k4",
    "length": 2,
    "difficulty": 2.5,
    "genre": "Politics",
    "type": "Critique",
    "summary": "Michels notes that all political bodies inherently gravitate towards an uneven distribution of power; one necessary to direct the masses."
}
export default json;