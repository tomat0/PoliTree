const json = {
    "id": "ness_worker-organization",
    "title": "New Forms of Worker Organization: The Syndicalist and Autonomist Restoration of Class-Struggle Unionism",
    "author": "Immanuel Ness",
    "section": "B-SYND",
    "subsection": false,
    "pdf": "",
    "epub": "bafykbzaceaetnecyvxut76owanh7rjzrttko27us6e65lvf3t5qvcwtlis7m6",
    "length": 3,
    "difficulty": 2.5,
    "genre": "Politics",
    "type": "Tactics",
    "summary": "Ness discusses the decline of traditional labor unions in recent decades, why they failed, and how workers around the world have re-invented their forms of organization to adapt."
}
export default json;