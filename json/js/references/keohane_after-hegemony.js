const json = {
    "id": "keohane_after-hegemony",
    "title": "After Hegemony",
    "author": "Robert Keohane",
    "section": "B-COSM",
    "subsection": false,
    "pdf": "bafykbzacecptv4jvj4yy5zcofpxhmyag2hxi6vbbqjnozyel3lzfssqggk23m",
    "epub": "",
    "length": 2,
    "difficulty": 2,
    "genre": "International Relations",
    "type": "Critique",
    "summary": "Keohane criticizes the tendency of many analysts to be skeptical towards international cooperation. He argues that building strong international institutions can go a long way towards mitigating conflict, and that America does not need to act as the world policeman."
}

export default json;