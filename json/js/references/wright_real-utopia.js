const json = {
    "id": "wright_real-utopia",
    "title": "Envisioning Real Utopias",
    "author": "Erik Olin Wright",
    "section": "B-UTOP",
    "subsection": false,
    "pdf": "",
    "epub": "",
    "length": 2,
    "difficulty": 1.5,
    "genre": "Politics",
    "type": "Tactics",
    "summary": "This book outlines a form of prefigurative politics in which Wright answers the question: how can we begin building a feasible alternative to capitalism?"
}
export default json;