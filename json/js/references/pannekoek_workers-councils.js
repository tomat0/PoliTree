const json = {
    "id": "pannekoek_workers-councils",
    "title": "Workers' Councils",
    "author": "Antonie Pannekoek",
    "section": "B-LCOM",
    "subsection": true,
    "pdf": "bafykbzacebd7z36chv3ckli73u72jg253gropjpifwmv3i72ylkonjonrhsde",
    "epub": "bafykbzacecjbvcyi3qlbyqmlf6z2rpkjy75vfttq2cbax7vujyo2ayqblmxm4",
    "length": 0,
    "difficulty": 1.5,
    "genre": "Economics",
    "type": "Theory",
    "summary": "Pannekoek, disillusioned by the insufficiency of political parties and unions, develops a new theory of organization through workers' councils."
}
export default json;