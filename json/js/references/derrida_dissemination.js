const json = {
    "id": "derrida_dissemination",
    "title": "Dissemination",
    "author": "Jacques Derrida",
    "section": "B-POMO",
    "subsection": false,
    "pdf": "bafykbzacebqlbdkmh6iwigff4bfkc4uecowasxkwe47plef25r7ijvpslwsjs",
    "epub": "bafykbzacebfmzbyuedkb6yfwk2qlyimgkrekb3ug7wz6gris7rzflk5hkfbpw",
    "length": 3,
    "difficulty": 4,
    "genre": "Linguistics",
    "type": "Critique",
    "summary": "Derrida performs a structural critique by analyzing the nature of opposites through the process of linguistic deconstruction."
}
export default json;