const json = {
    "id": "marx_capital_1",
    "title": "Capital: A Critique of Political Economy (Volume I)",
    "author": "Karl Marx",
    "section": "L-COMM",
    "subsection": false,
    "pdf": "bafykbzacea6h42v2hoomh5kwovwghhlgr57gwdu6pfukd3jrqc6pqi34kt5rs",
    "epub": "bafykbzacead6vwklea6fj32m5gyu4x2thceqetrrwrgev4o7hbcjmqe3ysd22",
    "length": 3.5,
    "difficulty": 2.5,
    "genre": "Economics",
    "type": "Seminal",
    "summary": "Marx performs a full dissection of the logic underlying the capitalist economy; he re-interprets fundamental economic concepts such as value, commodity, labor, and property."
}
export default json;