const json = {
    "id": "faye_archeofuturism",
    "title": "Archeofuturism: European Visions of the Post-catastrophic Age",
    "author": "Guillaume Faye",
    "section": "B-IDNT",
    "subsection": false,
    "pdf": "",
    "epub": "bafykbzacebsmilyppa24h5lc7mzjc2grfb3stp6mtmc4ch6nig4hvjkhpefuu",
    "length": 2,
    "difficulty": 0.5,
    "genre": "Politics",
    "type": "Praxis",
    "summary": "Faye fuses the reactionary and the hypermodern elements of the right, creating a theoretical groundwork for what would later become the alt-right."
}
export default json;