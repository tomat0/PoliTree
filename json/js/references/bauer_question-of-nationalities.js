const json = {
    "id": "bauer_question-of-nationalities",
    "title": "The Question of Nationalities and Social Democracy",
    "author": "Otto Bauer",
    "section": "B-LNAT",
    "subsection": false,
    "pdf": "bafykbzaceamujz2bs6ljm52iwu5y5qzbsg7ab6y3mn2frrwjlhj3qtm7jx7zm",
    "epub": "",
    "length": 1.5,
    "difficulty": 1,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Bauer attempts one of the earliest reconciliations of Marxian socialism and nationalism, investigating the character of a nation and theorizing a system of national autonomy."
}
export default json;