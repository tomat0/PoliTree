const json = {
    "id": "fisher_capitalist-realism",
    "title": "Capitalist Realism: Is there no alternative?",
    "author": "Mark Fisher",
    "section": "B-UTOP",
    "subsection": false,
    "pdf": "bafykbzacecnpkveknsvrma5uboz6chx6tdazaaqqaqqndxideieeqizbyxgtk",
    "epub": "bafykbzacecgoo72qxbultwc3vo7n56gr4lmm4codkr5mgv5mgisit2diaa3ss",
    "length": 0.5,
    "difficulty": 1.5,
    "genre": "Politics",
    "type": "Critique",
    "summary": "Fisher argues that the seeming inevitablity of capitalism is an illusion maintained by the public imagination being limited by the messages reinforced in the media."
}
export default json;