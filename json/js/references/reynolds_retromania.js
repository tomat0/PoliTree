const json = {
    "id": "reynolds_retromania",
    "title": "Retromania: Pop Culture's Addiction to Its Own Past",
    "author": "Simon Reynolds",
    "section": "B-FUTR",
    "subsection": false,
    "pdf": "bafykbzaceb37mnks6hov4avfqrxjsrbswxinpyx5ymmfzqbl2xvywfk7wxx2m",
    "epub": "bafykbzacedspc2rkd3vd3yyj6g6idozwg2pdpxrp3pnnw3b4cwuouuyunt4zu",
    "length": 3,
    "difficulty": 0.5,
    "genre": "History",
    "type": "Critique",
    "summary": "Reynolds discusses pastiche's dominance over postmodern media, and how it has slowly replaced our future with nostalgia and retrospect."
}
export default json;