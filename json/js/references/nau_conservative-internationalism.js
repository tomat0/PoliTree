const json = {
    "id": "nau_conservative-internationalism",
    "title": "Conservative Internationalism",
    "author": "Henry Nau",
    "section": "B-IDEA",
    "subsection": true,
    "pdf": "bafykbzacebiz5uvui3ve6vydmolutbefeg5i7ob5fvu7pql2wahjfdi5xxcao",
    "epub": "",
    "length": 2.5,
    "difficulty": 1,
    "genre": "History",
    "type": "Defense",
    "summary": "Analyzing various presidencies, Nau is able to outline a form of internationalism that still remains relevant to this day: conservative internationalism."
}
export default json;