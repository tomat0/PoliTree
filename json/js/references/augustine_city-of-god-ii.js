const json = {
    "id": "augustine_city-of-god-ii",
    "title": "The City of God (Volume II)",
    "author": "Saint Augustine of Hippo",
    "section": "L-INTG",
    "subsection": false,
    "pdf": "bafykbzacecbox6h2ju54roc3zp3i25ysvibgi6dxwzix43qgqhcuidbukofts",
    "epub": "",
    "length": 3,
    "difficulty": 3,
    "genre": "Theology",
    "type": "Perspective",
    "summary": "Augustine discusses the origins of evil and its place in history; painting the conflicts of the world as a front of the much larger battle between the forces of good and evil."
}
export default json;