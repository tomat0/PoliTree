const json = {
    "id": "cole_guild-socialism",
    "title": "Guild Socialism: Restated",
    "author": "G.D.H. Cole",
    "section": "B-DSOC",
    "subsection": false,
    "pdf": "bafykbzaceajry3dsmy47p4vh3dkizu7kky2pbhflfupd2fzkxm3oykrt4q6a2",
    "epub": "",
    "length": 1.5,
    "difficulty": 1.5,
    "genre": "Politics",
    "type": "Tactics",
    "summary": "Cole envisions 'guild socialism', a strategy towards building up worker-democracy, and how economic reforms can assist in its development."
}
export default json;