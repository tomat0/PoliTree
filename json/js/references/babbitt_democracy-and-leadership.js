const json = {
    "id": "babbitt_democracy-and-leadership",
    "title": "Democracy and Leadership",
    "author": "Irving Babbit",
    "section": "L-INTG",
    "subsection": false,
    "pdf": "bafykbzacea3bf4q44r6grfges6m3r5v4c6ibhmkpunyfeibgqg4mkmiliqqau",
    "epub": "",
    "length": 2.5,
    "difficulty": 0.5,
    "genre": "Politics",
    "type": "Critique",
    "summary": "Babbitt defends the necessity of a transcendent moral standard to base society upon, arguing that no amount of democratic goodwill is capable of compensating for a lack of integrity."
}
export default json;