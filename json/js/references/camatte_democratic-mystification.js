const json = {
    "id": "camatte_democratic-mystification",
    "title": "The Democratic Mystification",
    "author": "Jacques Camatte",
    "section": "B-LCOM",
    "subsection": true,
    "pdf": "",
    "epub": "",
    "length": 0.5,
    "difficulty": 1.5,
    "genre": "Politics",
    "type": "Critique",
    "summary": "Camatte levels a critique against the ideal of democracy, breaking with other communists who advocated for the furthering of proletarian democracy."
}
export default json;