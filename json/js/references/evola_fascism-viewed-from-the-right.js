const json = {
    "id": "evola_fascism-viewed-from-the-right",
    "title": "Fascism Viewed From The Right",
    "author": "Julius Evola",
    "section": "B-FASC",
    "subsection": true,
    "pdf": "",
    "epub": "",
    "length": 1,
    "difficulty": 2.5,
    "genre": "Politics",
    "type": "Critique",
    "summary": "Self-proclaimed 'superfascist' Julius Evola critiques Italian Fascism and its issues, while still defending the core principles of Fascism itself; instead he calls for fascists to learn from their mistakes, abandon populist sentiments, and rebuild their movement."
}
export default json;