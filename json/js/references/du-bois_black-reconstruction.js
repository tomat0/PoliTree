const json = {
    "id": "du-bois_black-reconstruction",
    "title": "Black Reconstruction in America: 1860-1880",
    "author": "W. E. B. Du Bois",
    "section": "L-PROG",
    "subsection": false,
    "pdf": "bafykbzacebfipa5gck7ps4e6keyosvoym3hfzrwc4frvi5wkuray2dijlf6sc",
    "epub": "bafykbzacea5f74q34yl5pgbgwo4lt2dxo22lo3n7tuucuhzzgvwsm6fogn4gs",
    "length": 4.5,
    "difficulty": 1,
    "genre": "History",
    "type": "Perspective",
    "summary": "Du Bois offers a progressive interpetation of Reconstruction-era history, challenging common notions and highlighting the need for the Southern working class to unite, regardless of race."
}
export default json;