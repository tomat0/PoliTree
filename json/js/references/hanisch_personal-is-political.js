const json = {
    "id": "hanisch_personal-is-political",
    "title": "The Personal is Political",
    "author": "Carol Hanisch",
    "section": "B-LBMV",
    "subsection": false,
    "pdf": "",
    "epub": "",
    "length": 0.5,
    "difficulty": 0.5,
    "genre": "Politics",
    "type": "Tactics",
    "summary": "Hugely influential to the social movements of the 1960s, Hanisch argues that there is no barrier between 'personal' and 'political' issues, calling for the need of all women to organize."
}
export default json;