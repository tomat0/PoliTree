const json = {
    "id": "kagan_world-america-made",
    "title": "The World America Made",
    "author": "Robert Kagan",
    "section": "B-IDEA",
    "subsection": false,
    "pdf": "bafykbzacecxav5e2sjvwgl3pqbbeqympkfs4gtpfvvr6lnkffh6v5jx77g7ja",
    "epub": "bafykbzaceacoibyjdsnsmhvowfmqfmgd6lvmi23dxbqed6pkcb77nziadymoi",
    "length": 0.5,
    "difficulty": 1,
    "genre": "History",
    "type": "Defense",
    "summary": "Kagan demonstrates the various ways in the United States' hegemony has improved the world, and the need for its preservation."
}
export default json;