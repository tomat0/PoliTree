const json = {
    "id": "nozick_anarchy-state-utopia",
    "title": "Anarchy, State, and Utopia",
    "author": "Robert Nozick",
    "section": "B-MINR",
    "subsection": false,
    "pdf": "bafykbzacea2xl6w5zrij7uqexjqk6ctzk36y7ucuwygcg6cfrrdfuqwdjct66",
    "epub": "",
    "length": 2.5,
    "difficulty": 1.5,
    "genre": "Politics",
    "type": "Defense",
    "summary": "Providing a fresh statement of the original social-contract theory, Nozick defends the idea of small-government against attacks from both left-wing liberals and right-wing anarchists."
}
export default json;