const json = {
    "id": "black_abolition-of-work",
    "title": "The Abolition of Work",
    "author": "Bob Black",
    "section": "B-PLFT",
    "subsection": false,
    "pdf": "bafykbzacea6r4kerpfi2npxi3aazkfaxjum5odgfxm7yobghmurraoo2x5jqi",
    "epub": "",
    "length": 0,
    "difficulty": 0.5,
    "genre": "Politics",
    "type": "Critique",
    "summary": "Black reinterprets the 'work/play' distinction, defending not just the productivity of play but also its necessity in escaping a commodity-centered society."
}
export default json;