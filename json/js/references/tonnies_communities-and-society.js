const json = {
    "id": "tonnies_communities-and-society",
    "title": "Communities and Civil Society",
    "author": "Ferdinand T\u00f6nnies",
    "section": "L-NATN",
    "subsection": false,
    "pdf": "bafykbzaceaotojwuttwhun3y4ev375euxiy4ghseo2yyujbbh3eaoz7lahybu",
    "epub": "",
    "length": 2,
    "difficulty": 1,
    "genre": "Politics",
    "type": "Critique",
    "summary": "T\u00f6nnies discusses the nature of community, the contrasts between traditional and industrial community, and modernity's part in the destruction of the familial bonds of old."
}
export default json;