const json = {
    "id": "mises_bureaucracy",
    "title": "Bureaucracy",
    "author": "Ludwig von Mises",
    "section": "B-MINR",
    "subsection": false,
    "pdf": "bafykbzaceahvvf67k2imqwr2wske3c4bpwrxe7gzucnxxvcvciikaytji75cw",
    "epub": "",
    "length": 1,
    "difficulty": 1.5,
    "genre": "Economics",
    "type": "Critique",
    "summary": "Mises argues that a functional market rests on the principle of private ownership, and that bureaucratic administration of the economy can only lead to disaster."
}
export default json;