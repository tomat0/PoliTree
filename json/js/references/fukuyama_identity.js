const json = {
    "id": "fukuyama_identity",
    "title": "Identity: The Demand for Dignity and the Politics of Resentment",
    "author": "Francis Fukuyama",
    "section": "B-LCON",
    "subsection": false,
    "pdf": "bafykbzacectzgxrofdqa5rc3xgrpvmvmtghmup7rcskaekmxkrrkat6mw37wg",
    "epub": "bafykbzacebvggllnk6r6fbjzock4r7z4kmyhgnegqisajdsto4sdtgmk6s3mw",
    "length": 1.5,
    "difficulty": 1.5,
    "genre": "Politics",
    "type": "Critique",
    "summary": "Fukuyama challenges the rise of identity politics on both the left and right, arguing that the rising rejection of liberal universalism is corrosive to the very foundations of democracy itself."
}

export default json;