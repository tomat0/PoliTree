const json = {
    "id": "hayek_road-to-serfdom",
    "title": "The Road to Serfdom",
    "author": "Friedrich Hayek",
    "section": "L-LIBT",
    "subsection": false,
    "pdf": "bafykbzacedoizp4pzszqu3vj7b3j34ez7zpzxzr2q56z3lcxn56enhgavi4sg",
    "epub": "",
    "length": 2,
    "difficulty": 1,
    "genre": "Economics",
    "type": "Critique",
    "summary": "Hayek makes a case for the inseparable nature of economic and political freedom, warning that economic centralization can only lead to tyranny."
}
export default json;