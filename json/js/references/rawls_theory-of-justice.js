const json = {
    "id": "rawls_theory-of-justice",
    "title": "A Theory of Justice",
    "author": "John Rawls",
    "section": "L-PROG",
    "subsection": false,
    "pdf": "bafykbzacebajozlwacqj7ru7naetjtp4ucz2sasharaswfqz62qbq3deudg3u",
    "epub": "bafykbzacebfxccgygezq2mwz25hpj4edgoq5uf4taef6xvomz3ugr22yrk6va",
    "length": 3.5,
    "difficulty": 2.5,
    "genre": "Ethics",
    "type": "Theory",
    "summary": "Rawls re-interprets the social contract to reconcile freedom and equality, developing a concept of positive rights which takes into account distributive injustice."
}
export default json;