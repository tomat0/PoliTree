const json = {
    "id": "mises_human-action",
    "title": "Human Action",
    "author": "Ludwig von Mises",
    "section": "B-ANCP",
    "subsection": false,
    "pdf": "bafykbzacebaoe54uyshur7z4ligcnwvmc5uekb2qusa5pvq3hnuxzhray7axg",
    "epub": "",
    "length": 5,
    "difficulty": 2.5,
    "genre": "Economics",
    "type": "Theory",
    "summary": "Mises develops an alternative framework of economics centered around individual action and decision-making, as opposed to the empirical approach pursued by more mainstream currents."
}
export default json;