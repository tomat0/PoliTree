const json = {
    "id": "veblen_leisure-class",
    "title": "Theory of the Leisure Class",
    "author": "Thorstein Veblen",
    "section": "B-MERT",
    "subsection": false,
    "pdf": "bafykbzacebjg7utesvijb26ue4nb4ladtivbmsyaq335lroz3v62qnmwuyeke",
    "epub": "bafykbzacecwrozto2lpavmraowlqzhfpmu2glvs6hicvy7zdedtlfcnxggx4m",
    "length": 2,
    "difficulty": 1.5,
    "genre": "Economics",
    "type": "Critique",
    "summary": "Veblen criticizes society's tendency to encourage unproductive leisure among its upper classes, as opposed to productivity and merit."
}
export default json;