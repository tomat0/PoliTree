const json = {
    "id": "badiou_metapolitics",
    "title": "Metapolitics",
    "author": "Alan Badiou",
    "section": "B-IDNT",
    "subsection": false,
    "pdf": "bafykbzaceaw3ej4lbmsixc22wgogtabjvbo2w4balkxb2fnqw4xhuqvhmzukq",
    "epub": "",
    "length": 1,
    "difficulty": 2,
    "genre": "Politics",
    "type": "Theory",
    "summary": ""
}
export default json;