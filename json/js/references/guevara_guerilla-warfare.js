const json = {
    "id": "guevara_guerilla-warfare",
    "title": "Guerilla Warfare",
    "author": "Ernesto Guevara",
    "section": "B-LNAT",
    "subsection": false,
    "pdf": "",
    "epub": "",
    "length": 0.5,
    "difficulty": 0.5,
    "genre": "Politics",
    "type": "Tactics",
    "summary": "Guevara's intent with this book was to provide a manual for insurrection; he posits guerilla warfare as a keystone of anti-imperialist resistance and provides instructions on how to wage it."
}
export default json;