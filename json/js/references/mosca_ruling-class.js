const json = {
    "id": "mosca_ruling-class",
    "title": "The Ruling Class",
    "author": "Gaetano Mosca",
    "section": "B-ELIT",
    "subsection": false,
    "pdf": "",
    "epub": "bafykbzacebvaz5gae747yjnsrmh743jwr7eahb5uturrtbtvfixqbzrnitifi",
    "length": 2.5,
    "difficulty": 1,
    "genre": "Politics",
    "type": "Seminal",
    "summary": "Mosca notes how in democracies, power isn't necessarily held by the masses, but more accurately held by a politically active minority."
}
export default json;