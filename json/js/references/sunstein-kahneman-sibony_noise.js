const json = {
    "id": "sunstein-kahneman-sibony_noise",
    "title": "Noise: A Flaw in Human Judgment",
    "author": "Daniel Kahneman, Olivier Sibony and Cass Sunstein",
    "section": "B-RCNT",
    "subsection": false,
    "pdf": "",
    "epub": "bafykbzaceb7dn7mrosjejm7cnwxk2dprkjgpeypvrigzckilyjf2fbz6kc72u",
    "length": 3.5,
    "difficulty": 3.5,
    "genre": "Psychology",
    "type": "Theory",
    "summary": "The authors discuss all the irrational aspects in human decision-making, and how many democracies fail to account for this noise, creating worse judgements and worse outcomes."
}

export default json;