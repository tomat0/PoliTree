const json = {
    "id": "plato_republic",
    "title": "Republic",
    "author": "Plato",
    "section": "L-ARIS",
    "subsection": false,
    "pdf": "bafykbzacea7ypnyskysnhro3dc3ion6jcfcfv5qoryye6hn4vf3gwbrjgvtvw",
    "epub": "bafykbzaceajar5ldvxjllh343nrj7i6kxzuyd33mmnzxjb7pgsvrwbs7eqxnw",
    "length": 2.5,
    "difficulty": 2,
    "genre": "Politics",
    "type": "Seminal",
    "summary": "Plato reflects on various forms of governance, and relates them to each other; he theorizes a new, ideal form of governance known as 'aristocracy', rule of the best."
}
export default json;