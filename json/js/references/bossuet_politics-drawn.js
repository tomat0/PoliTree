const json = {
    "id": "bossuet_politics-drawn",
    "title": "Politics Drawn From The Very Words of Holy Scripture",
    "author": "Jacques-Benigne Bossuet",
    "section": "B-THEO",
    "subsection": false,
    "pdf": "bafykbzaceasjmlegagu3qzb57qgja22fmjhouc3mxlir4zdhvcv6ipj4kxt2s",
    "epub": "",
    "length": 2,
    "difficulty": 1.5,
    "genre": "Politics",
    "type": "Defense",
    "summary": "Bousett draws from Biblical sources in order to defend the absolute authority of a king as one ordained by God and necessary for the preservation of Christianity."
}
export default json;