const json = {
    "id": "schmitt_concept-of-the-political",
    "title": "The Concept of The Political",
    "author": "Carl Schmitt",
    "section": "L-NATN",
    "subsection": false,
    "pdf": "bafykbzaceazznboe7re2g3mgrt3gyyynfufz67j7bibvvecqm5liv7vyiwylu",
    "epub": "bafykbzaceaskb4zblss5a4hjwr3pdsn7przg4evsif5exceqni32btk3rhdsm",
    "length": 1,
    "difficulty": 2,
    "genre": "Politics",
    "type": "Critique",
    "summary": "Schmitt attacks the conciliatory view of politics, positing instead that the goal of politics is for groups to settle their differences through war and elimination."
}
export default json;