const json = {
    "id": "glossop_world-federation",
    "title": "World Federation? A Critical Analysis of Federal World Government",
    "author": "Ronald J. Glossop",
    "section": "B-COSM",
    "subsection": true,
    "pdf": "bafykbzaceb42jlpjp22iap7gcvz4ctn7fsetrnhnu5ycoloftp7raxgqigph4",
    "epub": "",
    "length": 1.5,
    "difficulty": 1,
    "genre": "International Relations",
    "type": "Blueprint",
    "summary": "Glossop gives a thorough primer on the on the topic of world federations: how it can solve modern problems, rebuttals to criticisms, what it is, and what forms it could take."
}

export default json;