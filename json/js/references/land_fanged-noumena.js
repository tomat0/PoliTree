const json = {
    "id": "land_fanged-noumena",
    "title": "Fanged Noumena",
    "author": "Nick Land",
    "section": "B-FUTR",
    "subsection": false,
    "pdf": "bafykbzacecev2awadb44jr74tw42c4ikamzomsqjjqykb6zn2f77ymq4vkq7g",
    "epub": "bafykbzacecd6lovjnrmq4dgmkhqgja4grq5g7ya5eax6fzqpy5buur2ixcifc",
    "length": 3,
    "difficulty": 5,
    "genre": "Fiction",
    "type": "Seminal",
    "summary": "A collection of Land's most influential works, this work contains a mix of futuristic hyperfiction and critiques of the philosophy that preceded him."
}
export default json;