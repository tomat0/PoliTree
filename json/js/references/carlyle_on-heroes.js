const json = {
    "id": "carlyle_on-heroes",
    "title": "On Heroes, Hero-Worship, and The Heroic in History",
    "author": "Thomas Carlyle",
    "section": "B-FASC",
    "subsection": false,
    "pdf": "bafykbzacebkgm2nlf6rb4rd43nmkfgt2ovl3rwk4mplpuxd4ynoecgh6xfzkw",
    "epub": "bafykbzacebilk3b4r4pbrxpkq73e4poh3lssmdeadxkdufgr2aeiw2h6w645e",
    "length": 2,
    "difficulty": 1,
    "genre": "History",
    "type": "Perspective",
    "summary": "A series of lectures by Carlyle in which he considers the various types of hero-figures and the important roles they play in serving as the backbone of a movement."
}
export default json;