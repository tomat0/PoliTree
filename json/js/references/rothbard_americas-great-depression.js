const json = {
    "id": "rothbard_americas-great-depression",
    "title": "America's Great Depression",
    "author": "Murray Rothbard",
    "section": "B-MINR",
    "subsection": false,
    "pdf": "bafykbzacecdzrubkjsg6nhzoj3tg2sp5aersxaev7ptt4a3gnffguqjscbmri",
    "epub": "",
    "length": 2,
    "difficulty": 1,
    "genre": "Economics",
    "type": "Critique",
    "summary": "Against the mainstream consensus, Rothbard argues that the Great Depression was actually caused by government intervention in the economy, not a lack of it."
}
export default json;