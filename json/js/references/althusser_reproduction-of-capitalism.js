const json = {
    "id": "althusser_reproduction-of-capitalism",
    "title": "On The Reproduction of Capitalism",
    "author": "Louis Althusser",
    "section": "B-NMRX",
    "subsection": true,
    "pdf": "bafykbzacecmp2ydocgytcy6bqbyl5y2fyh2xv54yf3stajo562fyqjbrhmhre",
    "epub": "bafykbzaceasatysb2ovv4cb2ibieye72e3ntjlc72lwbyntwnt67fukym7jf6",
    "length": 2,
    "difficulty": 2,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Following the 1968 unrest, Althusser develops a theory regarding the various ideological and social structures that serve to reinforce capitalism."
}
export default json;