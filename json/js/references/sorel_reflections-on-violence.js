const json = {
    "id": "sorel_reflections-on-violence",
    "title": "Reflections on Violence",
    "author": "Georges Sorel",
    "section": "B-FASC",
    "subsection": false,
    "pdf": "bafykbzacecsjsq3nx3hx4b6u5ekvznea2lv7ud4kazwgnrj3yw7dfjpaq7f3k",
    "epub": "bafykbzacediteulwfl3622wt43nq3bbumatyjy7d4uzpxydev2wd5zy6qdy2q",
    "length": 2.5,
    "difficulty": 2.5,
    "genre": "Politics",
    "type": "Defense",
    "summary": "Sorel discusses the revolutionary role of myth, the creative character of violence, and the ways in which revolutionary struggle is capable of taking old motifs and breathing new life into them."
}
export default json;