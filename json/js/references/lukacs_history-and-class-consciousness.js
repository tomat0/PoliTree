const json = {
    "id": "lukacs_history-and-class-consciousness",
    "title": "History and Class Consciousness",
    "author": "Gy\u00f6rgy Luk\u00e1cs",
    "section": "B-NMRX",
    "subsection": false,
    "pdf": "bafykbzacec7ku3fz73mvzzntktc6enewdx6tw5qhou3ptb3dee33bvhijpvz2",
    "epub": "bafykbzaceas6cflgahgkge52qsau3bzowmsxxn3zmcjekdknhmuzludj25mqm",
    "length": 3,
    "difficulty": 3,
    "genre": "Philosophy",
    "type": "Interpretation",
    "summary": "Lukacs returns the focus of Marxism back to philosophy, arguing that what stands at the center of Marxism is not any one static theory or prediction, but a method of analysis."
}
export default json;