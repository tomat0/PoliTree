const json = {
    "id": "taleb_antifragile",
    "title": "Antifragile: Things That Gain From Disorder",
    "author": "Nassim Nicholas Taleb",
    "section": "B-NEOR",
    "subsection": false,
    "pdf": "bafykbzacedtb6e3bzi22binnnwv6bwokuznbfu27juea45isihz3fhfdh5i6a",
    "epub": "bafykbzacecd4umo4wnlwepdfpujlnuqv26bkohvdzf4wts7qxojswhkmkxujm",
    "length": 3.5,
    "difficulty": 3,
    "genre": "Sociology",
    "type": "Theory",
    "summary": "Taleb explores the idea of 'antifragile' institutions, traditional forms of doing and organizing things which become stronger in times of crisis."
}

export default json;