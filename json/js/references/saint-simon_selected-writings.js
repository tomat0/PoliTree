const json = {
    "id": "saint-simon_selected-writings",
    "title": "Selected Writings on Science, Industry, and Social Organisation",
    "author": "Henri Saint-Simon",
    "section": "B-TCHN",
    "subsection": false,
    "pdf": "",
    "epub": "",
    "length": 2.5,
    "difficulty": 0,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Saint-Simon was a chief advocate of a society that rewards hard-working men and guided by science; through this collection of his writings, he expands on his vision."
}
export default json;