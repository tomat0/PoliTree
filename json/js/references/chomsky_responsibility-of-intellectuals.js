const json = {
    "id": "chomsky_responsibility-of-intellectuals",
    "title": "The Responsibility of Intellectuals",
    "author": "Noam Chomsky",
    "section": "L-NEWL",
    "subsection": false,
    "pdf": "",
    "epub": "bafykbzacecfjopuhklp75s43xpyioexbptujb3i5xfgaqeljd63h425x56xka",
    "length": 0.5,
    "difficulty": 0.5,
    "genre": "Politics",
    "type": "Critique",
    "summary": "Chomsky levels an attack against the intellectuals of his time for their justification of the Vietnam War, arguing that it is an intellectual's duty to investigate and expose lies that continue to uphold power."
}
export default json;