const json = {
    "id": "ikenberry_liberal-leviathan",
    "title": "Liberal Leviathan",
    "author": "G. John Ikenberry",
    "section": "B-IDEA",
    "subsection": true,
    "pdf": "bafykbzaced6tjrbcyrgv2k4golgw6qwfksatmxx4jbojgiruhzfuhfroiie6g",
    "epub": "",
    "length": 3,
    "difficulty": 1.5,
    "genre": "International Relations",
    "type": "Theory",
    "summary": "Ikenberry advocates for deep engagement in foreign countries in order to promote the development of democracy through multilateral institutions such as the United Nations, and eschews what it sees as the imperialist ambitions of conservative internationalism and the Iraq War."
}
export default json;