const json = {
    "id": "mauss_the-gift",
    "title": "The Gift: Forms and Functions of Exchange in Archaic Societies",
    "author": "Marcel Mauss",
    "section": "B-SANR",
    "subsection": false,
    "pdf": "bafykbzacedrbfnaqzdaotg5b3ncwjbaxkyby7sgrfgfbimvq6egstgmvl2hmc",
    "epub": "bafykbzacedai4fxb7q5pkg4hoecekob43q6z3qnykqmycsohkwskjjyszxamq",
    "length": 1.5,
    "difficulty": 2,
    "genre": "Sociology",
    "type": "Defense",
    "summary": "Mauss challenges the usual narratives about human nature through his observations how 'primitive societies' tend to be based upon a principle of mutual gift-giving as opposed to self-interest."
}
export default json;