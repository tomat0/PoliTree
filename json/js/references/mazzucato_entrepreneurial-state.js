const json = {
    "id": "mazzucato_entrepreneurial-state",
    "title": "The Entrepreneurial State",
    "author": "Mariana Mazzucato",
    "section": "B-SDEM",
    "subsection": false,
    "pdf": "bafykbzaceccwsxrzughm3pgyhzdgdzr7gtom4c6ctwgniwwwj7op6p7udiy72",
    "epub": "bafykbzacedrbbiq4xbd6ds2p6apkt5qm55ig4pukssapg3gkj7r2x6qwgyifk",
    "length": 2,
    "difficulty": 1,
    "genre": "Economics",
    "type": "Critique",
    "summary": "Mazzucato contests the claim that governments are less effective than companies at controlling industry, looking at how many Western innovations were the result of government innovations."
}
export default json;