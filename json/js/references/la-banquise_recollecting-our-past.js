const json = {
    "id": "la-banquise_recollecting-our-past",
    "title": "Recollecting our Past",
    "author": "La Banquise",
    "section": "B-ULFT",
    "subsection": false,
    "pdf": "",
    "epub": "",
    "length": 1,
    "difficulty": 1,
    "genre": "History",
    "type": "Critique",
    "summary": "This issue of <i>La Banquise</i> navigates the oft-confusing history of the 'ultra-left' and the milieu in which modern tendencies have developed. "
}
export default json;