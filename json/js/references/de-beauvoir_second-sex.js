const json = {
    "id": "de-beauvoir_second-sex",
    "title": "The Second Sex",
    "author": "Simone de Beauvoir",
    "section": "B-LBMV",
    "subsection": true,
    "pdf": "bafykbzaceanfykpneoqcpml4r3toio3sapnoygmzg4anzlex5yuugulu6bwxi",
    "epub": "bafykbzacedac4ihx6cq5hcd5iqlielopa4veuexkiyeyjw5wnv4uo4y4c77d4",
    "length": 5,
    "difficulty": 1,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Beauvoir looks throughout history to exactly what makes a woman, rejecting various essentialistic explanations, rather instead positing femininity as a socially constructed negation of masculinity."
}

export default json;