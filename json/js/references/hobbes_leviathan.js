const json = {
    "id": "hobbes_leviathan",
    "title": "Leviathan",
    "author": "Thomas Hobbes",
    "section": "L-ARIS",
    "subsection": false,
    "pdf": "bafykbzaceczr5h364aapfzledjvtdx25vzri2blovqzchpgx3gl4h6uuawveo",
    "epub": "bafykbzacebytdgskyvaztjmbjl3xrkwxybtlfau55kfpbvzecho25jfr7fidw",
    "length": 3,
    "difficulty": 1,
    "genre": "Politics",
    "type": "Defense",
    "summary": "Hobbes, against of many of his peers, makes a modernist case for an absolute government, claiming that it is necessary to prevent the natural state of chaos."
}
export default json;