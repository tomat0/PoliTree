const json = {
    "id": "dahl_polyarchy",
    "title": "Polyarchy: Participation and Opposition",
    "author": "Robert Dahl",
    "section": "B-PLUR",
    "subsection": false,
    "pdf": "bafykbzacedc6tmyqxugzhaadmsszckqvtr7h5d7lnfh64nr2sthz2vduf55hg",
    "epub": "",
    "length": 1,
    "difficulty": 2,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Dahl analyzes democracy, noting that the extent to which a society is democratic depends on the society's ability to encourage both competition and participation."
}
export default json;