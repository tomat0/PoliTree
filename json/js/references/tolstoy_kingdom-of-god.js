const json = {
    "id": "tolstoy_kingdom-of-god",
    "title": "The Kingdom of God Is Within You",
    "author": "Leo Tolstoy",
    "section": "B-PACF",
    "subsection": false,
    "pdf": "bafykbzaceaptascvjjni6qn6hl5v5dxpfgx4fphpkjlm6qzf5twken4f5kuj6",
    "epub": "",
    "length": 1,
    "difficulty": 1,
    "genre": "Theology",
    "type": "Critique",
    "summary": "Thoreau proposes a nonviolent form of action known as civil disobedience; this concept would soon be integrated into countless pacifist movements throughout history."
}
export default json;