const json = {
    "id": "rawls_political-liberalism",
    "title": "Political Liberalism",
    "author": "John Rawls",
    "section": "B-SYNC",
    "subsection": false,
    "pdf": "bafykbzacebyzbgef2if4ufhqq7qqbnctcem3c5fpqvn3ozqqsssnyzubpwb6i",
    "epub": "bafykbzacedo4ms6naisep3g6zymu4mxrcc4a5rghsxkj4kztgqbg6f7afucom",
    "length": 3,
    "difficulty": 1,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Rawls contemplates the issue of resolving political disputes, and in the process expands upon his idea of an overlapping consensus being the guiding principle of society."
}
export default json;