const json = {
    "id": "more_diachronic-self",
    "title": "The Diachronic Self: Identity, Continuity, Transformation",
    "author": "Max More",
    "section": "B-THUM",
    "subsection": false,
    "pdf": "bafykbzacecfebvzelqswkxxtbj3cuaw4yjrg4slfeikmpw3mdciarqaql7px4",
    "epub": "",
    "length": 2,
    "difficulty": 5,
    "genre": "Philosophy",
    "type": "Theory",
    "summary": "More challenges the common understanding of what constitutes identity, arguing that modifications to one's physical form does not destroy their ontological self."
}
export default json;