const json = {
    "id": "rocker_anarcho-syndicalism",
    "title": "Anarcho-Syndicalism: Theory and Practice",
    "author": "Rudolf Rocker",
    "section": "B-SYND",
    "subsection": false,
    "pdf": "bafykbzaceazurwcwamzdbmricmsfgt2lwm2n3is6plmo4sgsddudkhkk4463m",
    "epub": "",
    "length": 1,
    "difficulty": 1,
    "genre": "History",
    "type": "Theory",
    "summary": " Rocker gives an overview of the histories of the anarchist and workers' movements, creating a comprehensive outline of anarcho-syndicalism in the process."
}
export default json;