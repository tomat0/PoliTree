const json = {
    "id": "aquinas_treatise-on-law",
    "title": "Treatise on Law",
    "author": "Thomas Aquinas",
    "section": "L-INTG",
    "subsection": false,
    "pdf": "bafykbzacedhfkq4wzh52g6g7s575wqfvs57a4twawoqekx2qadymkohwkf4dg",
    "epub": "",
    "length": 1,
    "difficulty": 3,
    "genre": "Politics",
    "type": "Seminal",
    "summary": "Aquinas investigates the nature of law, looking to understand its origins and the justification for its existence, whether it be divinely or naturally inspired."
}

export default json;