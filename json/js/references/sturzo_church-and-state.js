const json = {
    "id": "sturzo_church-and-state",
    "title": "Church and State",
    "author": "Luigi Sturzo",
    "section": "B-PATR",
    "subsection": false,
    "pdf": "bafykbzaceaiszjaxyy6vs4e23c6x3von22llpr6ct5eivvvizrcgpjhhgfrm6",
    "epub": "",
    "length": 4,
    "difficulty": 3,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Sturzo reflects on the relationship between the church as a social institution and the state as a political institution, eventually concluding that the strengthening of the community must come from the fusion of the two."
}
export default json;