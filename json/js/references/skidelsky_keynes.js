const json = {
    "id": "skidelsky_keynes",
    "title": "Keynes: Return of the Master",
    "author": "Robert Skidelsky",
    "section": "B-SDEM",
    "subsection": false,
    "pdf": "bafykbzacebowi2iudpyfgepx2fjslcakjousfkifxs3mvei5bru3faimw6x2i",
    "epub": "bafykbzacea6satw2vs57vwxytu4kzfbj3lsvkl3atodd4w5holt2fsonh3chk",
    "length": 1,
    "difficulty": 0.5,
    "genre": "History",
    "type": "Interpretation",
    "summary": "Skidelsky reviews the history of the Keynesian revolution, its revisions in the wake of neoliberalism, and how modern economic crises have set the stage for its resurgence."
}
export default json;