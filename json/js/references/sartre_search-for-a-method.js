const json = {
    "id": "sartre_search-for-a-method",
    "title": "Search for a Method",
    "author": "Jean-Paul Sartre",
    "section": "B-NMRX",
    "subsection": true,
    "pdf": "bafykbzacec7hmfgoe3hcsqwu4iv5pzu3baawslzvragvsg2nly74334to6spq",
    "epub": "",
    "length": 1.5,
    "difficulty": 2,
    "genre": "Philosophy",
    "type": "Theory",
    "summary": "Sartre makes the case that Marxism and existentialism are complementary, suggesting that the a fusion of the two is needed to tackle the problems of modern society."
}
export default json;