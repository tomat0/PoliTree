const json = {
    "id": "bap_bronze-age-mindset",
    "title": "Bronze Age Mindset",
    "author": "Bronze Age Pervert",
    "section": "B-FASC",
    "subsection": false,
    "pdf": "",
    "epub": "bafk2bzaceckik3jrknqz36wgyuv6ze4zysnv6fcdrqcwzpplwfmznlvemuu66",
    "length": 1,
    "difficulty": 1,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Ironic yet provocative in its style, BAP lays out a manifesto decrying the modern ideals of equity and moralism in favor of a worldview which valorizes strength and domination."
}
export default json;