const json = {
    "id": "krugman_rethinking-international-trade",
    "title": "Rethinking International Trade",
    "author": "Paul Krugman",
    "section": "B-SLIB",
    "subsection": true,
    "pdf": "bafykbzacedlwizos3z4q6wjhi5wixpgz3wbmvagx46yktmbx76nkahhpdna2s",
    "epub": "",
    "length": 2,
    "difficulty": 3,
    "genre": "Economics",
    "type": "Theory",
    "summary": "Krugman recaps developments in theories of international trade, attempting to strike a balance between keeping markets open but also fair to domestic industry."
}

export default json;