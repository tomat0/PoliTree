const json = {
    "id": "heidegger_question-concerning-technology",
    "title": "The Question Concerning Technology and Other Essays",
    "author": "Martin Heidegger",
    "section": "L-ACCL",
    "subsection": false,
    "pdf": "bafykbzaceddtj326hb3anq7wdnkxr3jp3mxh4dnpozw2be5aj5jaogm3y7uyy",
    "epub": "",
    "length": 1.5,
    "difficulty": 3,
    "genre": "Technology",
    "type": "Critique",
    "summary": "Heidegger considers the existential relationship between man and technology, and the implications the development of technology has on mankind as a whole."
}
export default json;