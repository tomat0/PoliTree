const json = {
    "id": "thaler-sunstein_nudge",
    "title": "Nudge: Improving Decisions about Health, Wealth, and Happiness",
    "author": "Richard H. Thaler and Cass R. Sunstein",
    "section": "B-MINR",
    "subsection": true,
    "pdf": "bafykbzacedi74qet6m336nuchmhalmz5ytn3jxghe2t4oevztbsylathr6gfg",
    "epub": "bafykbzaceculylzbppwx2jzhkcrycazg7wy6sgxcudoeu5gmbx5ercivye3ym",
    "length": 2,
    "difficulty": 1,
    "genre": "Economics",
    "type": "Theory",
    "summary": "Thaler and Sunstein argue that you don't need a coercive government to ensure people make the choices that are best for society. Instead, they believe that we can use behavioral economics to design our systems in a way that 'nudges' people in the right direction while respecting their freedom."
}

export default json;