const json = {
    "id": "scott_seeing-like-a-state",
    "title": "Seeing Like A State",
    "author": "James C. Scott",
    "section": "B-LUDD",
    "subsection": false,
    "pdf": "bafykbzacebigtbgpa5xerkwixpzvkz4mwpv2jaxe5jxpmcnif4lkr7siy5a5o",
    "epub": "bafykbzaceascjydkvtubqdctpztxn6lhalojhdcbjpc6dreoaxxqxwgzrk4zs",
    "length": 3,
    "difficulty": 2.5,
    "genre": "Politics",
    "type": "Critique",
    "summary": "Scott challenges the idea of the administrative state and how top-down forms of planning and scientific standard only serve to exert control rather than actually solving problems."
}
export default json;