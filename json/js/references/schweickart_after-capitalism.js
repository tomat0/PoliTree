const json = {
    "id": "schweickart_after-capitalism",
    "title": "After Capitalism",
    "author": "David Schweickart",
    "section": "B-DSOC",
    "subsection": true,
    "pdf": "bafykbzacebrnbkvutly2fcgc535tb4zmplbf6enxalzeqpsb6xyr2hhylfrhc",
    "epub": "bafykbzacec7jy6w3hroikznsb5wsppzaolgcal7daopltovftkuj4w5xynaee",
    "length": 2,
    "difficulty": 1.5,
    "genre": "Economics",
    "type": "Defense",
    "summary": "Schweickart provides a justification for worker self-management, its logistics and improvements over capitalism, and how society can transition towards it."
}

export default json;