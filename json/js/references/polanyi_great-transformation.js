const json = {
    "id": "polanyi_great-transformation",
    "title": "The Great Transformation",
    "author": "Karl Polanyi",
    "section": "B-PATR",
    "subsection": false,
    "pdf": "bafykbzaceaxyito23chogufsazf2utqxfaz4zr3cec36qdszin6t5ncnqke3s",
    "epub": "bafykbzacebkyemu5qcqyr4nlrwgog3mj3ztqysi2j27ulexfqwb3qipydpe36",
    "length": 2,
    "difficulty": 1.5,
    "genre": "Economics",
    "type": "Theory",
    "summary": "Looking at free-market capitalism from a historical lens, Polanyi discusses how it has led to the breaking down of old communal bonds in favor of expanded trade."
}
export default json;