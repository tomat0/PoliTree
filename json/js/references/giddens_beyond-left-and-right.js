const json = {
    "id": "giddens_beyond-left-and-right",
    "title": "Beyond Left and Right: The Future of Radical Politics",
    "author": "Anthony Giddens",
    "section": "B-SYNC",
    "subsection": false,
    "pdf": "bafykbzacea4h4ugnffygrdisqzstnubi3sdc2z7s4oka6qbu7g2vlo5bvosla",
    "epub": "",
    "length": 1,
    "difficulty": 0.5,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Giddens re-evaluates our conceptions of left and right, coming to conclusions regarding violence, fundamentalism, and division from a stance he calls 'neither left nor right'."
}
export default json;