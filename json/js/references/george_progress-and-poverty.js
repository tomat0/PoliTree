const json = {
    "id": "george_progress-and-poverty",
    "title": "Progress and Poverty",
    "author": "Henry George",
    "section": "L-PROG",
    "subsection": false,
    "pdf": "bafykbzacecl54ncw6a2r6hks7lgxpty4yvxoabtfmqkuqqenyucr6ucr3ew3o",
    "epub": "bafykbzaceali7qhps2xs35zxexeqwkhw35kwixfelvxg7cagg5csbsjr3uple",
    "length": 1.5,
    "difficulty": 2,
    "genre": "Economics",
    "type": "Critique",
    "summary": "George attempts to answer the question of how society can progress yet sustain such severe inequality; he ties it back to the ownership of land and calls for a land tax as a solution."
}
export default json;