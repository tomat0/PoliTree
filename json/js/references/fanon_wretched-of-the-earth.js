const json = {
    "id": "fanon_wretched-of-the-earth",
    "title": "The Wretched of the Earth",
    "author": "Frantz Fanon",
    "section": "B-LBMV",
    "subsection": true,
    "pdf": "bafykbzacebcpezezuxoruebuyo6uidcqzckfisi46dtnuy2vgwoccxkaqccc6",
    "epub": "bafykbzaceblrnw7hejn5cktjsqwh4crgkdsc2xo4rdfbxrexo2io3s3suzbq4",
    "length": 1.5,
    "difficulty": 2,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Fanon deals with the more personal, less tangible effects of colonialism on a people: specifically its history, effects on the individual, and remedy."
}

export default json;