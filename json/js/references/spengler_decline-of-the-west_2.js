const json = {
    "id": "spengler_decline-of-the-west_2",
    "title": "Decline of The West (Volume II)",
    "author": "Oswald Spengler",
    "section": "B-IDNT",
    "subsection": false,
    "pdf": "bafykbzacebpxo2ycxhtqdql3zsotyhmbl5hkzvpvht352muf7oalyd3swotqo",
    "epub": "bafykbzacedqfiblolaz2lmlujldaedugm56cg3zhhbathzxs6czhlwf47asq2",
    "length": 4,
    "difficulty": 2,
    "genre": "History",
    "type": "Theory",
    "summary": "Spengler divides history into different ages, discussing the fate of modern man and developing an early theory of racial identity."
}
export default json;