const json = {
    "id": "laclau+mouffe_hegemony-and-strategy",
    "title": "Hegemony and Socialist Strategy",
    "author": "Ernest Laclau and Chantal Mouffe",
    "section": "L-NEWL",
    "subsection": false,
    "pdf": "bafykbzaceakl74tfkhv4yctmdb4xzjeaixdtz5uik4kqga2xhn74wkeba4s7g",
    "epub": "bafykbzaceb4phrpuxdx6geify2j7k7cqxneridzyiqj275lqwwwmlwt4ywjug",
    "length": 1,
    "difficulty": 3,
    "genre": "Politics",
    "type": "Defense",
    "summary": "Chantal and Mouffe criticize the economic focus of Marxist theory, reconstructing an analysis through the lens of hegemony."
}
export default json;