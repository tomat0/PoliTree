const json = {
    "id": "perlman_against-leviathan",
    "title": "Against His-tory, Against Leviathan",
    "author": "Fredy Perlman",
    "section": "B-PLFT",
    "subsection": false,
    "pdf": "bafykbzacedxshvxzbwbspuo6454k5x3qhmw4tzqktd5pbms7nl6o6hhgthca4",
    "epub": "",
    "length": 2,
    "difficulty": 1.5,
    "genre": "History",
    "type": "Critique",
    "summary": "Perlman retells history from the perspective of individuals and their resistance to civilizing forces, all tying into a larger critique of civilization itself."
}
export default json;