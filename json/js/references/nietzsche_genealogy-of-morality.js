const json = {
    "id": "nietzsche_genealogy-of-morality",
    "title": "The Genealogy of Morality",
    "author": "Friedrich Nietzsche",
    "section": "B-EGOI",
    "subsection": false,
    "pdf": "bafykbzacedtlzgt7wuwcoqjjr6ndzusvskmukqqbpgdqvdmymfq6vg5gbgnna",
    "epub": "bafykbzaceaqo7wn4s7yik6g3ckrsvh53766viuzq4m3lewvslejm4sldrhp5o",
    "length": 1,
    "difficulty": 1,
    "genre": "Philosophy",
    "type": "Critique",
    "summary": "Nietzsche examines, outlines, and criticizes the foundations of morality, viewing it as an impediment on the liberty of the individual."
}
export default json;