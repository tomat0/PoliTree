const json = {
    "id": "esping_three-worlds",
    "title": "The Three Worlds of Welfare Capitalism",
    "author": "G\u00f8sta Esping-Andersen",
    "section": "B-SDEM",
    "subsection": false,
    "pdf": "bafykbzaceahvmkiosicfiy45l54nztvjzg2ra72rw2z5j2yeuovdzr4g6k2f6",
    "epub": "",
    "length": 1.5,
    "difficulty": 2.5,
    "genre": "Economics",
    "type": "Theory",
    "summary": "Esping-Andersen breaks down the modern 'welfare state' into various types with each their own characteristics, and evaluates how they function differently in practice."
}
export default json;