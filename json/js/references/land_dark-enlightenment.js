const json = {
    "id": "land_dark-enlightenment",
    "title": "The Dark Enlightenment",
    "author": "Nick Land",
    "section": "B-NEOR",
    "subsection": false,
    "pdf": "",
    "epub": "bafykbzaceauup25owjqvh3jxzqfmvbtpkky4smlti4vomgnfcjaskwgct4tz6",
    "length": 0.5,
    "difficulty": 3,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Land considers the state of liberal democracy and its shortcomings, touching on key neo-reactionary topics such as historiography, conflict, and political authority."
}
export default json;