const json = {
    "id": "stirner_ego-and-its-own",
    "title": "The Ego and Its Own",
    "author": "Max Stirner",
    "section": "B-EGOI",
    "subsection": false,
    "pdf": "bafykbzaceckhtvpus24bxcpwryjiejaiopmy4hb4k5btjmb7fnfod26lagqfo",
    "epub": "bafykbzaceciszlsulelvfqfbcm2lons4bri4cqunkxmllubj6q7ypwfehsvuc",
    "length": 1.5,
    "difficulty": 1,
    "genre": "Philosophy",
    "type": "Theory",
    "summary": "Stirner launches an attack on the various ideological factions of his time, instead opting for an understanding that centers around the individual."
}
export default json;