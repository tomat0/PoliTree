const json = {
    "id": "trotsky_permanent-revolution",
    "title": "The Permanent Revolution (and Results & Prospects)",
    "author": "Leon Trotsky",
    "section": "B-VANG",
    "subsection": true,
    "pdf": "bafykbzacedxf2hzd7cdtotxtwxse6oq3fhokpx3mrfdxpotzezhxqiirte7mc",
    "epub": "bafk2bzacebnrma6hrll36iep4g7ijadet4nxbhy6bkplk33ekcoh2zv6g6wx2",
    "length": 2.5,
    "difficulty": 2,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Trotsky introduces his main contributions to Leninist thought, most notably the emphasis on the industrial proletariat and the internationalizing of socialist revolution."
}
export default json;