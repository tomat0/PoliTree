const json = {
    "id": "waring_taylorism-transformed",
    "title": "Taylorism Transformed: Scientific Management Theory Since 1945",
    "author": "Stephen P. Waring",
    "section": "B-TCHN",
    "subsection": false,
    "pdf": "",
    "epub": "bafykbzaceakut6fu6sm36sdfio3ubnsxrnikuoxalp46uuvcish3pzvlck7mm",
    "length": 4,
    "difficulty": 2,
    "genre": "History",
    "type": "Theory",
    "summary": "Waring reflects on the various scholars of management theory, highlighting the political nature of the corporation as an institution and bridging the gap between management theory and political theory."
}
export default json;