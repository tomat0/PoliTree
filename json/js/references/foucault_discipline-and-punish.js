const json = {
    "id": "foucault_discipline-and-punish",
    "title": "Discipline and Punish",
    "author": "Michel Foucault",
    "section": "B-POMO",
    "subsection": false,
    "pdf": "bafykbzacebus4oaptbju3qk7btlgjl2c5usdwgjbetaqgngouqblf7m7abjue",
    "epub": "bafykbzacebporaoj73ruhglkpxymtj3rzb4d64mwslljcms3vmmeuruqe6zy4",
    "length": 2.5,
    "difficulty": 2.5,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Foucault uses the history of prisons as a case study to show how the true nature of power is not located in any one group, but dispersed across society."
}

export default json;