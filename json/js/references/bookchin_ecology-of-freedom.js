const json = {
    "id": "bookchin_ecology-of-freedom",
    "title": "The Ecology of Freedom",
    "author": "Murray Bookchin",
    "section": "B-SANR",
    "subsection": false,
    "pdf": "bafykbzacedl667fdmsnnw2uqtmz4rsigxrzfllvm6ynugsc2mke7sgwpdzya2",
    "epub": "",
    "length": 2.5,
    "difficulty": 2.5,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Bookchin breaks from Marx's class analysis, instead developing an understanding of society based around the concept of hierarchy as opposed to just capitalism."
}

export default json;