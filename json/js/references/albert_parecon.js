const json = {
    "id": "albert_parecon",
    "title": "Parecon: Life After Capitalism",
    "author": "Michael Albert",
    "section": "B-DSOC",
    "subsection": true,
    "pdf": "bafykbzaceaihx5e7y3udwrv2aupnzbyvczhegphz7qbvia3zivsoaaajpbtdo",
    "epub": "",
    "length": 2,
    "difficulty": 1.5,
    "genre": "Economics",
    "type": "Blueprint",
    "summary": "Albert envisions a marketless form of socialism in which allocation is coordinated by local councils and people are rewarded for their sacrifice rather than their power."
}

export default json;