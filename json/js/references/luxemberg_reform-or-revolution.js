const json = {
    "id": "luxemberg_reform-or-revolution",
    "title": "Social Reform or Revolution?",
    "author": "Rosa Luxemberg",
    "section": "B-LCOM",
    "subsection": true,
    "pdf": "bafykbzacebeql3jsfma5ptyijaaixno2zzcj4xyjtrvxgvijy3noyh23l65su",
    "epub": "bafk2bzaced6w4d3d4n2srlprcjvtxeugnqslbdsihotujbk4vpt7q27clncqs",
    "length": 1,
    "difficulty": 2,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Against the more reformist currents in the socialist movement of her time, Luxemberg affirms that socialism can only be realized through the contradictions of capitalism reaching a boiling point: spontaneous revolution."
}
export default json;