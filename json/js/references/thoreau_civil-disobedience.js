const json = {
    "id": "thoreau_civil-disobedience",
    "title": "Civil Disobedience",
    "author": "Henry Thoreau",
    "section": "B-PACF",
    "subsection": false,
    "pdf": "bafykbzaceccdgw5ujecowjcszjqy7fyrthe67qtxdelu6phupawptfxpfsddw",
    "epub": "bafk2bzacea5eyenyqrswfk4w3mm3wq2aeq5s5b2t3tgu2gjqhqfgsdff47kgg",
    "length": 0.5,
    "difficulty": 0.5,
    "genre": "Politics",
    "type": "Tactics",
    "summary": "Thoreau proposes a nonviolent form of action known as civil disobedience; this concept would soon be integrated into countless pacifist movements throughout history."
}
export default json;