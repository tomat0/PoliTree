const json = {
    "id": "erik_liberty-or-equality",
    "title": "Liberty or Equality",
    "author": "Erik von Kuehnelt-Leddihn",
    "section": "B-MONR",
    "subsection": false,
    "pdf": "bafykbzacecrd6olalqielyhaptlbqi3vvnylkpc77lzeauoht4gs5mk644aba",
    "epub": "bafykbzacedpwavstgktsvc3dv4jxvoeuhaaf3ebbisle67uzwfg2mvat32r4k",
    "length": 2.5,
    "difficulty": 1,
    "genre": "Politics",
    "type": "Critique",
    "summary": "von Kuehnelt-Leddihn argues against the Enlightenment notion that the ideals of liberty and democracy go hand in hand, but rather instead are diametrically opposed."
}
export default json;