const json = {
    "id": "kropotkin_conquest-of-bread",
    "title": "The Conquest of Bread",
    "author": "Peter Kropotkin",
    "section": "B-SANR",
    "subsection": false,
    "pdf": "bafykbzacedjcgnmjggqggj46ssr7mhhza75uyymysirkduk3va66u4keeq7um",
    "epub": "bafykbzaced25s3hqiwzinujq2v6ghwdykacrgeddkdwbwq2ddjkw4kfn4bpd6",
    "length": 0.5,
    "difficulty": 1,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Kropotkin highlights the issues he has with both feudalism and capitalism, calling for the abolition of private property and detailing what he believes an anarcho-communist society may look like."
}
export default json;