const json = {
    "id": "proudhon_principle-of-federation",
    "title": "The Principle of Federation",
    "author": "Pierre-Joseph Proudhon",
    "section": "B-SYND",
    "subsection": false,
    "pdf": "bafykbzacecye5ybsmbbrpiet2xjjo7uhdm6qthzwtxbsgt4iv7cqnz5fggdx2",
    "epub": "",
    "length": 1,
    "difficulty": 1.5,
    "genre": "Politics",
    "type": "Tactics",
    "summary": "Proudhon develops a theory of federated organization, relying on a network of locally managed communities; this would eventually inspire the organizational tactics of the syndicalist movement."
}
export default json;