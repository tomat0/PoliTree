const json = {
    "id": "glazer_melting-pot",
    "title": "Beyond the Melting Pot",
    "author": "Nathan Glazer",
    "section": "B-CNAT",
    "subsection": false,
    "pdf": "bafykbzaceb2vynaxuxf4pvidnav3mlvygt7tvgdqyxzmkkyjiiwfheaekxbcu",
    "epub": "",
    "length": 2.5,
    "difficulty": 1.5,
    "genre": "Sociology",
    "type": "Perspective",
    "summary": "By observing the difficulties New York immigrants face with assimilation, Glazer argues that the task is far from complete and puts forth a modernized case for cultural assimilation."
}
export default json;