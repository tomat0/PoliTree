const json = {
    "id": "mills_power-elite",
    "title": "The Power Elite",
    "author": "C. Wright Mills",
    "section": "L-POPL",
    "subsection": false,
    "pdf": "bafykbzaceaex2oea7jnpparmdpmhf6xa5rngjovhcogyen7dmyj4lotpf7rl4",
    "epub": "bafykbzacedkkj2cq6goaahw3hyjuaykdaanyrzx47icf3e332g4ejxl3s5wg2",
    "length": 1.5,
    "difficulty": 1.5,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Mills discusses the concentration of power in liberal democracies, discussing how the establishments of various social institutions coalesce to form a powerful class of elites."
}
export default json;