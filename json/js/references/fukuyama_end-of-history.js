const json = {
	"id": "fukuyama_end-of-history",
	"title": "The End of History and The Last Man",
	"author": "Francis Fukuyama",
	"section": "B-IDEA",
	"subsection": false, 
	"pdf": "bafykbzacecmtyir6ttr3r6kl5v2ycxznhcbzsrw7rpflqz3yv262cf43nhbwo",
	"epub": "bafykbzacedqsobdhftgxjdvolcattpfe4cwljujc2nghv7u2cphh2yzmd6qok",
	"length": 3,
	"difficulty": 3,
	"genre": "Seminal",
	"type": "History",
	"summary": "Following the collapse of the USSR, Fukuyama reflects upon history, coming to the conclusion that liberal democracy is the ultimate stage of governance."
}

export default json;