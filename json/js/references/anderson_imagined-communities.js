const json = {
    "id": "anderson_imagined-communities",
    "title": "Imagined Communities",
    "author": "Benedict Anderson",
    "section": "B-CNAT",
    "subsection": false,
    "pdf": "bafykbzaceazycutmjp44kvt3vt44k7jvmwqtut3lrlo7lzewrljonl5hco7ss",
    "epub": "bafykbzaceb4gm6jiyuovmqd6fvstado4a37x72udvzqauyr7kiojfwunmkd7e",
    "length": 1.5,
    "difficulty": 2.5,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Anderson takes a serious look into the origins of nations, finding them to be communities constructed for the purpose of bringing together countless, disconnected people."
}
export default json;