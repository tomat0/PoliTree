const json = {
    "id": "kirk_conservative-mind",
    "title": "The Conservative Mind",
    "author": "Russell Kirk",
    "section": "B-PALO",
    "subsection": false,
    "pdf": "",
    "epub": "bafykbzacednc6gdmm3yyld6pnh6fpm37nppkpip7vol63rb4bzrjogsun67hm",
    "length": 3,
    "difficulty": 1,
    "genre": "History",
    "type": "Defense",
    "summary": "Kirk reflects on various conservatives throughout history, outlining and setting a clear direction for the greater paleoconservative movement."
}
export default json;