const json = {
    "id": "tocqueville_old-regime",
    "title": "The Ancien Regime and the Revolution",
    "author": "Alexis de Tocqueville",
    "section": "L-MODR",
    "subsection": false,
    "pdf": "bafykbzaceara5tsklaptb7uizowm4dqfaijricpepzgjs7wzudbkr36ggqgva",
    "epub": "bafykbzaceakipejpkbx2a3jcjnofrsmhqgcxms2pzdgzjcxcpyx67ehoalelu",
    "length": 1,
    "difficulty": 1,
    "genre": "History",
    "type": "Perspective",
    "summary": "Tocqueville contests the notion of the French Revolution being inherently radical; he instead holds that despite the insurrectionary nature, it was, in reality, much more reformist in its demands."
}
export default json;