const json = {
    "id": "oakeshott_politics-of-scepticism",
    "title": "The Politics of Faith and the Politics of Scepticism",
    "author": "Michael Oakeshott",
    "section": "B-LCON",
    "subsection": false,
    "pdf": "bafykbzacedfwvcgso3xggw4z5udwuwezqesfuyjdokib7sgdymitts6udyglc",
    "epub": "",
    "length": 1,
    "difficulty": 2,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Oakeshott believes at the center of politics is the question of what governments should do. He believes its crucial to exercise caution about trying to achieve perfection, but that we should not fall into total cynicism either."
}

export default json;