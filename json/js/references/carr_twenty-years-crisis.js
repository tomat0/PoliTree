const json = {
    "id": "carr_twenty-years-crisis",
    "title": "The Twenty Years' Crisis: 1919-1939",
    "author": "E.H. Carr",
    "section": "B-REAL",
    "subsection": false,
    "pdf": "bafykbzaceaghqjy4rxnsbf7z6nph62nymfc6ay7fmefuwhmiruthm6ihlr742",
    "epub": "",
    "length": 2.5,
    "difficulty": 1.5,
    "genre": "History",
    "type": "Perspective",
    "summary": "Carr points out that despite humanity's best efforts to maintain world peace, two world wars still broke out. He charges the overly naive outlook on foreign policy with allowing such things to happen."
}
export default json;