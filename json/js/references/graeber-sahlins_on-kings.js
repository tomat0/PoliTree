const json = {
    "id": "graeber-sahlins_on-kings",
    "title": "On Kings",
    "author": "David Graeber and Marshall Sahlins",
    "section": "B-MONR",
    "subsection": false,
    "pdf": "bafykbzacebzjjcpgspqgouzjqznzm4mmpbbprks2yip7dkwznlktromgpo4ue",
    "epub": "",
    "length": 3.5,
    "difficulty": 2,
    "genre": "History",
    "type": "Defense",
    "summary": "Graeber and Sahlins conduct an investigation of the cultural and political significance of kings, discovering the role unique to them as leader-figures."
}
export default json;