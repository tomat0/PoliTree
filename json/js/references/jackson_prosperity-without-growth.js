const json = {
	"id": "jackson_prosperity-without-growth",
	"title": "Prosperity Without Growth",
	"author": "Tim Jackson",
	"section": "B-DSOC",
	"subsection": true, 
	"pdf": "",
	"epub": "bafykbzaceauesx7isvzvov6x6kjfw2xnti4bi55f4nnlcrgzgsnz5kjtsh7xs",
	"length": 2.5,
	"difficulty": 1.5,
	"genre": "Ecology",
	"type": "Critique",
	"summary": "Jackson draws a distinction between prosperity and growth, showing how an economy can still sustainably provide for people without having to sacrifice our lives and planet in the name of maximizing growth. "
}

export default json;