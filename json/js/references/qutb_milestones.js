const json = {
    "id": "qutb_milestones",
    "title": "Milestones",
    "author": "Sayyid Qutb",
    "section": "B-THEO",
    "subsection": false,
    "pdf": "bafykbzacec3env3kq25dikicmr3vtcecwn27vwiutl6kodpajz3iifeiy4yts",
    "epub": "bafk2bzacebxver64rkuimk7o6lk2mspkfc26qntx57igo6h6pvwuhtxkeb6ig",
    "length": 3,
    "difficulty": 1.5,
    "genre": "Politics",
    "type": "Critique",
    "summary": "Faced with what he sees as the decline of Islamic morality in the world, Qutb defends the need for divine law and discusses what is necessary to return it."
}
export default json;