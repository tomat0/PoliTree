const json = {
    "id": "zedong_guerilla-warfare",
    "title": "On Guerilla Warfare",
    "author": "Mao Zedong",
    "section": "B-LNAT",
    "subsection": false,
    "pdf": "bafykbzacedgvjtdlgn63zwzu2uzwxpnazgsnly75exfj6puqzzp6kjknrg7nm",
    "epub": "",
    "length": 0.5,
    "difficulty": 0.5,
    "genre": "Politics",
    "type": "Tactics",
    "summary": "Drawing on his experiences from the Chinese Civil War, Mao presents a guide to guerilla warfare: a style of tactics which allow smaller fighting forces to outmaneuver much larger militaries. These tactics would be hugely influential on various anti-imperialist struggles abroad."
}
export default json;