const json = {
    "id": "burke_reflections-on-revolution",
    "title": "Reflections on the Revolution in France",
    "author": "Edmund Burke",
    "section": "B-LCON",
    "subsection": false,
    "pdf": "bafykbzacedpwhrllvbyw2aa6zh4qlb5hymn37phqt3kjozsek5onjlbts5zbq",
    "epub": "bafykbzaceazbbtsot5fd7qviryu6ttvofmc4pbxlxsek3u2jlzm4hbxcvcxqg",
    "length": 1.5,
    "difficulty": 2.5,
    "genre": "Politics",
    "type": "Critique",
    "summary": "Burke criticizes the process of the French Revolution, arguing that its foundations and radical change would pave the road for tyranny; he instead pushes for gradual reform as the proper vessel of change."
}

export default json;