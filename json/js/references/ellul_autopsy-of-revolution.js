const json = {
    "id": "ellul_autopsy-of-revolution",
    "title": "Autopsy of Revolution",
    "author": "Jacques Ellul",
    "section": "B-PACF",
    "subsection": false,
    "pdf": "bafykbzaced7ikr5zivxom7dzkif62qsa3fhoferb4tbl33c44akz6pwgqovrq",
    "epub": "",
    "length": 1.5,
    "difficulty": 1,
    "genre": "Politics",
    "type": "Critique",
    "summary": "Ellul writes harshly about insurrectionary revolt, discussing it as a revolutionary dead-end, and affirming the need for nonviolence in revolutionary action."
}
export default json;