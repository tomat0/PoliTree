const json = {
    "id": "marcuse_one-dimensional-man",
    "title": "One-Dimensional Man",
    "author": "Herbert Marcuse",
    "section": "L-NEWL",
    "subsection": false,
    "pdf": "bafykbzacedcuw7dxnfiqykka3m6jq6e6hkd2q3h53moxibc7cilrc6ccnqrp4",
    "epub": "bafykbzacedu5dpyidhi37v43hwnjovo6bev4lysph3mtdb3l4r2ri73jzvdmy",
    "length": 2.5,
    "difficulty": 2,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Marcuse makes a purely pessimistic analysis of modern society, discussing how it destroys individuality; in response, he urges people to reject consumerism in favor of artistic expression."
}
export default json;