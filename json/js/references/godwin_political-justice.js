const json = {
    "id": "godwin_political-justice",
    "title": "Enquiry Concerning Political Justice",
    "author": "William Godwin",
    "section": "L-ANAR",
    "subsection": false,
    "pdf": "bafykbzacebh4v7jbka4ashjwasfharwpv7unya5bebppg6vcwg7sr2coiu322",
    "epub": "bafykbzaceacqxiusq3tgjkurqemt7b77er7jvojs7otpbcytkqptbdnooxyig",
    "length": 3,
    "difficulty": 1,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Godwin creates one of the first anarchist arguments of the modern era, challenging the notion of the state being necessary for order."
}
export default json;