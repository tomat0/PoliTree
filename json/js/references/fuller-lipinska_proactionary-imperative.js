const json = {
    "id": "fuller-lipinska_proactionary-imperative",
    "title": "The Proactionary Imperative: A Foundation for Transhumanism",
    "author": "Steven Fuller and Veronika Lipi\u0144ska",
    "section": "B-THUM",
    "subsection": false,
    "pdf": "bafykbzacece7h2lzdemfxrl7zsurezf3d7z3bpk6izxlefiugphyt4t24bcgw",
    "epub": "",
    "length": 1,
    "difficulty": 2,
    "genre": "Ethics",
    "type": "Defense",
    "summary": "In response to the uncertainties surrounding biological modification, Fuller and Lipi\u0144ska defend actions based upon a 'proactionary principle', which argues that there is an element of risk-taking essential to technological progress."
}
export default json;