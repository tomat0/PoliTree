const json = {
    "id": "proudhon_what-is-property",
    "title": "What is Property?",
    "author": "Pierre-Joseph Proudhon",
    "section": "B-MUTL",
    "subsection": false,
    "pdf": "bafykbzacebqwrt67s57dmtgf4hvv24aak54wtycenfojsbqj7kdm24sutiomw",
    "epub": "",
    "length": 2,
    "difficulty": 1.5,
    "genre": "Economics",
    "type": "Seminal",
    "summary": "Proudhon discusses property in the abstract, drawing a line between illegitimate property and property acquired through one's labor, harshily criticizing the traditional understanding of the concept."
}
export default json;