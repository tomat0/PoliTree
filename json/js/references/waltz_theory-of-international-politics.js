const json = {
    "id": "waltz_theory-of-international-politics",
    "title": "Theory of International Politics",
    "author": "Kenneth Waltz",
    "section": "B-REAL",
    "subsection": false,
    "pdf": "bafykbzacebe6ch562abb7zycgou5yap7fcr74fcfaduhf5x3sfqkgms7iii4e",
    "epub": "",
    "length": 2,
    "difficulty": 2.5,
    "genre": "International Relations",
    "type": "Theory",
    "summary": "Considered the cornerstone work on neorealism, Waltz discusses the principles and concepts of international relations in extensive detail."
}
export default json;