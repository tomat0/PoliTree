const json = {
    "id": "beck_globalization",
    "title": "What is Globalization?",
    "author": "Ulrich Beck",
    "section": "B-COSM",
    "subsection": false,
    "pdf": "bafykbzacebwbhzkgyttgthinrm7yhcpgwjtwymkuwv7pbzruvulwfcbg4jyp4",
    "epub": "bafykbzaceb4hh6fefpufsk6hq5keelaby232ihrfzcrh4yso3bfqgkyjvttxw",
    "length": 1,
    "difficulty": 2.5,
    "genre": "International Relations",
    "type": "Critique",
    "summary": "Beck provides an introduction to the debates surrounding globalization, setting the stage for a critique of the concept from a cosmopolitan angle."
}
export default json;