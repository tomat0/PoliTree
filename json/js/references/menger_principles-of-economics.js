const json = {
    "id": "menger_principles-of-economics",
    "title": "Principles of Economics",
    "author": "Carl Menger",
    "section": "L-LIBT",
    "subsection": false,
    "pdf": "bafykbzaceayginzuqhxmfrsus4krk3m67raqde6hfxma2raa3nvgghs2slqxm",
    "epub": "",
    "length": 1.5,
    "difficulty": 1,
    "genre": "Economics",
    "type": "Theory",
    "summary": "Considered to be one of the pioneering works in microeconomics, Menger takes a bottom-up approach to economic analysis, developing concepts such as marginalism and the subjective theory of value."
}
export default json;