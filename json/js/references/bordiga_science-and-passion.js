const json = {
    "id": "bordiga_science-and-passion",
    "title": "The Science and Passion of Communism",
    "author": "Amadeo Bordiga",
    "section": "B-LCOM",
    "subsection": true,
    "pdf": "bafykbzacedty6frpzfjbpnsjo4kd7voraw6daiq4mrac3n5olaaubz6pxfrb2",
    "epub": "",
    "length": 3.5,
    "difficulty": 1.5,
    "genre": "Politics",
    "type": "Theory",
    "summary": "A collection of writings by the Italian Current's most well-known member, all written against the backdrop of mid-century struggles; these essays would prove to be greatly influential on the Communist Left."
}
export default json;