const json = {
    "id": "galbraith_american-capitalism",
    "title": "American Capitalism: The Concept of Countervailing Power",
    "author": "John Kenneth Galbraith",
    "section": "B-SLIB",
    "subsection": false,
    "pdf": "bafykbzacebispoyymrxfej4tebhfzmk6x6ycntolraluu5jhunht5g74xkp4a",
    "epub": "",
    "length": 1.5,
    "difficulty": 2,
    "genre": "Economics",
    "type": "Critique",
    "summary": "Galbraith observes how an unrestrained market naturally becomes less competitive and fair over time, and the role various political institutions can play in keeping oligopoly in check."
}
export default json;