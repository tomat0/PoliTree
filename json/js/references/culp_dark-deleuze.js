const json = {
    "id": "culp_dark-deleuze",
    "title": "Dark Deleuze",
    "author": "Andrew Culp",
    "section": "L-ACCL",
    "subsection": false,
    "pdf": "bafykbzacedu2pdcv7fads7ulccabrxwingtuzgno5pprfblb3g7ez6krpq6k4",
    "epub": "bafk2bzacecfbooyxmwgh4m7ebqchelqdggm2qv4uuqa4tgoby7dlqxoipzcdw",
    "length": 0.5,
    "difficulty": 4.5,
    "genre": "Politics",
    "type": "Critique",
    "summary": "Culp seeks to reclaim the essentially negative and radical character of Deleuze's writings; he paints an apocalyptic view of the world which conventional politics cannot rescue."
}
export default json;