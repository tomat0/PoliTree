const json = {
    "id": "adorno-horkheimer_dialectic-of-enlightement",
    "title": "Dialectic of Enlightenment",
    "author": "Theodor Adorno and Max Horkheimer",
    "section": "B-NMRX",
    "subsection": false,
    "pdf": "bafykbzacebr42v6qw7hrsqxfln73wak5h2px7n4wharislwyxjjluqbo4ypcg",
    "epub": "bafykbzaced34keyekj5nomynwlv3zsclnwlddgcrwvxrx34hqc5ghwygd52fe",
    "length": 1.5,
    "difficulty": 3,
    "genre": "Sociology",
    "type": "Theory",
    "summary": "In the wake of the various totalitarian regimes of the 20th century, Adorno and Horkheimer look upon the results of the Enlightenment pessimistically, arguing that both the movement towards 'rationalization' and the struggles against it would find themselves ending in the same spot."
}
export default json;