const json = {
    "id": "schelling_arms-and-influence",
    "title": "Arms and Influence",
    "author": "Thomas C. Schelling",
    "section": "L-INTR",
    "subsection": false,
    "pdf": "bafykbzacedklni4eohyebueojbn7m3ejiw3cbrif643dwgwly3oahmo7jref6",
    "epub": "bafykbzacecqey4us77vskjadedpcoltor6wncd5tsvfmh5hwracpn676ahqgy",
    "length": 2,
    "difficulty": 1,
    "genre": "International Relations",
    "type": "Theory",
    "summary": "Schelling talks about the nature of military power, and how both the capacity to make war and the capacity to make peace are two sides of the same coin, both made possible through projecting strength."
}
export default json;