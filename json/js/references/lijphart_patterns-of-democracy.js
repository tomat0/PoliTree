const json = {
    "id": "lijphart_patterns-of-democracy",
    "title": "Patterns of Democracy",
    "author": "Arend Lijphart",
    "section": "B-PLUR",
    "subsection": false,
    "pdf": "bafykbzaceb2elvgioqjebungvricu4jr6cytucu3anisad3suwwvo4qzryhms",
    "epub": "bafykbzacedjfh35iap444n65per6qlwdei5j4v2phn5qyttp6qfuw7gg7q4i6",
    "length": 2.5,
    "difficulty": 0.5,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Lijphart stresses the need for democracy not just to have the right institutions but also a political culture that encourages decisions based around consensus rather than  majority-rule."
}
export default json;