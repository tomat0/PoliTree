const json = {
    "id": "jmp_continuity-and-rupture",
    "title": "Continuity and Rupture",
    "author": "J. Moufawad-Paul",
    "section": "L-COMM",
    "subsection": true,
    "pdf": "bafykbzaceasfaylgo7ewgt7lknotev7u6ljcf5flk5iyy7suokff6mbzfhj4k",
    "epub": "",
    "length": 2.5,
    "difficulty": 2.5,
    "genre": "Politics",
    "type": "Theory",
    "summary": "J.M.P draws on the background of Maoism, bringing it into the current age: he claims it constitutes both a continuity and a rupture, simultaneously breaking from its predecessors while maintaining their essence."
}
export default json;