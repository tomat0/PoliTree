const json = {
    "id": "beito_the-voluntary-city",
    "title": "The Voluntary City: Choice, Community, and Civil Society",
    "author": "David T. Beito",
    "section": "B-ANCP",
    "subsection": false,
    "pdf": "bafykbzaced5xydi22fwi2bkqhsy5ybdeecqm45jtdprtyk24fmqr5457oh2bo",
    "epub": "",
    "length": 3.5,
    "difficulty": 1,
    "genre": "Politics",
    "type": "Blueprint",
    "summary": "Summary: In an attempt to disprove the necessity of the state, Beito illustrates several examples of the regarding the various functions of society and how they could be maintained, even without coercion."
}
export default json;