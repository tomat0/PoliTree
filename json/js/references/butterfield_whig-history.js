const json = {
    "id": "butterfield_whig-history",
    "title": "The Whig Interpretation of History",
    "author": "Herbert Butterfield",
    "section": "B-NEOR",
    "subsection": false,
    "pdf": "bafykbzacebfnbv3oyksrtaiodk75q444sidzrtipsigejww6iydrbm7hkenyy",
    "epub": "",
    "length": 0.5,
    "difficulty": 3,
    "genre": "History",
    "type": "Critique",
    "summary": "Butterfield calls into question the accuracy of the common post-Enlightenment narrative that society is constantly progressing in the direction of ideal democracy."
}
export default json;