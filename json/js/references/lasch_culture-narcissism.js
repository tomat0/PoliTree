const json = {
    "id": "lasch_culture-narcissism",
    "title": "The Culture of Narcissism",
    "author": "Christopher Lasch",
    "section": "B-PATR",
    "subsection": false,
    "pdf": "bafykbzaceblmqejeiv7chi3gi2wila3aklf2j6ggay4sfb5cjzi4hrjggrcje",
    "epub": "bafykbzacedrtwbydgo6wfvtvaxc2amty7yrrdy7hcsc2wnzketkxwnlephspk",
    "length": 2,
    "difficulty": 2.5,
    "genre": "History",
    "type": "Theory",
    "summary": "Lasch uses a combination of historical, psychoanalytic, and cultural analysis to understand the decline of traditional values of solidarity in favor of a culture of narrow self-interest. He traces it back to the foundations of modernity, both on a material and philosophic level."
}
export default json;