const json = {
    "id": "lenin_state-and-revolution",
    "title": "The State and Revolution",
    "author": "Vladmir Lenin",
    "section": "B-VANG",
    "subsection": false,
    "pdf": "bafykbzacedmdwekaom7rnilhpfsd7gubfomiowkd3kdroenwvdu4d24lytf42",
    "epub": "bafk2bzacebfcylztozhhbskh77d2a47wxo4l2kkgrvpop2uxo2yvs26hmau5e",
    "length": 1,
    "difficulty": 1,
    "genre": "Politics",
    "type": "Seminal",
    "summary": "Lenin puts forth a harsh criticism of social democracy which gradually develops into a discussion of the role of the state in class war; he concludes that the state must be utilized in a fashion to preserve the revolution."
}
export default json;