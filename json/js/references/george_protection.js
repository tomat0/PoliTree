const json = {
    "id": "george_protection",
    "title": "Protection or Free Trade",
    "author": "Henry George",
    "section": "B-NEOL",
    "subsection": false,
    "pdf": "bafykbzaceazgpuy6aykunulnsc2aq5fontwo6co5mo2m66r2hwfblslwrcxf6",
    "epub": "",
    "length": 1,
    "difficulty": 1,
    "genre": "Economics",
    "type": "Critique",
    "summary": "George argues that international trade helps a nation's economy in the long run, providing a defense on both ethical and pragmatic grounds."
}
export default json;