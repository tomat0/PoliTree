const json = {
    "id": "bagehot_english-constitution",
    "title": "The English Constitution",
    "author": "Walter Bagehot",
    "section": "B-MONR",
    "subsection": true,
    "pdf": "bafykbzacebebr3ewglbsrnsbgyn2axexq4yloi55zibpkilili7vv4jybgsty",
    "epub": "bafykbzacebr4xgkv7rxjl7l7tke742sxsdnfmkxiovxnokzqlpplbc6ypl224",
    "length": 0.5,
    "difficulty": 1.5,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Bagehot studies the evolution of English government, distinguishing how a constitutional monarchy works in principle from how it does in practice."
}
export default json;