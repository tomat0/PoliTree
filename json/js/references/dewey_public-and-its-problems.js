const json = {
    "id": "dewey_public-and-its-problems",
    "title": "The Public and Its Problems",
    "author": "John Dewey",
    "section": "L-MODR",
    "subsection": false,
    "pdf": "bafykbzacecomakhfcail65zejqjrtmelli2lsyocbvygzuz2vcn6yytrbim7w",
    "epub": "bafykbzacecaku2gvzaduqgboe2cm5w5bdwqb2nzigtlxeh3jkwcr73epsj4oi",
    "length": 1,
    "difficulty": 1,
    "genre": "Politics",
    "type": "Defense",
    "summary": "Dewey defends democracy against its skeptics, arguing that while the public may be prone to manipulation, the best way to deal with those issues is to encourage people to become more active and informed citizens."
}

export default json;