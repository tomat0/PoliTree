const json = {
    "id": "tainter_complex-societies",
    "title": "The Collapse of Complex Societies",
    "author": "Joseph Tainter",
    "section": "B-LUDD",
    "subsection": false,
    "pdf": "bafykbzacebmbo56npa3qqjw4ygr3m47ejl43obeyxn6ptsiuahshvrmp6ioos",
    "epub": "bafykbzacebano2xgku3rgpebc4p2xly2nl3hm5b3m6qe4frzumsfmx7ry7coy",
    "length": 2,
    "difficulty": 1.5,
    "genre": "Anthropology",
    "type": "Critique",
    "summary": "Drawing from history, Tainter finds a connection between societies' approach to problem-solving, increased structural complexity, and eventual collapse."
}
export default json;