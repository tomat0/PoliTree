const json = {
    "id": "proudhon_general-idea",
    "title": "General Idea of Revolution in The 19th Century",
    "author": "Pierre-Joseph Proudhon",
    "section": "B-MUTL",
    "subsection": false,
    "pdf": "bafykbzacebgpdjb6wj7oflzms47thwluxrysz4pdqrac4decylcnvpkoz6rts",
    "epub": "bafykbzaceddumszrorolqe3skctizxjqen7hrd7agppesq57sh3zlc3jfgagm",
    "length": 1,
    "difficulty": 1,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Proudhon takes a look at the character of revolutionary anarchism: its goals, methods, and principles."
}
export default json;