const json = {
    "id": "hobsbawm_age-of-extremes",
    "title": "The Age of Extremes: The Short Twentieth Century, 1914-1991",
    "author": "Eric Hobsbawm",
    "section": "L-COMM",
    "subsection": false,
    "pdf": "bafykbzacecku2c3jgh44jjyjsdt7ww63ametgbe266p4klbevnnvaad7rqjva",
    "epub": "bafykbzaceatqpbzc6u6kjf55blkfw3xmybygzlqch6wwbvvxv7y5qlv5ktq7y",
    "length": 4,
    "difficulty": 1.5,
    "genre": "History",
    "type": "Critique",
    "summary": "Hobsbawm provides a thorough survey of the 20th century, an era of continuous upheaval and various utopian movements, looking at their origins and eventual failures. A great deal of focus is given to the history of socialism, comparing the various movements and discussing the reasons why each of them failed."
}
export default json;