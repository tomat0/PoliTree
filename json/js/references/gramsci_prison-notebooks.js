const json = {
    "id": "gramsci_prison-notebooks",
    "title": "Prison Notebooks",
    "author": "Antonio Gramsci",
    "section": "B-NMRX",
    "subsection": true,
    "pdf": "bafykbzacedsfujctkcuv2hipp5ybseyfi6idw7ff5ku6ormrgfj4luttfdi3y",
    "epub": "bafykbzaceclt3nmpcixass43i3vokitafbrlrpksoescm3ayoardiwdkwyakc",
    "length": 3.5,
    "difficulty": 3,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Gramsci creates one of the first communist critiques of capitalism not just on a material, but also an ideological level; demonstrating the political role of 'common-sense' and education."
}
export default json;