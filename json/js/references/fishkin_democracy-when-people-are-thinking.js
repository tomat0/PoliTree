const json = {
    "id": "fishkin_democracy-when-people-are-thinking",
    "title": "Democracy When the People Are Thinking",
    "author": "James S. Fishkin",
    "section": "B-RCNT",
    "subsection": false,
    "pdf": "bafykbzaceb2u3jkdrkgon6l5gzbtg5kw6kpfug3otvlmkgzjvawkzvjs3vz2e",
    "epub": "",
    "length": 2,
    "difficulty": 0.5,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Fishkin brings up a classic dilemma in politics: do you listen to the fickle crowd or the out-of-touch elites? He argues that there is another option: restructuring our democracy in a way that prioritizes decisions that are informed."
}

export default json;