const json = {
    "id": "fourier_theory-of-four-movements",
    "title": "Theory of the Four Movements",
    "author": "Charles Fourier",
    "section": "B-UTOP",
    "subsection": false,
    "pdf": "bafykbzacedqxvo7elyzfci7fhyzssgeemvqtkjfzrxip4pbrp56dj7qh6gbem",
    "epub": "",
    "length": 2.5,
    "difficulty": 1.5,
    "genre": "Politics",
    "type": "Seminal",
    "summary": "A work considered to be far ahead of its time, Fourier guides us through an extensively vivid vision of utopia."
}
export default json;