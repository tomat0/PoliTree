const json = {
    "id": "bernstein_preconditions-of-socialism",
    "title": "Preconditions of Socialism",
    "author": "Eduard Bernstein",
    "section": "B-DSOC",
    "subsection": false,
    "pdf": "bafykbzacecuqwoyfowufatxujux5dcj6sd4yn46qgiqe7gdno3grkjzac56mm",
    "epub": "",
    "length": 2,
    "difficulty": 2,
    "genre": "Politics",
    "type": "Critique",
    "summary": "Bernstein argues that capitalism is able to adapt to crises and avoid collapse, positing instead that the best way to achieve socialism was to continuously push for reforms on the parliamentary stage."
}
export default json;