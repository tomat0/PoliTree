const json = {
    "id": "habermas_structural-transformation",
    "title": "The Structural Transformation of The Public Sphere",
    "author": "Jurgen Habermas",
    "section": "L-MODR",
    "subsection": false,
    "pdf": "bafykbzacebx2nor2p772at2qs5n2wux46j5lmeya7fywdz7uaf7lsn75i5fvg",
    "epub": "",
    "length": 1,
    "difficulty": 2.5,
    "genre": "History",
    "type": "Critique",
    "summary": "Habermas looks at the history of public discourse, and laments how the development of modern forms of mass media have led to its deterioration."
}
export default json;