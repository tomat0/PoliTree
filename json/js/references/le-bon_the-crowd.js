const json = {
    "id": "le-bon_the-crowd",
    "title": "The Crowd: A Study of The Popular Mind",
    "author": "Gustave Le Bon",
    "section": "L-ARIS",
    "subsection": false,
    "pdf": "bafykbzaceakknef6rod3kr4ck7ynjdns4rzzz7v3mbeygb6pm7joh32l5k5hg",
    "epub": "",
    "length": 1,
    "difficulty": 1,
    "genre": "Sociology",
    "type": "Theory",
    "summary": "An important work in crowd-psychology, detailing the fickleness, irrationality, and gullibility of the public as a collective."
}
export default json;