const json = {
    "id": "lenin_what-is-to-be-done",
    "title": "What Is to Be Done?: Burning Questions of Our Movement",
    "author": "Vladmir Lenin",
    "section": "B-VANG",
    "subsection": false,
    "pdf": "bafykbzaceb4s4ja3ws7ydytvxageyffyq5h4uncpezbw3znbsxaegvhfm3xty",
    "epub": "bafykbzacea56tfjfasaram7ubdqmt4lavv43eilbqqhcir4ylzdh4cw44lw7u",
    "length": 1,
    "difficulty": 2,
    "genre": "Politics",
    "type": "Tactics",
    "summary": "Lenin rejects the theory of spontanaeity, stressing the necessity of proper organization in the proper execution of revolutionary struggle.Lenin rejects the theory of spontanaeity, stressing the necessity of proper organization in the proper execution of revolutionary struggle."
}
export default json;