const json = {
    "id": "bentham_morals-and-legislation",
    "title": "An Introduction to the Principles and Morals of Legislation",
    "author": "Jeremy Bentham",
    "section": "B-TCHN",
    "subsection": false,
    "pdf": "bafykbzaceamezpyceoruvow73p5rrhkb3c5ik46skhowdbychs2wqc7giz4yy",
    "epub": "bafykbzacecyqcy7cahhezikmchfr43vas3bowgtwb4b2s2gcxsratefkj7pca",
    "length": 0.5,
    "difficulty": 1.5,
    "genre": "Ethics",
    "type": "Theory",
    "summary": "Bentham formulates a theory of ethics which attempts to maximize human happiness: to do this, he investigates causes of pleasure and pain, and also sketches methods to measure the positive effects of any actions."
}
export default json;