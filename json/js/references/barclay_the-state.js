const json = {
    "id": "barclay_the-state",
    "title": "The State",
    "author": "Harold Barclay",
    "section": "L-ANAR",
    "subsection": false,
    "pdf": "bafykbzaceaviolfwzqj6kg22nqzd56guqge6xvkbyelkjgwtrr2hddalvn2s2",
    "epub": "",
    "length": 1,
    "difficulty": 1,
    "genre": "History",
    "type": "Theory",
    "summary": "Barclay analyzes the history of the state and stateless societies, highlighting its tyranny, and calling into question its necessity."
}

export default json;