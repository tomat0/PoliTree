const json = {
    "id": "debord_society-of-the-spectacle",
    "title": "Society of the Spectacle (w/ Comments)",
    "author": "Guy Debord",
    "section": "B-ULFT",
    "subsection": true,
    "pdf": "bafykbzaceahwduwjgdtgiessmxex5wb7pleq2jffr6sm7tqdrr6tx3qzlvvpu",
    "epub": "bafk2bzacecwfldcvqvv5w7pg73ukoxx3s7nhzeembzanceyislidjif6jtu5q",
    "length": 1,
    "difficulty": 3.5,
    "genre": "Politics",
    "type": "Theory",
    "summary": "Debord discusses the effects of commodity in the 20th century, namely related to how it affects the individual; in contrast to the monotony of the Spectacle, Debord calls for a convergence of life, art, and politics. The included <i>Comments on Society of the Spectacle</i> elaborates and updates the concepts put forth in the original."
}
export default json;