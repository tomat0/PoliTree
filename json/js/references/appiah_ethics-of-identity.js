const json = {
    "id": "appiah_ethics-of-identity",
    "title": "The Ethics of Identity",
    "author": "Kwame Anthony Appiah",
    "section": "B-COSM",
    "subsection": false,
    "pdf": "bafykbzaced3lq5bviojx7t4dq6io3nrhhbk6y5ldo5m2pfdm3xrisjkp5x6gg",
    "epub": "bafykbzacecnbilq7aejgleiflxgoi7f27hsxvp6xz4tgioiofz4ypked6ymvm",
    "length": 2,
    "difficulty": 0.5,
    "genre": "Sociology",
    "type": "Theory",
    "summary": "Appiah places the question of a collective 'identity' into the modern context, rethinking it by taking into account developments in human rights, globalization, and individuality."
}
export default json;