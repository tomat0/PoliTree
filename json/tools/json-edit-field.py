import json
import os 

# Arguments, can modify these
directory = 'branches/' # Directory you want to search in, make an empty string to search all 
field = 'subbranches' # Field you want to check for matches
value = ''; # New value you want to put in, to dynamically set, reassign the variable later

# Global variables
errorCount= 0;
jsonRoot = '../json/'
searchDir = jsonRoot + directory;

# Terminal Colors
# Taken from the Blender build scripts cited by joeld in: https://stackoverflow.com/questions/287871/how-do-i-print-colored-text-to-the-terminal
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


print("PoliTree JSON Value Editor Tool\nWritten by Tomat0 under GPL-3.0-or-later. Edit arguments at top of .py file to use.\n")

for root, dirs, files in os.walk(searchDir, topdown=True):
    for name in files:
        # For each JSON file, copy the contents into the new JS file
        fp = os.path.join(root, name)
        try:
            jsonData = open(fp,"r")
        except:
            print(bcolors.FAIL + name + " is an invalid filename or type for reading." + bcolors.ENDC)
            errorCount += 1
            continue
        try:
            jsonContent = jsonData.read()
            jsonDict = json.loads(jsonContent)
        except:
            print(bcolors.FAIL + name + " failed to be updated due to a JSON syntax error." + bcolors.ENDC)
            errorCount += 1
            continue
        jsonData.close()
        try:
            jsonData = open(fp,"w")
        except:
            print(bcolors.FAIL + name + " is an invalid filename or type for writing." + bcolors.ENDC)
            errorCount += 1
            continue
        jsonDict[field] = value
        json.dump(jsonDict, jsonData, indent=4)          
        jsonData.close() # JSON file no longer needed

        



        
# Print results at the end
if errorCount == 0:
    print(bcolors.OKGREEN)
else:
    print(bcolors.WARNING)
print("Editing completed. " + str(errorCount) + " errors found." + bcolors.ENDC)
