import json
import os 


# Directory Paths
jsonRoot = '../json/'
clusterPath = jsonRoot + "clusters/"
leafPath = jsonRoot + "leaves/"
branchPath = jsonRoot + "branches/"
referencePath = jsonRoot + "references/"
questionPath = "questions/"
filterQPath = jsonRoot + questionPath + "filter/"
canopyQPath = jsonRoot + questionPath + "canopy/"
focusPath = clusterPath + 'focus/'
approachPath = clusterPath + 'approach/'




def validateJSON(jsonPath):
    try:
        jsonData = open(jsonPath,)
        json.load(jsonData)
    except ValueError as err:
        return False
    return True


# Search for IDs from string property
def idSearch(searchPath, searchID, printStr, fp):
    if not os.path.exists(searchPath + searchID + '.json'):
        print("\t\t" + fp + ": Invoked " + printStr + " ID " + searchID + " not found. " + "\n")


# Search for IDs from list property
def idListSearch(searchPath, searchList, printStr, fp):
    for i in searchList:
        flag = idSearch(searchPath, i, printStr, fp)
# Keep track of metrics when loading from multiple different paths
def trackMetrics(isExists, metricsDict):
    metricsDict['total'] += 1
    if isExists:
        metricsDict['valid'] += 1
    else: 
        metricsDict['invalid'] += 1


def trackBenchMetrics(benchMetrics, metricsDict, testName):
    print("\t" + testName + " completed, " + str(metricsDict['total']) + " files scanned. " + str(metricsDict['valid']) + " valid files found. " + str(metricsDict['invalid']) + " invalid files found.\n")
    benchMetrics['total'] += 1
    if ((metricsDict['valid'] == metricsDict['total']) and (metricsDict['invalid'] == 0)):
        benchMetrics['passed'] += 1
    else:
        benchMetrics['failed'] += 1

def referenceListSearch(refList):
    for r in refList:
        jsonData = open(referencePath+r+'.json',)
        jsonData = json.load(jsonData)
        print(json.dumps(jsonData, indent=4))

def stringParse(string):
    try:
        string = json.loads(string)
    except json.decoder.JSONDecodeError:
        try:
            string = int(string)
        except ValueError:
            string = string  
    return string


print("PoliTree JSON Validation Testbench\nWritten by Tomat0 under GPL-3.0-or-later\nEnter /h for help\n")
while True:
    cmd = input("Enter your command:\n")

    if cmd[0:4] == '/p t':
        benchMetrics = {"total": 0, "passed": 0, "failed": 0}

        # Test 1: JSON Validation: Checks every JSON for validity
        print("Test 1: JSON Validation\n")
        jsonValidationMetrics = {"total": 0, "valid": 0, "invalid": 0}
        for root, dirs, files in os.walk(jsonRoot, topdown=True):
           for name in files:
                fp = os.path.join(root, name)
                isValid = validateJSON(fp)
                jsonValidationMetrics['total'] += 1
                if isValid:
                    jsonValidationMetrics['valid'] += 1
                else: 
                    print("\t\tInvalid JSON format at " + fp + "\n")
                    jsonValidationMetrics['invalid'] += 1
        trackBenchMetrics(benchMetrics, jsonValidationMetrics, "JSON Validation Test")


        # Test 2: Filename Validation: Checks to ensure each filename matches the ID
        print("Test 2: Filename Validation\n")
        filenameValidationMetrics = {"total": 0, "valid": 0, "invalid": 0}
        for root, dirs, files in os.walk(jsonRoot, topdown=True):
           for name in files:
                fp = os.path.join(root, name)
                fn = name.replace('.json', '')
                jsonData = open(fp,)
                jsonData = json.load(jsonData)
                if (fn != 'config'):  # Config has no ID property, so skip
                    filenameValidationMetrics['total'] += 1
                    if jsonData['id'] == fn:
                        filenameValidationMetrics['valid'] += 1
                    else:
                        filenameValidationMetrics['invalid'] += 1
                        print("\t\tInvalid filename at " + fp + "\n")
        trackBenchMetrics(benchMetrics, filenameValidationMetrics, "Filename Validation Test")

        # Test 3: ID Validation: Checks to ensure referenced IDs are valid
        print("Test 3: Validating Referenced IDs\n")
        idValidationMetrics = {"total": 0, "valid": 0, "invalid": 0}

        # Validate Cluster JSON's children
        for root, dirs, files in os.walk(clusterPath, topdown=True):
            for name in files:
                fp = os.path.join(root, name)
                isExists = 1
                jsonData = open(fp,)
                jsonData = json.load(jsonData)    
                children = jsonData['leaves'] + jsonData['other']
                idListSearch(leafPath, children, 'Leaf', fp)
                trackMetrics(isExists, idValidationMetrics)

        # Validate Leaf JSON's invoked IDs
        for root, dirs, files in os.walk(leafPath, topdown=True):
            for name in files:
                fp = os.path.join(root, name)
                isExists = 1
                jsonData = open(fp,)
                jsonData = json.load(jsonData)
                flag = idListSearch(focusPath, jsonData['focus'], 'Focus', fp)
                idListSearch(approachPath, jsonData['approach'], 'Approach', fp)
                idListSearch(branchPath, jsonData['branches'], 'Branch', fp)
                idListSearch(referencePath, jsonData['references'], 'Reference', fp)
                trackMetrics(isExists, idValidationMetrics)
                
        # Validate Branch JSONs invoked IDs
        for root, dirs, files in os.walk(branchPath, topdown=True):
            for name in files:
                fp = os.path.join(root, name)
                isExists = 1
                jsonData = open(fp,)
                jsonData = json.load(jsonData)  
                flag = idListSearch(referencePath, jsonData['references'], 'Reference', fp)
                
                flag = idSearch(leafPath, jsonData['leaf'], 'Leaf', fp)   
                trackMetrics(isExists, idValidationMetrics)

        # Validate Reference JSONs invoked IDs 
        for root, dirs, files in os.walk(referencePath, topdown=True):
            for name in files:
                fp = os.path.join(root, name)
                isExists = 1
                jsonData = open(fp,)
                jsonData = json.load(jsonData)
                if not os.path.exists(branchPath + jsonData['section'] + ".json"):
                    flag = idSearch(leafPath, jsonData['section'], 'Leaf/Branch', fp)
                trackMetrics(isExists, idValidationMetrics)

        # Validate Filter Question JSON invoked IDs 
        for root, dirs, files in os.walk(filterQPath, topdown=True):
            for name in files:
                fp = os.path.join(root, name)
                isExists = 1
                jsonData = open(fp,)
                jsonData = json.load(jsonData)
                idSearch(leafPath, jsonData['leaf'], 'Leaf', fp)
                idListSearch(branchPath, jsonData['branch-a'], 'Branch', fp)
                idListSearch(branchPath, jsonData['branch-b'], 'Branch', fp)
                idListSearch(filterQPath, jsonData['children'], 'Filter Question', fp)
                trackMetrics(isExists, idValidationMetrics)

        # Validate Canopy Question JSON invoked IDs 
        for root, dirs, files in os.walk(canopyQPath, topdown=True):
            for name in files:
                fp = os.path.join(root, name)
                isExists = 1
                jsonData = open(fp,)
                jsonData = json.load(jsonData)
                for a in range(0,len(jsonData['focus'])):
                    idSearch(focusPath, jsonData['focus'][a], 'Focus', fp)
                for a in jsonData['answers']:
                    idListSearch(approachPath, a['approach'], 'Approach', fp) 
                trackMetrics(isExists, idValidationMetrics)

        trackBenchMetrics(benchMetrics, idValidationMetrics, "ID Validation Test")

        # Test 4: Reference Hash Validation: Checks to ensure references have links
        print("Test 4: Validating Hashes\n")
        hashValidationMetrics = {"total": 0, "valid": 0, "invalid": 0}
        for root, dirs, files in os.walk(referencePath, topdown=True):
               for name in files:
                    isValid = 1
                    fp = os.path.join(root, name)
                    fn = name.replace('.json', '')
                    jsonData = open(fp,)
                    jsonData = json.load(jsonData)

                    if len(jsonData['pdf']) == 0:
                        if len(jsonData['epub']) == 0:
                            print("\t\tMissing PDF/EPUB hash for " + fp + "\n")
                            isValid = 0
                        else:
                            print("\t\tMissing PDF hash for " + fp + "\n")
                            isValid = 0
                    if len(jsonData['pdf'])%62 != 0: # A valid IPFS link should be 46 chars long, we can check this (while filtering out the emptys) by doing a modulo
                        print("\t\tIncorrect PDF hash length for  " + fp + "\n" + str(len(jsonData)))
                        isValid = 0
                    if len(jsonData['epub'])%62 != 0:
                        print("\t\tIncorrect EPUB hash length for  " + fp + "\n" + str(len(jsonData)))
                        isValid = 0
                    trackMetrics(isValid, hashValidationMetrics)
        trackBenchMetrics(benchMetrics, hashValidationMetrics, "Hash Validation Test")    
                        


        print("Bench completed, " + str(benchMetrics['total']) + " tests run. " + str(benchMetrics['passed']) + " tests passed. " + str(benchMetrics['failed']) + " tests failed.")



    # Preview references for a given branch/leaf
    if cmd[0:4] == '/p r':
        idName = cmd[5:]
        if os.path.exists(branchPath + idName + ".json"):
            jsonData = open(branchPath + idName + ".json")
            jsonData = json.load(jsonData)
            referenceListSearch(jsonData['references'])
        elif os.path.exists(leafPath + idName + ".json"):
            jsonData = open(leafPath + idName + ".json")
            jsonData = json.load(jsonData) 
            referenceListSearch(jsonData['references'])
        else:
            print("Invalid ID supplied: " + idName + "\n")


    # Add a new field to a directory's JSONs
    if cmd[0:4] == '/f a':
        targetDir = input("Enter directory to add fields:\n")
        targetDir = jsonRoot + targetDir
        fieldName = input("Enter field name:\n")
        defaultVal = input("Enter default field value:\n")
        defaultVal = stringParse(defaultVal)

        if os.path.exists(targetDir):
            for root, dirs, files in os.walk(targetDir, topdown=True):
                for name in files:
                    fp = os.path.join(targetDir, name)
                    jsonData = open(fp,)
                    jsonData = json.load(jsonData)
                    if fieldName in jsonData:
                        print("Field " + fieldName + " already exists at " + fp + ". Skipped!\n")
                    else:
                        jsonData[fieldName] = defaultVal
                        jsonExport = json.dumps(jsonData, indent=4)
                        with open(fp, "w") as o:
                            o.write(jsonExport)
                            print("Field " + fieldName + " with value " + str(defaultVal) + " written to " + fp + "\n")
        
        
    if cmd[0:2] == '/h':
        print("========================================================\n")
        print("Command List\n")
        print("/h:\t Pulls up Help menu\n")
        print("/p t:\t Prints output of validation tests\n")
        print("/p r [ID]:\t Displays references for a given leaf/branch ID\n")
        print("========================================================\n")
