import json
import os 

errorCount = 0

# Terminal Colors
# Taken from the Blender build scripts cited by joeld in: https://stackoverflow.com/questions/287871/how-do-i-print-colored-text-to-the-terminal
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


# Directory Paths
jsonRoot = '../json/'
jsRoot = '../js/'

# Paths needed for writing the importJSON.js file
importPath = '../../js/backend/importJSON.js' # Path to importJSON.js relative to this file
samplePath = '../../js/backend/importJSON.js.sample'
relRoot = '../../json/js/' # Path to the JS JSONs directory relative to importJSON.js

print("PoliTree JSON to JS Conversion Tool\nWritten by Tomat0 under GPL-3.0-or-later\nEnter /c to convert\n")

while True:
    cmd = input("Enter your command:\n")
    if cmd[0:2] == '/c':
        importContent = ""
        importList = "["
        for root, dirs, files in os.walk(jsonRoot, topdown=True):
            for name in files:
                # For each JSON file, copy the contents into the new JS file
                fp = os.path.join(root, name)
                try:
                    jsonData = open(fp,)
                except:
                    print(bcolors.FAIL + name + " is an invalid filename or type." + bcolors.ENDC)
                    errorCount += 1
                    continue
                try:
                    jsonContent = jsonData.read()
                    jsonDict = json.loads(jsonContent)
                except:
                    print(bcolors.FAIL + name + " failed to be updated due to a JSON syntax error." + bcolors.ENDC)
                    errorCount += 1
                    continue
                jsonData.close() # JSON file no longer needed

                # Open the new JS file, copy into it the header to turn it into valid JS and then the contents
                jsDir = root.replace("json", "js")
                jsName = name.replace("json", "js")
                newFP = os.path.join(jsDir, jsName)
                jsFile =  open(newFP, "w+")

                # To access the dict objects in JS, you'll have to do [importname].json
                jsHeader = "const json = " 
                jsFooter = "\nexport default json;"
                jsContent = jsHeader + jsonContent + jsFooter;
                jsFile.write(jsContent)
                jsFile.close()
                print(newFP + " successfully written.")
                
                # Each file gets an import line written to importJSON.js, construct the content line by line
                
                # Dashes and pluses are not valid for JS variable names, will have to replace them with underscores
                importName = jsonDict['id'].replace("-", "_") #JSON id acts as basis for import name 
                importName = importName.replace("+", "_")
                importList += importName + ","
                rootIndex = newFP.find('/js/') + len('/js/')
                relPath = relRoot + newFP[rootIndex:]
                importLine = "import {} from '{}';\n".format(importName, relPath)
                importContent = importContent + importLine

        #With list complete, add it in at the end
        importList = "export const all = {}];\n".format(importList[:-1])
        importContent += importList

        # Pull functions from importJSON.js.sample into here
        templateFile = open(samplePath,)
        templateContent = templateFile.read()
        importContent += templateContent
        templateFile.close()

        # Write contents to the file
        importFile = open(importPath, "w")
        importFile.write(importContent)
        print(importPath + " successfully written.")
    # Print results at the end
    if errorCount == 0:
        print(bcolors.OKGREEN)
    else:
        print(bcolors.WARNING)
    print("Conversion completed. " + str(errorCount) + " errors found." + bcolors.ENDC)
    errorCount = 0 # Reset after each loop



 # THE PART REPLACING DASHES FUCKS UP THE PATH NAMES, YOU NEED TO FIX THAT
